# every include
using LinearAlgebra
using StaticArrays
using Random
using Makie
using FileIO
using Logging
using GeometryTypes
const deflog = global_logger()

using Revise
using RANSAC
using RANSACVisualizer
using CSGBuilding

cd(@__DIR__)

m = load("furcsa100.obj");
m = load("icsg_159csg_l2.obj");
m = load("Part4_l.obj");
println("Mesh size: $(size(m.vertices,1))")
showgeometry(m)

global_logger(nosource_IterInflog())
Random.seed!(1234);
rp = randperm(size(m.vertices,1));
rn = 1:2:size(m.vertices,1)
#mrv = m.vertices[rp[rn]]
mrv = RANSAC.noisifyv_fixed(m.vertices[rp[rn]], true, 0.03)
mrn = m.normals[rp[rn]]

showgeometry(mrv, mrn, arrow=0.4)
scatter(mrv, markersize=0.7)

pcr = PointCloud(mrv, mrn, 2);

global_logger(nosource_IterLow1log())

p = RANSACParameters{Float64}(ϵ_plane=0.2,
                                α_plane=deg2rad(5),
                                ϵ_sphere=0.1,
                                α_sphere=deg2rad(1),
                                ϵ_cylinder=0.3,
                                α_cylinder=deg2rad(2.5),
                                ϵ_cone=0.15,
                                α_cone=deg2rad(1),
                                ϵ_transl=5.7,
                                α_transl=deg2rad(5),
                                minsubsetN=15,
                                itermax=2,
                                #shape_types=[:sphere, :plane, :cylinder, :cone],
                                shape_types=[:translational_surface],
                                thinning_par=0.5,
                                force_transl=false,
                                thin_method=:deldir,
                                τ = 500,
                                min_normal_num=0.8,
                                prob_det=0.9);

cand, extr = ransac(pcr, p, true, reset_rand=true);
showtype(extr)
showtype(cand)
bc = largestshape(cand)
bshape = cand[bc.index].candidate.shape
scatter(mrv[bshape.contourindexes], markersize=5)
ash = cand[2].candidate.shape
scatter(mrv[ash.contourindexes], markersize=5)

# contour and circles of an EXTRACTED
bex = extr[1].candidate.shape
sc = scatter(bex.contour, scale_plot=false, markersize=3)
RANSACVisualizer.drawcircles!(sc, bex.contour, p.ϵ_transl; strokecolor=:blue, scale_plot=false)

cis = bex.candidate.shape.contour
tris = RANSAC.delaunay(cis)
wframe(cis, tris, scale_plot=false)

isd(cis, tris)

showbytype(pcr, extr, markersize=2)
showbytype(pcr, cand, markersize=0.5)
showshapes(pcr, extr, markersize=0.3)
showshapes!(sc, pcr, [cand[6]]; markersize=1)
scatter(pcr.vertices[getrest(pcr)], markersiez=10)


pp = RANSAC.project2sketchplane(mrv, bex.coordframe)
pp = RANSAC.project2sketchplane(mrv, bex.coordframe)
scatter(pp, scale_plot=false)

bshape
pp2, nn2 = RANSAC.project2sketchplane(pcr, 1:size(mrv,1), bshape.coordframe, p)

sc2 = scatter(pp2[1:2:end], scale_plot=false, markersize=0.2)
RANSACVisualizer.drawcircles!(sc2, pp2[1:2:end], p.ϵ_transl; strokecolor=:blue, scale_plot=false)
RANSACVisualizer.drawcircles!(sc2, pp2[1:2:end],5.5; strokecolor=:blue, scale_plot=false)

# every include
using LinearAlgebra
using StaticArrays
using Random
using Makie
using FileIO
using Logging
using GeometryTypes
const deflog = global_logger()
using BSON
using Setfield

using Revise
using RANSAC
using RANSACVisualizer
using CSGBuilding

cd(@__DIR__)

## for state of the art
bp = joinpath(dirname(@__DIR__), "thesis", "figures\\")
# rms
rmse(n, ps) = sqrt((evaldistance(n, ps)/length(ps)))


#####
# first one
####

dbgen = BSON.load("Part4redprimitives_genetic.bson")
dbr_new = BSON.load("new_Part4redprimitives_ransac.bson")

obb = RANSAC.findOBB_(dbr_new[:pc].vertices)
sl1 = norm(obb[1]-obb[2])
sl2 = norm(obb[1]-obb[3])
sl3 = norm(obb[1]-obb[5])
sls = [sl1, sl2, sl3]
obb_a = sqrt(sum(sls .^2))


m = load("Part4red.obj");
println("Mesh size: $(size(m.vertices,1))")
showgeometry(m)

## input pic
sc = Scene(resolution=(1920,1080))
showgeometry!(sc, m, markersize=1, arrow=0.1)
sc.center=false
save(bp*"ex1_input.png", sc)

pc = PointCloud(m.vertices, m.normals, 4)
d = octreedepth(pc)

## segmented
extr = dbr_new[:extr]
pcr = dbr_new[:pc]

## colored by type
sc = Scene(resolution=(1920,1080))
sc2 = showbytype!(sc, pcr, extr, markersize=1.5)
sc.center=false
save(bp*"ex1_segmented.png", sc2)

## colored by pcs
sc = Scene(resolution=(1920,1080))
sc2 = showshapes!(sc, pcr, extr, markersize=1.5)
sc2.center=false
save(bp*"ex1_segmentedcol.png", sc2)

## genetic results
bc = dbgen[:bestcached]
bt = dbgen[:besttree]
tree2tex(toNiceTree(bc), "ex1bc.txt")

# rms
rmse(bt, pcr.vertices)

depth(bc)
treesize(bc)

## modifie
edgel = (mincorner = -60.500003814697266, maxcorner = 262.0000305175781, edgelength = 110)
writeparaviewformat(bt, "ex1_original", edgel)

bt.children[1].children[2].data.radius

modbt = @set bt.children[1].children[2].data.radius = 10

writeparaviewformat(modbt, "ex1_original", edgel)

########################
## Translational results
########################

dbgen = BSON.load("Part4redtranslational_genetic.bson")
dbr_new = BSON.load("new_Part4redtranslational_ransac.bson")

m = load("Part4red.obj");
println("Mesh size: $(size(m.vertices,1))")
showgeometry(m)

## input pic
sc = Scene(resolution=(1920,1080))
showgeometry!(sc, m, markersize=1, arrow=0.1)
sc.center=false
#save(bp*"ex1_input.png", sc)

pc = PointCloud(m.vertices, m.normals, 4)
d = octreedepth(pc)

## segmented
extr = dbr_new[:extr]
pcr = dbr_new[:pc]

## colored by type
sc = Scene(resolution=(1920,1080))
sc2 = showbytype!(sc, pcr, extr, markersize=1.5)
sc2.center=false
save(bp*"ex2_segmented.png", sc2)

## colored by pcs
sc = Scene(resolution=(1920,1080))
sc2 = showshapes!(sc, pcr, extr, markersize=1.5)
sc2.center=false
save(bp*"ex2_segmentedcol.png", sc2)

## colored by pcs
sc2 = lines(extr[1].candidate.shape.contour, scale_plot=false)
scatter!(sc2, extr[1].candidate.shape.contour, scale_plot=false, color=:green, markersize=1)
sc2.center=false
save(bp*"ex2_contour.png", sc2)



## genetic results
bc = dbgen[:bestcached]
bt = dbgen[:besttree]
tree2tex(toNiceTree(bc), "ex2bc.txt")

depth(bt)
treesize(bt)


rmse(bt, pcr.vertices)

## modifie
edgel = (mincorner = -80, maxcorner = 270, edgelength = 110)
writeparaviewformat(bt, "ex2_original", edgel)
