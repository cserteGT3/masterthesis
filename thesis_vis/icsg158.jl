# every include
using LinearAlgebra
using StaticArrays
using Random
using Makie
using FileIO
using BSON
using Logging
using GeometryTypes
const deflog = global_logger()

using Revise
using RANSAC
using RANSACVisualizer
using CSGBuilding
const CSGB = CSGBuilding

bp = joinpath(dirname(@__DIR__), "thesis", "figures\\")
cd(@__DIR__)

fn = "Part4red.obj"
m = load(fn);
println("Mesh size: $(size(m.vertices,1))")
showgeometry(m)



#########
### ransac
#########

dbr1 = BSON.load("icsg_158csgtransls-v2_ransac.bson")

dbgen1 = BSON.load("icsg_158csgtransls-v2_genetic.bson")

pcr1 = dbr1[:pointcloud]
extr1 = dbr1[:extracted]


#########
### pic 1
#########

sc = scatter(pcr1.vertices, markersize=0.05, show_axis = false, resolution=(1920,1080))
sc.center=false
#save(bp*"m4_input.png", sc)

#########
### by type
#########

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showbytype!(sc, pcr1, extr1, markersize=0.05)
sc2.center=false
#save(bp*"m4_segmented.png", sc2)

#=
sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showbytype!(sc, pcr1, extr1[1:3], markersize=0.05)
sc2.center=false
#save(bp*"ex1_segmented_tr.png", sc2)
=#

#########
### diff
#########


sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showshapes!(sc, pcr1, extr1, markersize=0.05)
sc2.center=false
#save(bp*"ex1_segmentedcol.png", sc2)


t2t = ["Translational$i" for i in [1,3,4]]

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showshapes!(sc, pcr1, extr1[[1,3,4]], markersize=0.05, texts=t2t)
sc2.center=false
#save(bp*"m4_partsegcol_tr.png", sc2)

# #########
### parameters
#########
d = octreedepth(pcr)
extr1 |> length
dbgen1[:surfaces] |> length

extr2 |> length
dbgen2[:surfaces] |> length


genp1 = dbgen1[:params]
genp1.ϵ_d
genp1.α|>rad2deg
genp1.itermax

genp2 = dbgen2[:params]
genp2.ϵ_d
genp2.α|>rad2deg
genp2.itermax

# #########
### tree
#########

convp = (α_conv=cosd(5), ϵ_conv=1);
bc1 = dbgen1[:bestcached]
CSGB.tree2tex_orderfix(bc1, "bc1.txt", pcr1, extr1, convp, preamble=true)
tree2tex(toNiceTree(bc1), "ori.txt")

convp = (α_conv=cosd(5), ϵ_conv=1);
bc2 = dbgen2[:bestcached]
CSGB.tree2tex_orderfix(bc2, "bc2.txt", pcr1, extr2, convp, preamble=true)
tree2tex(toNiceTree(bc2), "or2.txt")

# #########
### quan
#########
bt1 = dbgen1[:besttree]
bt2 = dbgen2[:besttree]
rmse(bt1, pcr1.vertices)
rmse(bt2, pcr1.vertices)

depth(bt1)
treesize(bt1)

depth(bt2)
treesize(bt2)

rif1 = dbgen1[:runinfo]
rif1[:ransactimes]
rif1[:genetictimes]

rif2 = dbgen2[:runinfo]
rif2[:ransactimes]
rif2[:genetictimes]

edgel = (mincorner = -60, maxcorner = 260, edgelength = 150)

writeparaviewformat(bt1, "p4_ori", edgel)
writeparaviewformat(bt2, "p4_tr", edgel)
