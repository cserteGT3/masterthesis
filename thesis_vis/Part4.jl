# every include
using LinearAlgebra
using StaticArrays
using Random
using Makie
using FileIO
using BSON
using Logging
using GeometryTypes
const deflog = global_logger()

using Revise
using RANSAC
using RANSACVisualizer
using CSGBuilding
const CSGB = CSGBuilding

bp = joinpath(dirname(@__DIR__), "thesis", "figures\\")
cd(@__DIR__)

fn = "Part4red.obj"
m = load(fn);
println("Mesh size: $(size(m.vertices,1))")
showgeometry(m)


#########
### pic 1
#########

sc = scatter(m.vertices, markersize=1, show_axis = false, resolution=(1920,1080))
sc.center=false
#save(bp*"ex1_input.png", sc)

#########
### ransac
#########

dbr1 = BSON.load("new_Part4redprimitives_ransac.bson")
dbr2 = BSON.load("new_Part4redtranslational_ransac.bson")

dbgen1 = BSON.load("Part4redprimitives_genetic.bson")
dbgen2 = BSON.load("Part4redtranslational_genetic.bson")

pcr1 = dbr1[:pc]
extr1 = dbr1[:extr]
extr2 = dbr2[:extr]

#########
### by type
#########

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showbytype!(sc, pcr1, extr1, markersize=1)
sc2.center=false
#save(bp*"ex1_segmented.png", sc2)

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showbytype!(sc, pcr1, extr2, markersize=1)
sc2.center=false
#save(bp*"ex1_segmented_tr.png", sc2)

#########
### diff
#########

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showshapes!(sc, pcr1, extr1, markersize=1)
sc2.center=false
#save(bp*"ex1_segmentedcol.png", sc2)

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showshapes!(sc, pcr1, extr2, markersize=1)
sc2.center=false
#save(bp*"ex1_segmentedcol_tr.png", sc2)

###################
## contour
###################

function interp(a, b, n)
    r = collect(range(0, stop=1, length=n+2))
    deleteat!(r, 1)
    deleteat!(r, lastindex(r))
    return [(1-i)*a+i*b for i in r]
end


trls = extr2[1].candidate.shape
c = trls.contour

plotcontour(trls, true, true)

s7_ = Scene(scale_plot=false, show_axis = false, resolution=(1920,1080))
sct1 = lines!(s7_, c, scale_plot=false)
tms_ = [RANSAC.midpoint(c, i) for i in eachindex(c)]
tns_ = [dn2shape_outw(tms_[i],trls)[2] for i in eachindex(tms_)]
tms = tms_[1:2:end]
tns = tns_[1:2:end]
x_ = (x->x[1]).(tms)
y_ = (x->x[2]).(tms)
u_ = (x->x[1]).(tns)
v_ = (x->x[2]).(tns)
arrows!(s7_, x_, y_, u_, v_, scale_plot=false, lengthscale=7f0, linewidth=1.5, arrowsize=2)
s7_.center=false
save(bp*"ex1_contnorm.png", s7_)

# #########
### parameters
#########
d = octreedepth(pcr)
extr1 |> length
dbgen1[:surfaces] |> length

extr2 |> length
dbgen2[:surfaces] |> length


genp1 = dbgen1[:params]
genp1.ϵ_d
genp1.α|>rad2deg
genp1.itermax

genp2 = dbgen2[:params]
genp2.ϵ_d
genp2.α|>rad2deg
genp2.itermax

# #########
### tree
#########

convp = (α_conv=cosd(5), ϵ_conv=1);
bc1 = dbgen1[:bestcached]
CSGB.tree2tex_orderfix(bc1, "bc1.txt", pcr1, extr1, convp, preamble=true)
tree2tex(toNiceTree(bc1), "ori.txt")

convp = (α_conv=cosd(5), ϵ_conv=1);
bc2 = dbgen2[:bestcached]
CSGB.tree2tex_orderfix(bc2, "bc2.txt", pcr1, extr2, convp, preamble=true)
tree2tex(toNiceTree(bc2), "or2.txt")

# #########
### quan
#########
bt1 = dbgen1[:besttree]
bt2 = dbgen2[:besttree]
rmse(bt1, pcr1.vertices)
rmse(bt2, pcr1.vertices)

depth(bt1)
treesize(bt1)

depth(bt2)
treesize(bt2)

rif1 = dbgen1[:runinfo]
rif1[:ransactimes]
rif1[:genetictimes]

rif2 = dbgen2[:runinfo]
rif2[:ransactimes]
rif2[:genetictimes]

edgel = (mincorner = -60, maxcorner = 260, edgelength = 150)

writeparaviewformat(bt1, "p4_ori", edgel)
writeparaviewformat(bt2, "p4_tr", edgel)
