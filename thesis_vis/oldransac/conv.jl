# works on RANSAC#2f6c318629111c7aa347054964773529f73a67d2
# ] add https://github.com/cserteGT3/RANSAC.jl#2f6c318629111c7aa347054964773529f73a67d2

using RANSAC, BSON, StaticArrays
println("give file:")
fn = readline()
db = BSON.load(fn)
nwed = Dict(:extr=>db[:extracted], :pc=>db[:pointcloud], :ri=>db[:runinfo])
bson("new_"*fn, nwed)
println("finished")
