## Cached examples that must be constructed by hand
using StaticArrays
using AbstractTrees
using RANSAC
using D3Trees
#using Revise
using CSGBuilding
const CSGB = CSGBuilding
using Logging

cd(@__DIR__)


## Hollow sphere

function hollow_cached()
    spc = CachedSurface("Sphere1", 1)
    plc = CachedSurface("Plane2", 2)
    cylc = CachedSurface("Cylinder3", 3)

    spcn = CachedCSGNode(spc, [], gensym())
    plcn = CachedCSGNode(plc, [], gensym())
    cylcn = CachedCSGNode(cylc, [], gensym())
    tr1 = CachedCSGNode(:intersection, [spcn, plcn], gensym())
    tr = CachedCSGNode(:subtraction, [tr1, cylcn], gensym())
    return tr
end

hsphere = hollow_cached()

h_ntr = toNiceTree(hsphere)
tree2tex(h_ntr, "hollow_sphere.tex", false)

## lock or what
function lock_cached()
    c1 = CachedCSGNode(CachedSurface("Cylinder1", 1), [], gensym())
    c2 = CachedCSGNode(CachedSurface("Cylinder2", 2), [], gensym())
    c3 = CachedCSGNode(CachedSurface("Cylinder3", 3), [], gensym())
    c4 = CachedCSGNode(CachedSurface("Cube4", 4), [], gensym())
    s5 = CachedCSGNode(CachedSurface("Sphere5", 5), [], gensym())

    n1 = CachedCSGNode(:intersection, [c4, s5], gensym())

    n2 = CachedCSGNode(:union, [c1, c2], gensym())
    n3 = CachedCSGNode(:union, [c3, n2], gensym())

    tr = CachedCSGNode(:subtraction, [n1, n3], gensym())
    return tr
end

cubec = lock_cached()

inchrome(D3Tree(cubec))
tree2tex(toNiceTree(cubec), "lock.tex", true)
