# every include
using LinearAlgebra
using StaticArrays
using Random
using Makie
using FileIO
using BSON
using Logging
using GeometryTypes
const deflog = global_logger()

using Revise
using RANSAC
using RANSACVisualizer
using CSGBuilding
const CSGB = CSGBuilding

bp = joinpath(dirname(@__DIR__), "thesis", "figures\\")
cd(@__DIR__)

fn = "Part4red.obj"
m = load(fn);
println("Mesh size: $(size(m.vertices,1))")
showgeometry(m)


#########
### pic 1
#########

sc = scatter(m.vertices, markersize=1, show_axis = false, resolution=(1920,1080))
sc.center=false
#save(bp*"ex1_input.png", sc)

#########
### ransac
#########

dbr1 = BSON.load("furcsa32keps10-v6_ransac.bson")
dbr2 = BSON.load("furcsa32ktransl-v1_ransac.bson")

dbgen1 = BSON.load("furcsa32keps10-v6_genetic.bson")
dbgen2 = BSON.load("furcsa32ktransl-v1_genetic.bson")

pcr1 = dbr1[:pointcloud]
extr1 = dbr1[:extracted]
extr2 = dbr2[:extracted]

#########
### pic 1
#########

sc = scatter(pcr1.vertices, markersize=0.05, show_axis = false, resolution=(1920,1080))
sc.center=false
save(bp*"m5_input.png", sc)

#########
### by type
#########

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showbytype!(sc, pcr1, extr1, markersize=0.1)
sc2.center=false
save(bp*"m5_segmented.png", sc2)

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showbytype!(sc, pcr1, extr2, markersize=0.1)
sc2.center=false
save(bp*"m5_segmented_tr.png", sc2)

#########
### diff
#########

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showshapes!(sc, pcr1, extr1, markersize=0.1)
sc2.center=false
#save(bp*"m5_segmentedcol.png", sc2)

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showshapes!(sc, pcr1, extr2, markersize=0.1)
sc2.center=false
#save(bp*"m5_segmentedcol_tr.png", sc2)

###################
## contour
###################

function interp(a, b, n)
    r = collect(range(0, stop=1, length=n+2))
    deleteat!(r, 1)
    deleteat!(r, lastindex(r))
    return [(1-i)*a+i*b for i in r]
end


trls = extr2[1].candidate.shape
c = trls.contour

plotcontour(trls, true, true)

s7_ = Scene(scale_plot=false, show_axis = false, resolution=(1920,1080))
sct1 = lines!(s7_, c, scale_plot=false, linewidth=1.75)
tms_ = [RANSAC.midpoint(c, i) for i in eachindex(c)]
tns_ = [dn2shape_outw(tms_[i],trls)[2] for i in eachindex(tms_)]
tms = tms_[1:2:end]
tns = tns_[1:2:end]
x_ = (x->x[1]).(tms)
y_ = (x->x[2]).(tms)
u_ = (x->x[1]).(tns)
v_ = (x->x[2]).(tns)
arrows!(s7_, x_, y_, u_, v_, scale_plot=false)
#arrows!(s7_, x_, y_, u_, v_, scale_plot=false, lengthscale=2f0, linewidth=1.5, arrowsize=2)
s7_.center=false
#save(bp*"furcsa_contnorm.png", s7_)

# #########
### parameters
#########
d = octreedepth(pcr)
extr1 |> length
dbgen1[:surfaces] |> length

extr2 |> length
dbgen2[:surfaces] |> length


genp1 = dbgen1[:params]
genp1.ϵ_d
genp1.α|>rad2deg
genp1.itermax

genp2 = dbgen2[:params]
genp2.ϵ_d
genp2.α|>rad2deg
genp2.itermax

obb = RANSAC.findOBB_(pcr1.vertices)
sl1 = norm(obb[1]-obb[2])
sl2 = norm(obb[1]-obb[3])
sl3 = norm(obb[1]-obb[5])
sls = [sl1, sl2, sl3]
obb_a = sqrt(sum(sls .^2))

# #########
### tree
#########

rp1 = dbr1[:params][1]
rp2 = dbr2[:params][1]

convp = (α_conv=cosd(5), ϵ_conv=rp1.ϵ_plane);
bc1 = dbgen1[:bestcached]
CSGB.tree2tex_orderfix(bc1, "bc5.txt", pcr1, extr1, convp, preamble=true)
tree2tex(toNiceTree(bc1), "ori5.txt")

convp = (α_conv=cosd(2), ϵ_conv=rp2.ϵ_plane);
bc2 = dbgen2[:bestcached]
CSGB.tree2tex_orderfix(bc2, "bc5.txt", pcr1, extr2, convp, preamble=true)
tree2tex(toNiceTree(bc2), "or5.txt")

# #########
### quan
#########
bt1 = dbgen1[:besttree]
bt2 = dbgen2[:besttree]
rmse(bt1, pcr1.vertices)
rmse(bt2, pcr1.vertices)

depth(bt1)
treesize(bt1)

depth(bt2)
treesize(bt2)

rif1 = dbgen1[:runinfo]
rif1[:ransactimes]
rif1[:genetictimes]

rif2 = dbgen2[:runinfo]
rif2[:ransactimes]
rif2[:genetictimes]

edgel = (mincorner = -8, maxcorner = 8, edgelength = 120)

writeparaviewformat(bt1, "m5_ori", edgel)
writeparaviewformat(bt2, "m5_tr", edgel)
