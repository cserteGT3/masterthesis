# every include
using LinearAlgebra
using StaticArrays
using Random
using Makie
using FileIO
using BSON
using Logging
using GeometryTypes
const deflog = global_logger()

using Revise
using RANSAC
using RANSACVisualizer
using CSGBuilding
const CSGB = CSGBuilding

bp = joinpath(dirname(@__DIR__), "thesis", "figures\\")
cd(@__DIR__)

fn = "icsg_lockcsg2.obj"
m = load(fn);
println("Mesh size: $(size(m.vertices,1))")
showgeometry(m)


#########
### pic 1
#########

sc = scatter(m.vertices, markersize=0.05, show_axis = false, resolution=(1920,1080))
sc.center=false
#save(bp*"lock_input.png", sc)

#########
### ransac
#########

db = BSON.load("icsg_lockcsg2wo-v1_ransac.bson")
dbgen = BSON.load("icsg_lockcsg2wo-v1_genetic.bson")
p = db[:params]
pcr = db[:pointcloud]
extr = db[:extracted]

#########
### by type
#########

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showbytype!(sc, pcr, extr, markersize=0.07)
sc2.center=false
#save(bp*"lock_segmented.png", sc2)

#########
### diff
#########

sc = Scene(show_axis = false, resolution=(1920,1080))
sc2 = showshapes!(sc, pcr, extr, markersize=0.07)
sc2.center=false
#save(bp*"lock_segmentedcol.png", sc2)

# #########
### parameters
#########
d = octreedepth(pcr)
dbgen[:surfaces] |> length
extr |> length

genp = dbgen[:params]
genp.ϵ_d
genp.α|>rad2deg

# #########
### tree
#########

bc = dbgen[:bestcached]
tree2tex(toNiceTree(bc), "lock.txt")

# #########
### quan
#########
bt = dbgen[:besttree]
rmse(bt, pcr.vertices)

rif = dbgen[:runinfo]
rif[:ransactimes]
rif[:genetictimes]
depth(bt)
treesize(bt)

convp = (α_conv=cosd(2), ϵ_conv=p[1].ϵ_plane);
tree2tex(toNiceTree(bc), "lock2.txt")
CSGB.tree2tex_orderfix(bc, "lock3.txt", pcr, extr, convp, preamble=true)


obb = RANSAC.findOBB_(pcr.vertices)
sl1 = norm(obb[1]-obb[2])
sl2 = norm(obb[1]-obb[3])
sl3 = norm(obb[1]-obb[5])
sls = [sl1, sl2, sl3]
obb_a = sqrt(sum(sls .^2))

# #########
### mod
#########
using Setfield
edgel = (mincorner = -10, maxcorner = 10, edgelength = 120)

# cylinder 3
bt.children[1].children[2].children[1].children[2].data

bt_ = @set bt.children[1].children[2].children[1].children[2].data.radius = 0.5

# cylinder 2
bt.children[1].children[1].children[1].children[2].children[1].children[1].data

bt__ = @set bt_.children[1].children[1].children[1].children[2].children[1].children[1].data.radius=4

# sphere 1
bt.children[1].children[1].children[1].children[1].children[1].children[1].children[1].children[1].children[2].data

bt2 = @set bt__.children[1].children[1].children[1].children[1].children[1].children[1].children[1].children[1].children[2].data.radius=7


writeparaviewformat(bt, "lock_ori", edgel)
writeparaviewformat(bt2, "lock_mod", edgel)
