# This file is only for testing the meshes and the parameters
using LinearAlgebra
using Logging
using Random
using StaticArrays
using D3Trees
using BSON
using FileIO
using Makie
using UnionFind
using NearestNeighbors

using Revise
using CSGBuilding
using RANSAC
using RANSACVisualizer
const CSGB = CSGBuilding
const deflog = global_logger()
global_logger(nosource_IterInflog())
global_logger(nosource_IterLow1log())

cd(@__DIR__)
bp = joinpath(dirname(@__DIR__), "thesis", "figures\\")


fn = "Part516k.obj"
fmesh = load(fn);
Random.seed!(4321)
newv = RANSAC.noisifyv_fixed(fmesh.vertices, true, 0.5)
sc = scatter(newv, markersize=1.5, show_axis = false)
sc.center=false
#save(bp*"transl_input.png", sc)
cam3d!(sc)
println("mesh size: ", size(fmesh.vertices,1))

pcr = PointCloud(newv, fmesh.normals, 4);

p = RANSACParameters{Float64}(ϵ_plane=1,
                                α_plane=deg2rad(3),
                                ϵ_sphere=1,
                                α_sphere=deg2rad(3),
                                ϵ_cylinder=1.2,
                                α_cylinder=deg2rad(5),
                                ϵ_cone=0.2,
                                α_cone=deg2rad(1),
                                ϵ_transl=3,
                                α_transl=deg2rad(5),
                                α_perpend = cosd(89),
                                minsubsetN=15,
                                itermax=500,
                                τ=500,
                                #shape_types=[:translational_surface],
                                shape_types=[:sphere, :plane, :cylinder, :cone, :translational_surface],
                                #shape_types=[:plane, :sphere, :cylinder, :cone],
                                thinning_par=1.5,
                                force_transl=false,
                                thin_method=:slow,
                                min_normal_num=0.8,
                                jumpback=true,
                                prob_det=0.9);

cand, extr, rtime = ransac(pcr, p, true, reset_rand=true);

showtype(extr)
println("Left: $(count(pcr.isenabled)) of $(pcr.size)")

showbytype(pcr, extr, markersize=2)
showshapes(pcr, extr, markersize=2)

scatter(pcr.vertices[pcr.isenabled], markersize=1)

scatter(pcr.vertices[extr[1].inpoints])

###################
#globals
###################

eshape = extr[1].candidate.shape
cf = eshape.coordframe


###################
## pic 2
## normal direction
###################
sd = [15287, 6483, 15737];
dirok, nv = RANSAC.transldir(pcr.vertices[sd], pcr.normals[sd], p)

sc = scatter(newv, markersize=1, show_axis = false, resolution=(1920,1080))
sc.center=false
scatter!(sc, pcr.vertices[sd], color=:blue, markersize=8)
arrows!(sc, Point3f0.(pcr.vertices[sd]), Point3f0.(pcr.normals[sd]), arrowsize=5, lengthscale=25.0f0, linewidth=5, arrowcolor=:green)

p1 = pcr.vertices[sd[1]]
xv = cf[1];
yv = cf[2];
niceplane = [p1+20*xv+10*yv, p1-120*xv-60*yv, p1+60*xv-120*yv]
niceplane2 = [p1-90*xv-180*yv, p1-120*xv-60*yv, p1+60*xv-120*yv]
mesh!(sc, niceplane2, markersize=15, color=(:yellow, 50), shading=false, transparency=true)
mesh!(sc, niceplane, markersize=15, color=(:yellow, 50), shading=false, transparency=true)
mp = sum(unique(vcat(niceplane, niceplane2)))/4
#scatter!(sc, [mp], color=:green, markersize=15)
arrows!(sc, Point3f0.([mp]), Point3f0.([cf[3]]), arrowsize=6, lengthscale=75.0f0, linewidth=6, arrowcolor=:red)
#save(bp*"transl_transldir.png", sc)

###################
## pic 3
## projected points
###################

# 3204 points
proj1, _ = RANSAC.project2sketchplane(pcr, pcr.subsets[1], cf, p)
sc1 = scatter(proj1, scale_plot=false, show_axis = false, resolution=(1920,1080), markersize=0.5)
sc1.center=false
#save(bp*"transl_proj1.png", sc1)

# 12679 points
projall, _ = RANSAC.project2sketchplane(pcr, vcat(pcr.subsets...), cf, p)
sc2 = scatter(projall, scale_plot=false, show_axis = false, resolution=(1920,1080), markersize=0.5)
sc2.center=false
#save(bp*"transl_projall.png", sc2)

###################
## pic 4
## next step
###################
sc1 = scatter(proj1, scale_plot=false, show_axis = false, resolution=(1920,1080), markersize=0.5)
sc1.center=false
drawcircles!(sc1, proj1, p.ϵ_transl, strokecolor=:blue)
#save(bp*"transl_circled.png", sc1)

compf = RANSAC.segmentpatches(proj1, p.ϵ_transl)
g1 = proj1[findall(x->x==1, compf.ids)]
g2 = proj1[findall(x->x==2, compf.ids)]
s = scatter(g1, scale_plot=false, show_axis = false, resolution=(1920,1080), markersize=0.5, color=:green)
scatter!(s, g2, scale_plot=false, show_axis = false, resolution=(1920,1080), markersize=0.5, color=:red)
s.center=false
#save(bp*"transl_segmented.png", s)

###################
## pic 4
## thinning
###################


s0_ = Scene(scale_plot=false, show_axis = false, resolution=(1920,1080))
scatter!(s0_, g1, markersize=0.5)
save(bp*"thin_in.png", s0_)


#g1 is the contour
tris = RANSAC.delaunay(g1);
all_edges = RANSAC.to_edges!(tris);
weights = [norm(g1[e[1]] - g1[e[2]]) for e in all_edges];
tree = RANSAC.spanning_tree(all_edges, weights);
thinned, chunks = RANSAC.thinning(g1, tree, p.thinning_par);

sc2_ = wframe(g1, tris, scale_plot=false, show_axis = false, resolution=(1920,1080))
#sc2 = title(sc2_, "Triangulation")
#save(bp*"transl_delaunay.png", sc2_)

s3_ = Scene(scale_plot=false, show_axis = false, resolution=(1920,1080))
#sc3 = title(sc3_, "Minimum spanning tree")
plotspantree!(s3_, g1, tree, color=:black, linewidth=1.05)
#save(bp*"transl_spantree.png", s3_)

s4_ = Scene(scale_plot=false, show_axis = false, resolution=(1920,1080))
lines!(s4_,(x->x[1]).(thinned), (x->x[2]).(thinned), color=:black, linewidth=1.5)
#save(bp*"transl_polyline.png", s4_)



###################
## pic final
## result
###################
impls = [CSGNode(CSGB.toimplicit(e.candidate.shape), []) for e in extr]

minV, maxV = findAABB(pcr.vertices)
minc = minimum(minV)
maxc = maximum(maxV)

tr = impls[1]

tr1 = CSGNode(CSGB.intersection, [impls[1], impls[3]])
tr2 = CSGNode(CSGB.intersection, [tr1, impls[4]])
trf = CSGNode(CSGB.subtraction, [tr2, impls[2]])
edgel = (mincorner=minc-5, maxcorner=maxc+5, edgelength=110);
writeparaviewformat(trf, "Part5_hand", edgel)

tree2tex(toNiceTree(trf), "Part5.tex")

using BSON

bson("Part5res.bson", Dict(:tree=>trf, :res=>extr, :pc=>pcr))

dbp5 = BSON.load("Part5res.bson")
extr = dbp5[:res]
impltr = toimplicit(extr[1].candidate.shape)

###################
## pic6 final - pw
## result
###################

impln = CSGNode(impltr, [])

edgel = (mincorner=-120, maxcorner=150, edgelength=110);
writeparaviewformat(impln, "only5", edgel)

printlN("dont forget the control")


###################
## pic7
## normals
###################

function interp(a, b, n)
    r = collect(range(0, stop=1, length=n+2))
    deleteat!(r, 1)
    deleteat!(r, lastindex(r))
    return [(1-i)*a+i*b for i in r]
end


trls = extr[1].candidate.shape
c = trls.contour

plotcontour(trls, true, true)

s7_ = Scene(scale_plot=false, show_axis = false, resolution=(1920,1080))
sct1 = lines!(s7_, c, scale_plot=false)
tms_ = [RANSAC.midpoint(c, i) for i in eachindex(c)]
tns_ = [dn2shape_outw(tms_[i],trls)[2] for i in eachindex(tms_)]
tms = tms_[1:2:end]
tns = tns_[1:2:end]
x_ = (x->x[1]).(tms)
y_ = (x->x[2]).(tms)
u_ = (x->x[1]).(tns)
v_ = (x->x[2]).(tns)
arrows!(s7_, x_, y_, u_, v_, scale_plot=false, lengthscale=7f0, linewidth=1.5, arrowsize=2)
#save(bp*"transl_normals.png", s7_)
