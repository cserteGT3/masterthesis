using StaticArrays
using AbstractTrees
using RANSAC
using D3Trees
#using Revise
using CSGBuilding
const CSGB = CSGBuilding
using Logging

cd(@__DIR__)

function makeit()
    sp = ImplicitSphere([0.0,0,0], 4.5)
    pl = ImplicitPlane([0.0,0,0], [0,1,0])
    cyl = ImplicitCylinder([0,0,1], [0,-2.5,0], 1.5)

    spn = CSGNode(sp, [])
    spn_c = CachedCSGNode(CachedSurface("Sphere1", 1), [], gensym())
    pln = CSGNode(pl, [])
    pln_c = CachedCSGNode(CachedSurface("Plane2", 2), [], gensym())
    cyln = CSGNode(cyl, [])
    cyln_c = CachedCSGNode(CachedSurface("Cylinder3", 3), [], gensym())

    tr1 = CSGNode(CSGB.intersection, [spn, pln])
    tr1_c = CachedCSGNode(:intersection, [spn_c, pln_c], gensym())
    tr = CSGNode(CSGB.subtraction, [tr1, cyln])
    tr_c = CachedCSGNode(:subtraction, [tr1_c, cyln_c], gensym())
    return tr, [sp, pl, cyl], tr_c
end

wtr, surfs, wtr_c = makeit();
edgel = (mincorner=-5, maxcorner=5, edgelength=110);

# writeparaviewformat(wtr, "wtr", edgel)

function testwtr(p, n, surfac, iters; kwargs...)
    pari = CSGGeneticBuildParameters{Float64}(itermax=iters; kwargs...)
    @info "cachedfuncgeneticbuildtree with $iters iterations."
    return cachedfuncgeneticbuildtree(surfac, p, n, pari)
end

vsw, nsw = readobj("wtr.obj", edgel);

#=
using FileIO
using GeometryTypes
using RANSACVisualizer
using Makie

m = load("wtr.obj");
mv = vertices(m)

mv2 = mv ./ (edgel.edgelength-1)
mv2 = mv2.*(abs(edgel.mincorner)+abs(edgel.maxcorner))

sc = plotimplshape(surfs[1])
plotimplshape!(sc, surfs[2], color=(:red, 0.2), scale = (10., 10.))
plotimplshape!(sc, surfs[3], color=(:green, 0.2), scale=10)
scatter!(sc, mv2[1:2:end])
scatter!(sc, vsw[1:2:end])
=#

# test run
alls, bestt = testwtr(vsw, nsw, surfs, 2, maxdepth=7);

# real run
alls, bestt = testwtr(vsw, nsw, surfs, 3000, maxdepth=7);

writeparaviewformat(bestt, "bestwtr", edgel)

tofile(D3Tree(alls[1]), "holedsphere.html")

println("fully finished")

## code generation

function dumpcode(cwrap)
    expr = Expr(:function,
        Expr(:tuple,
        cwrap.Params[1],
        cwrap.Params[2]),
        Expr(:block,cwrap.Core...))
    return expr
end

wtrf = CSGB.tree2code(wtr_c)
tf = CSGB.tree2func(wtr_c)
d = dumpcode(wtrf)
d2 = :(function (var"##409", var"##410")
    var"##404" = (CSGBuilding.evalf)(Cylinder3, var"##409", var"##410")
    var"##403" = (CSGBuilding.evalf)(Plane2, var"##409", var"##410")
    var"##402" = (CSGBuilding.evalf)(Sphere1, var"##409", var"##410")
    var"##405" = (CSGBuilding.inters)(var"##402", var"##403")
    var"##406" = (CSGBuilding.subtr)(var"##405", var"##404")
    return var"##406"
    end)


println("dummy line")
#=
julia> d
:(function (var"##409", var"##410")
      var"##404" = (CSGBuilding.evalf)(Cylinder3, var"##409", var"##410")
      var"##403" = (CSGBuilding.evalf)(Plane2, var"##409", var"##410")
      var"##402" = (CSGBuilding.evalf)(Sphere1, var"##409", var"##410")
      var"##405" = (CSGBuilding.inters)(var"##402", var"##403")
      var"##406" = (CSGBuilding.subtr)(var"##405", var"##404")
  end)

  julia> d
  :(function (var"##409")
        var"##404" = (CSGBuilding.evalf)(Cylinder3, var"##409")
        var"##403" = (CSGBuilding.evalf)(Plane2, var"##409")
        var"##402" = (CSGBuilding.evalf)(Sphere1, var"##409")
        var"##405" = (CSGBuilding.inters)(var"##402", var"##403")
        var"##406" = (CSGBuilding.subtr)(var"##405", var"##404")
        return var"##406"
    end)

\begin{algorithm}[H]
	\caption{Generated code for the hollow sphere model}
	\begin{algorithmic}[1]
		\STATE \texttt{  :(function (var"\#\#409")}
		\STATE \texttt{var"\#\#404" = (CSGBuilding.evalf)(Cylinder3, var"\#\#409")}
		\STATE \texttt{var"\#\#403" = (CSGBuilding.evalf)(Plane2, var"\#\#409")}
		\STATE \texttt{var"\#\#402" = (CSGBuilding.evalf)(Sphere1, var"\#\#409")}
		\STATE \texttt{var"\#\#405" = (CSGBuilding.inters)(var"\#\#402", var"\#\#403")}
		\STATE \texttt{var"\#\#406" = (CSGBuilding.subtr)(var"\#\#405", var"\#\#404")}
		\STATE \texttt{return var"\#\#406"}
		\STATE \texttt{end)}
	\end{algorithmic}
	\label{alg:gencode}
\end{algorithm}

=#
