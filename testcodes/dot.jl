using LinearAlgebra
using Makie
using StaticArrays

mv = SVector(0.5, 0.1)

rvs = [SVector{2,Float32}(2*rand(2) .-1) for i in 1:100]


pp = [dot(mv, v) < 0 for v in rvs]

yes = rvs[pp]
no = rvs[.~pp]

sc = scatter([SVector(0.,0)], scale_plot=false)
arrows!(sc, fill(0, size(yes)), fill(0, size(yes)), (x->x[1]).(yes), (x->x[2]).(yes), arrowcolor=:blue, scale_plot=false, arrowsize=0.1)
arrows!(sc, fill(0, size(no)), fill(0, size(no)), (x->x[1]).(no), (x->x[2]).(no), arrowcolor=:red, scale_plot=false, arrowsize=0.1)
arrows!(sc, [0], [0], [mv[1]], [mv[2]], arrowcolor=:green, scale_plot=false, arrowsize=0.2)
