%----------------------------------------------------------------------------
\chapter{\bevezetes}
\label{sec:intro}
%----------------------------------------------------------------------------

The aim of reverse engineering -- as opposed to classic engineering -- is to reconstruct a digital representation of an existing object.
Importance of digital models cannot be overemphasized, and one focus of the fourth industrial revolution (Industry 4.0) is on virtualisation of whole industrial processes.
In both design and production, computer-aided tools are widely used (CAD/CAM -- \emph{Computer Aided Design/Manufacturing}).
There are cases, however, when a digital model is not available.
Causes could be manufacturing conditions (manual finishing), changes resulting from use (repair of worn parts), or lack of any digital model (parts manufactured before the digital era or legal/commercial restrictions).
If in these cases a digital model is required, measurement-based reconstruction techniques are used.
These methods are referred to as digital shape reconstruction or reverse engineering.

%TODO: measurements
The first step of the reconstruction process is to measure the existing object.
Several different instruments are available for this purpose, but the common characteristic is that the measurement is never perfectly accurate.
A recorded point cloud is potentially noisy, and may contain outliers that are not part of the object in real life.
These have the consequences that any method that processes these measured point clouds must handle these imperfections robustly.

%TODO: organic vs enginnered

\begin{figure}[h]
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.65\textwidth]{bunny-scanalyze-sh}
		\caption{Stanford bunny~\cite{bunny}}
		\label{fig:bunny}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.5\textwidth]{engineered}
		\caption{An engineered object~\cite{fayolle16}}
		\label{fig:engineered}
	\end{subfigure}
	\caption{Examples of freeform and engineered objects}
	\label{fig:freeformvsengineered}
\end{figure}

In reverse engineering, two main types of objects are distinguished: organic and engineered.
As shown in Fig.~\ref{fig:bunny}, an organic object cannot be described by composing simple shapes (such as planes, spheres, cylinders), while engineered objects (Fig.~\ref{fig:engineered}) are built up mostly from such primitives.
There is a major difference in the reconstruction of the two types of objects.
For organic objects, ``beauty" is more important than precision, namely surface smoothness is preferred over small inaccuracies.
However, when we consider engineered objects, precision is the most important aspect.
Accurate reconstruction is facilitated by recovering the design intent behind the part, such as recognizing symmetries, parallelism and other constraints used in design.

%TODO: fig for CSG-tree
%TODO: more on CSG-trees - why is it good, etc.
Another way of exploring the design intent is to rebuild the construction tree of the object, which partially refers to the model history.
This is also referred to as CSG-(Constructive Solid Geometry) or construction tree.
In this representation leaves are primitive shapes (such as cubes, spheres, tori, etc.) and nodes are set-operations (union, intersection, etc.).

\begin{figure}[h]
	\centering
	\includegraphics[width=0.45\textwidth]{Csg_tree}
	\caption{Example of a CSG-tree~\cite{csgtree}}
	\label{fig:csgtree}
\end{figure}

Parametrizing the primitives always results in a valid model and their parameters are intuitive to use.
Without such a tree, it is much more difficult to use or modify the reconstructed model.
The reconstruction of CSG-trees is still considered to be experimental and it is not yet used in production.

%TODO: methodology: how?
A broad range of techniques and algorithms are available te reconstruct engineered objects.
While all papers on this topic concentrate on accurate reconstruction, only a few of them tries to explore the designer intentions.
Available techniques first search for primitives in the input point cloud, then to build the CSG-tree, they use some kind of optimization technique; evolutionary approach, program synthesis or binary optimization just to name a few of them.
In this thesis we explore the possibility of adding a new primitive to the list, the translational surface.
We decided to omit rotational surfaces, as recognition of such shapes would deserve its own separate work.

Based on~\cite{fayolle16}, we chose the efficient RANSAC algorithm to detect primitives in the input point cloud, and genetic optimization to construct the CSG-tree.
The embedding of our translational surface representation into these two algorithms is also described.
%We will describe how our translational surface representation is embedded into these two algorithms.
Despite the fact that there are open-source libraries for both RANSAC and genetic algorithms, we have decided to reimplement these (in Julia~\cite{julia}), as in this way we have a better control over the processes, and it is easier to augment them with a new surface type.

The outline of the thesis is as follows: in Chapter~\ref{sec:litrev} relevant literature is discussed, focusing on the reconstruction of engineered objects.
In Chapter~\ref{sec:preliminaries} explanation and implementation of state-of-the-art algorithms are given, then in Chapter~\ref{sec:translational_surfs} the problem of reconstruction of translated surfaces is addressed.
Related results are presented in Chapter~\ref{sec:results}, and future work and possibilities are discussed in Chapter~\ref{sec:conclusion}.

%Based on~\cite{VARADY1997} reconstruction process is partitioned into four steps: data capture, pre-processing, segmentation and surface fitting and CAD model creation.
