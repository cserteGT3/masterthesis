%----------------------------------------------------------------------------
\chapter{Results}
\label{sec:results}
%----------------------------------------------------------------------------
We evaluate the performance of our method from different points of view.
First we show that our implementation can replicate state-of-the-art functionality, by going through the algorithm step-by-step and showing the interim results.
Then two use-cases will be introduced, where the use of translational surfaces enhance the result of the reconstruction.
All of our tests use synthetic inputs, designed in a CAD software, then exported as a triangulated mesh.
From the exported models we only used the vertices and surface normals, as input of our approach is always a point cloud.
All of the experiments ran on a computing node with an Intel i9-7960X (16 cores) processor and $128$ GB of RAM.
However, multi-threading was not utilized during the experiments.

Quantitative evaluation of the results includes the number of nodes and the depth of the resulting tree, the computation times, and the root-mean-square error (RMSE) of the resulted model.
This latter means that for all points in the input cloud, the squared distance from the surface is computed, then their average is computed (Eq.~\ref{eq:rmse}).
The length of the diagonal of the oriented bounding box is also calculated for scale.

\begin{equation}
\label{eq:rmse}
RMSE = \sqrt{\frac{\sum_{i=1}^{N} d_i^2}{N}}
\end{equation}

\newpage
\section{Example for the reconstruction process}
\label{sec:result_lockcsg}
To give an overview of the complete reconstruction process we recreated a frequently used example.
The input point cloud is shown in Fig.~\ref{fig:model2} with a rendered image, as well.

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{lock_input}
		\caption{Point cloud ($18616$ points)}
		\label{fig:m2_pc}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.91\textwidth]{lock_render}
		\caption{Rendered image}
		\label{fig:m2_render}
	\end{subfigure}
	\caption{\emph{Model 2}}
	\label{fig:model2}
\end{figure}

\subsection{Result of segmentation and fitting}
\label{sec:lock_ransac}
Figure~\ref{fig:lock_ransac} shows the result of the segmentation with the RANSAC paradigm.
In this example we considered plane, sphere, cylinder and cone primitives.
As it can be seen on the figures, every primitive is properly extracted.
The parameters used for the segmentation are listed in Table~\ref{tab:m2pars}.

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{lock_segmented}
		\caption{Coloured by type}
		\label{fig:m2_segm}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{lock_segmentedcol}
		\caption{Distinct colour for each primitive}
		\label{fig:m2_racol}
	\end{subfigure}
	\caption{Result of segmentation of \emph{Model 2}}
	\label{fig:lock_ransac}
\end{figure}


\begin{table}[h]
	\centering
	\caption{Parameters for the segmentation of \emph{Model 2}. (for notations see Table~\ref{tab:ransacpars})}
	\begin{tabular}{ c | c | c | c }
		Parameter & Description & Parameter & Description \\
		\hline
		\hline
		$N$ & $18616$ & $p_t$ & $90\%$\\
		$\tau$ & $500$ & $k$ & $3$ \\
		$d$ & $5$& $t$ & $15$ \\
		$r$ & $4$ & - & - \\
		$\epsilon_{plane}$ & $0.2$ & $\alpha_{plane}$  & $3^{\circ}$\\
		$\epsilon_{sphere}$ & $0.1$ & $\alpha_{sphere}$  & $3^{\circ}$\\
		$\epsilon_{cylinder}$ & $0.3$ & $\alpha_{cylinder}$  & $3^{\circ}$\\
		$\epsilon_{cone}$ & $0.2$ & $\alpha_{cone}$  & $1^{\circ}$\\
	\end{tabular}
	\label{tab:m2pars}
\end{table}

\subsection{Genetic optimization}
\label{sec:m2_genetic}
After computing the separating primitives, the list of primitives grows from $10$ to $20$.
This is expected, as for each cylinder and sphere primitives the oriented bounding box is computed and its planes are added to the list of primitives (without duplications).
%TODO ezt nem tudom megmutatni. le kell írni, hogy csúnya lenne megmutatni?

With the separating primitives defined, we ran the genetic optimization for $4000$ iterations.
Corresponding parameters are listed in Table~\ref{tab:m2_gentab}.

\begin{table}[H]
	\centering
	\caption{Parameters for the genetic optimization of \emph{Model 2} (for notations see Table~\ref{tab:geneticpars})}
	\begin{tabular}{ c | c | c | c }
		Parameter & Description & Parameter & Description \\
		\hline
		\hline
		$\epsilon_{d}$ & $0.1$ & $\alpha$  & $10^{\circ}$\\
		$\chi$ & $0.3$ & $\mu$ & $0.3$ \\
		$\mu_0$ & $0.3$ & Max. tree depth & $10$ \\
		Population size & $150$ & Number of iterations & $4000$ \\
		Best kept & $2$ & Tournament selection size & $30$ \\
		$p_{tournament}$ & $0.5$ & - & - \\
	\end{tabular}
	\label{tab:m2_gentab}
\end{table}

The resulting tree is shown in Fig.~\ref{fig:tree_m2}.

% ex1bc.txt
\begin{figure}[H]
    \centering
    \scalebox{.8}{
	\begin{forest}
		[$\sim$ [$\cup$ [$\cup$ [$\cup$ [$\cup$ [$\cup$ [$\cup$ [$\sim$ [$\cap$ [$\sim$ [Cylinder4 ]] [Sphere1 ]]] [$\sim$ [Plane7 ]]] [$\sim$ [Plane9 ]]] [$\sim$ [Plane6 ]]] [$\sim$ [$\sim$ [Cylinder2 ]]]] [$\sim$ [$\cap$ [Plane10 ] [Plane5 ]]]] [$\sim$ [$-$ [Plane8 ] [Cylinder3 ]]]]]
	\end{forest}}
	\caption{CSG tree of \emph{Model 2}}
	\label{fig:tree_m2}
\end{figure}

Rendering the zero level-set of the resulting implicit surface results in Figure~\ref{fig:m2_modo}.

Quantitative results are summarized in Table~\ref{tab:m2_quan}.

\begin{table}[H]
	\centering
	\caption{Quantitative results of reconstructing \emph{Model 2}}
	\begin{tabular}{ c | c }
		& \emph{Model 2} \\
		\hline
		\hline
		RANSAC run time & $24.11 \ [ s ] $ \\
		\# Segmented primitives & $10$ \\
		\# Segmented \& separating primitives & $20$ \\
		Genetic optimization run time & $ 9.37 \ [ h ] $ \\
		Tree depth & $10$ \\
		Number of nodes and leaves & $29$ \\
		OBB diagonal & $17.392$ \\
		RMSE & $0.0515$ \\
	\end{tabular}
	\label{tab:m2_quan}
\end{table}

\subsection{Modification of reconstructed model}
\label{sec:m2_mod}
As described in our objectives, we not only want to reconstruct a model, but also be able to modify its parameters.
This ability is shown in Fig.~\ref{fig:m2_mod}.
The followings were modified: radius of \emph{Cylinder3} from $R2.57$ to $R0.5$, radius of \emph{Cylinder3} from $R2.54$ to $R4.0$ and radius of \emph{Sphere1} from $R6.26$ to $R7$.
In Fig.~\ref{fig:m2_modm}, next to the originally reconstructed model, we show the modified one.

\begin{figure}[H]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{lock_finish}
		\caption{Reconstructed model}
		\label{fig:m2_modo}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{lock_mod}
		\caption{Modified model}
		\label{fig:m2_modm}
	\end{subfigure}
	\caption{Modification of \emph{Model 2}}
	\label{fig:m2_mod}
\end{figure}

\section{Reconstruction with translational surfaces}
\label{sec:res_transl}
In this section we give an example for the case when an additional primitive type (the translational surface) enhances the result of the reconstruction.
For an example, we replicated one of the models from~\cite{fayolle16}.
The input point cloud is shown in Fig.~\ref{fig:ex1_input}, along with a rendered image.
We chose this model (\emph{Model 3}), because it is clearly visible that it can be constructed by extruding a profile curve.
Description of the reconstruction process follows the above-described example.
The two cases -- with and without the translational primitive -- will be shown side-by-side.

\begin{figure}[H]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.6\textwidth]{ex1_input}
		\caption{Point cloud (34894 points)}
		\label{fig:ex1_pc}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.6\textwidth]{ex1_pw}
		\caption{Rendered image}
		\label{fig:ex1pw}
	\end{subfigure}
	\caption{\emph{Model 3}}
\label{fig:ex1_input}
\end{figure}

\subsection{Result of segmentation and fitting}
\label{sec:ex1_ransac}
Figure~\ref{fig:ex1_ransac} shows the result of the segmentation and fitting.
We considered plane, sphere, cylinder and cone primitives (top row) and the additional translational primitive (bottom row).
As can be seen on the figures every primitive is properly extracted.
The parameters used for the segmentation are listed in Table~\ref{tab:ex1pars}.
As we were able to extract the outer surface of the object in one primitive, the number of primitives almost halved.
Despite the smaller number of primitives to be extracted, the time of the segmentation is still $59\%$ larger (details in Table~\ref{tab:ex2_quan}).
In Fig.~\ref{fig:m3_contnormal} the contour of the translational surface is shown, with corresponding surface normals.

\begin{figure}[H]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.85\textwidth]{ex1_segmented}
		\caption{Coloured by type}
		\label{fig:ex1_segm}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.85\textwidth]{ex1_segmentedcol}
		\caption{Distinct colour for each primitive}
		\label{fig:ex1_racol}
	\end{subfigure}
	\vfill
		\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.85\textwidth]{ex1_segmented_tr}
		\caption{Coloured by type -- with translational primitive}
		\label{fig:ex1_segm_transl}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.85\textwidth]{ex1_segmentedcol_tr}
		\caption{Distinct colour for each primitive -- with translational primitive}
		\label{fig:ex1_racol_transl}
	\end{subfigure}
	\caption{Result of segmentation of \emph{Model 3}}
	\label{fig:ex1_ransac}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{ex1_contnorm}
	\caption{Contour and surface normals of \emph{Model 3}}
	\label{fig:m3_contnormal}
\end{figure}

\begin{table}[h]
	\centering
	\caption{Parameters for the segmentation of \emph{Model 3}. (for notations see Table~\ref{tab:ransacpars})}
	\begin{tabular}{ c | c | c | c }
		Parameter & Description & Parameter & Description \\
		\hline
		\hline
		$N$ & $34894$ & $p_t$ & $90\%$\\
		$\tau$ & $500$ & $k$ & $3$ \\
		$d$ & $5$& $t$ & $15$ \\
		$r$ & $4$ & - & - \\
		$\epsilon_{plane}$ & $0.5$ & $\alpha_{plane}$  & $5^{\circ}$\\
		$\epsilon_{sphere}$ & $0.2$ & $\alpha_{sphere}$  & $1^{\circ}$\\
		$\epsilon_{cylinder}$ & $0.8$ & $\alpha_{cylinder}$  & $5^{\circ}$\\
		$\epsilon_{cone}$ & $0.2$ & $\alpha_{cone}$  & $1^{\circ}$\\
		$\epsilon_{translational}$ & $3.5$ & $\alpha_{translational}$  & $5^{\circ}$\\
		$\alpha_{perpend}$  & $89.5^{\circ}$ & Thinning parameter & $\nicefrac{\epsilon_{translational}}{3}$\\
		Agreeing normals & $80\%$ & - & - \\
	\end{tabular}
	\label{tab:ex1pars}
\end{table}

\subsection{Genetic optimization}
\label{sec:ex1_genetic}
We computed the separating primitives for both cases, which changed the number of primitives from $15$ to $51$ and from $8$ to $35$.
With the separating primitives defined, we ran the genetic optimization for $5000$ iterations.
Corresponding parameters are listed in Table~\ref{tab:ex1_gentab}.

\begin{table}[H]
	\centering
	\caption{Parameters for the genetic optimization of \emph{Model 3}. (for notations see Table~\ref{tab:geneticpars})}
	\begin{tabular}{ c | c | c | c }
		Parameter & Description & Parameter & Description \\
		\hline
		\hline
		$\epsilon_{d}$ & $0.01$ & $\alpha$  & $10^{\circ}$\\
		$\chi$ & $0.3$ & $\mu$ & $0.3$ \\
		$\mu_0$ & $0.3$ & Max. tree depth & $10$ \\
		Population size & $150$ & Number of iterations & $5000$ \\
		Best kept & $2$ & Tournament selection size & $30$ \\
		$p_{tournament}$ & $0.5$ & - & - \\
	\end{tabular}
	\label{tab:ex1_gentab}
\end{table}

The resulting trees are shown on Fig.~\ref{fig:tree_m3} and Fig.~\ref{fig:tree_m3tr}.

\begin{figure}[H]
	\centering
	\begin{forest}
		[$-$ [$-$ [$\cup$ [$\sim$ [$\cap$ [Plane8 ] [Plane51 ]]] [$-$ [$\cap$ [$-$ [$\cap$ [$-$ [$\cap$ [$-$ [Plane6 ] [Cylinder4 ]] [Plane7 ]] [Cylinder10 ]] [Plane19 ]] [Cylinder11 ]] [Plane14 ]] [Cylinder13 ]]] [Cylinder1 ]] [$\cup$ [$\sim$ [$-$ [$\cap$ [$-$ [$\sim$ [$\cap$ [Cylinder12 ] [Plane21 ]]] [Cylinder3 ]] [Plane9 ]] [Cylinder5 ]]] [$\sim$ [$\sim$ [$\cup$ [$\sim$ [$\cap$ [Plane8 ] [Plane51 ]]] [Cylinder2 ]]]]]]
	\end{forest}
	\caption{Constructed tree without translational primitive}
	\label{fig:tree_m3}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{forest}
		[$\cap$ [$\cup$ [$\cap$ [$\cup$ [$\sim$ [$\cup$ [$\cup$ [$\cup$ [Cylinder5 ] [Cylinder6 ]] [Cylinder2 ]] [$-$ [Plane8 ] [$-$ [$-$ [Plane7 ] [Cylinder4 ]] [$\cap$ [Plane19 ] [$\cap$ [Cylinder3 ] [Plane10 ]]]]]]] [$\cap$ [Plane7 ] [$\cap$ [Plane24 ] [Plane18 ]]]] [Transl1 ]] [$-$ [Plane7 ] [$-$ [Plane7 ] [$\sim$ [Plane8 ]]]]] [$\cup$ [Plane8 ] [Plane12 ]]]
	\end{forest}
	\caption{Constructed tree with translational primitive}
	\label{fig:tree_m3tr}
\end{figure}

Rendering the zero level-set of the resulting implicit surface results in Figure~\ref{fig:m3_render}.
As one can see in Fig.~\ref{fig:m3_render}, the reconstructed object looks like the input model.
This, and the plot of the contour suggests that we were able to explore the design intent behind this model.

\begin{figure}[H]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{m3_pw_ntrl}
		\caption{Without translational surface}
		\label{fig:m3_render_ntrl}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{m3_pw_trl}
		\caption{With translational surface}
		\label{fig:m3_render_trl}
	\end{subfigure}
	\caption{Rendered results of \emph{Model 3}}
	\label{fig:m3_render}
\end{figure}

Similarly to \emph{Model 1} (Fig.~\ref{fig:m1_pw}), the surface of the reconstructed model contains small vertical grooves, resulting from the polyline representation.
Probably this causes the RMSE value to grow by $42\%$.
See Table~\ref{tab:ex2_quan} for quantitative results.
The higher computational burden of the translational primitive (compared to the others) also appears in the running time of the genetic algorithm.
Although the depth of the tree did not decrease, the total number of nodes did, with $14\%$.

\begin{table}[H]
	\centering
	\caption{Quantitative results of reconstructing \emph{Model 3}}
	\begin{tabularx}{\textwidth}{ X | c | c | c}
		& Without translational & With translational & Difference\\
		\hline
		\hline
		RANSAC run time & $244.02 \ [ s ] $ & $389.02 \ [ s ] $ & $+59\%$\\
		\# Segmented primitives & $15$ & $8$ & $-46.6\%$\\
		\# Segmented \& separating primitives & $51$ & $35$ & $-31.4\%$\\
		Genetic optimization run time & $ 21.47 \ [ h ] $ & $ 26.62 \ [ h ] $ & $+24\%$\\
		Tree depth & $10$ & $10$ & $=$\\
		Number of nodes and leaves & $43$ & $37$ & $-14\%$\\
		OBB diagonal & $302.269$ & $302.269$ &  \\
		RMSE & $0.201$ & $0.286$ & $+42\%$\\
	\end{tabularx}
	\label{tab:ex2_quan}
\end{table}

\subsection{Limitations}
\label{sec:transl_limit}
While we tested our methodology, we observed a limiting factor to the fitting process.
We show this aspect with the help of \emph{Model 4}~\cite{du18} in Fig.~\ref{fig:m4_input}.
%dbr1 = BSON.load("icsg_158csgtransls-v2_ransac.bson")

\begin{figure}[H]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{m4_input}
		\caption{Point cloud (32502 points)}
		\label{fig:m4_pc}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{m4_pw}
		\caption{Rendered image}
		\label{fig:m4_pw}
	\end{subfigure}
	\caption{\emph{Model 4}}
	\label{fig:m4_input}
\end{figure}

Fig.~\ref{fig:m4_ransac} shows the segmentation of \emph{Model 4}.
In this case, all available primitives were considered during fitting.

\begin{figure}[H]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.95\textwidth]{m4_segmented}
		\caption{Coloured by type}
		\label{fig:m4_ransac_seg1}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.95\textwidth]{m4_partsegcol_tr}
		\caption{Distinct colouring of the three translational primitives}
		\label{fig:m4_ransac_seg2}
	\end{subfigure}
	\caption{Result of segmentation of \emph{Model 4}}
	\label{fig:m4_ransac}
\end{figure}

As shown in Fig.~\ref{fig:m4_ransac_seg1}, we are able to recognize primitives in the whole point cloud.
However, the algorithm does not recognize that the ``base" of the object can be extruded from bottom-up.
Instead it recognizes a side-to-side translational surface.
The reason behind this behaviour is that the bottom and top faces contribute more points than the four sides only.
The fitted primitives are still valid, but they fail to interpret the design intent.

Presence of this behaviour suggests that there is a lower bound for the height-per-width ratio to recognize extruded surfaces.
Exploring this limit is not part of this thesis, however, Horváth and Erdős~\cite{lperd} suggest this value to be $\nicefrac{\pi}{4}$.

\section{Reconstruction of freeform extrusion}
\label{sec:res_freeform}
One great benefit of the proposed translational surface fitting method is that it can extract translational surfaces whose contour is a freeform curve
\emph{Model 5} (in Fig.~\ref{fig:m5_input}) is an example for this case.
\begin{figure}[H]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{m5_input}
		\caption{Point cloud (32486 points)}
		\label{fig:m5_pc}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{furi}
		\caption{Rendered image}
		\label{fig:m5_pw}
	\end{subfigure}
	\caption{\emph{Model 5}}
	\label{fig:m5_input}
\end{figure}

Contour of the outer surface consists of a large circular arc (far side) and a cosine curve (closer side on Fig.~\ref{fig:m5_input}).
The three holes are just regular cylinders.

\subsection{Result of segmentation and fitting}
\label{sec:m5_ransac}

Figure~\ref{fig:m5_ransac} shows the result of the segmentation step.
We considered plane, sphere, cylinder and cone primitives (top row) with the addition of a translational primitive (bottom row).
As can be seen on Figures~\ref{fig:m5_racol_transl}--\ref{fig:m5_segm_transl}, the outer surface is recognized as one translational primitive.
Fig.~\ref{fig:furcsa_contnorm} shows the contour of that primitive with corresponding surface normals.

The segmentation is considered successful without the the translational surface too.
The far side cylinder part is recognized faithfully, while the closer cosine surface is approximated with cylinders and planes.
The parameters used for the segmentation are listed in Table~\ref{tab:m5pars}.

\begin{figure}[H]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{m5_segmented}
		\caption{Coloured by type}
		\label{fig:m5_segm}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{m5_segmentedcol}
		\caption{Distinct colour for each primitive}
		\label{fig:m5_racol}
	\end{subfigure}
	\vfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{m5_segmented_tr}
		\caption{Coloured by type -- with translational primitive}
		\label{fig:m5_segm_transl}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{m5_segmentedcol_tr}
		\caption{Distinct colour for each primitive -- with translational primitive}
		\label{fig:m5_racol_transl}
	\end{subfigure}
	\caption{Result of segmentation of \emph{Model 5}}
	\label{fig:m5_ransac}
\end{figure}


\begin{figure}[h]
	\centering
	\includegraphics[width=0.35\textwidth]{furcsa_contnorm}
	\caption{Contour and surface normals of \emph{Model 5}}
	\label{fig:furcsa_contnorm}
\end{figure}


\begin{table}[h]
	\centering
	\caption{Parameters for the segmentation of \emph{Model 5}. (for notations see Table~\ref{tab:ransacpars})}
	\begin{tabular}{ c | c | c | c }
		Parameter & Description & Parameter & Description \\
		\hline
		\hline
		$N$ & $32486$ & $p_t$ & $90\%$\\
		$\tau$ & $500$ & $k$ & $3$ \\
		$d$ & $5$& $t$ & $15$ \\
		$r$ & $4$ & - & - \\
		$\epsilon_{plane}$ & $0.2$ & $\alpha_{plane}$  & $4^{\circ}$\\
		$\epsilon_{sphere}$ & $0.3$ & $\alpha_{sphere}$  & $1^{\circ}$\\
		$\epsilon_{cylinder}$ & $0.15$ & $\alpha_{cylinder}$  & $3^{\circ}$\\
		$\epsilon_{cone}$ & $0.2$ & $\alpha_{cone}$  & $1^{\circ}$\\
		$\epsilon_{translational}$ & $0.3$ & $\alpha_{translational}$  & $3^{\circ}$\\
		$\alpha_{perpend}$  & $89.5^{\circ}$ & Thinning parameter & $\nicefrac{\epsilon_{translational}}{4}$\\
		Agreeing normals & $80\%$ & - & - \\
	\end{tabular}
	\label{tab:m5pars}
\end{table}

\newpage
\subsection{Genetic optimization}
\label{sec:m5_genetic}
With the separating primitives computed, we can move on to the genetic optimization.
Corresponding parameters are listed in Table~\ref{tab:m5_gentab}.
The resulting trees are shown ine Fig.~\ref{fig:tree_m5} and Fig.~\ref{fig:tree_m5tr}.

\begin{figure}[H]
	\centering
\begin{forest}
	[$\cap$ [$-$ [$-$ [$-$ [$\cap$ [$\cap$ [$-$ [$\cap$ [$\sim$ [Cylinder10 ]] [Cylinder7 ]] [$-$ [Plane15 ] [Plane6 ]]] [Plane31 ]] [Plane5 ]] [Cylinder3 ]] [Cylinder2 ]] [Cylinder4 ]] [Cylinder1 ]]
\end{forest}
	\caption{Constructed tree without translational primitive}
	\label{fig:tree_m5}
\end{figure}

\begin{figure}[H]
	\centering
\begin{forest}
	[$\cap$ [$\cup$ [$\sim$ [Plane9 ]] [$\cap$ [$\cup$ [$\sim$ [Plane9 ]] [$\cap$ [Plane6 ] [$\cap$ [$\sim$ [$\cup$ [Cylinder4 ] [Cylinder3 ]]] [$\cup$ [$\sim$ [Cylinder2 ]] [Cylinder4 ]]]]] [$\cap$ [Plane5 ] [$\cup$ [Plane10 ] [Cylinder4 ]]]]] [$\cap$ [Transl1 ] [$\cup$ [Cylinder4 ] [Plane7 ]]]]
\end{forest}
	\caption{Constructed tree with translational primitive}
	\label{fig:tree_m5tr}
\end{figure}

The rendered results show that with the translational surface we can reconstruct the object faithfully (Fig.~\ref{fig:m5_render_trl}).
However, this cannot be said for the approximated model (Fig.~\ref{fig:m5_render_ntrl}).
While the cylindrical parts are well reconstructed, the cosine surface fails.

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.92\textwidth]{m5_render_ntrl}
		\caption{Without translational surface}
		\label{fig:m5_render_ntrl}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.92\textwidth]{m5_render_trl}
		\caption{With translational surface}
		\label{fig:m5_render_trl}
	\end{subfigure}
	\caption{Rendered results of \emph{Model 5}}
	\label{fig:m5_render}
\end{figure}


\begin{table}[H]
	\centering
	\caption{Parameters for the genetic optimization of \emph{Model 5} (for notations see Table~\ref{tab:geneticpars})}
	\begin{tabular}{ c | c | c | c }
		Parameter & Description & Parameter & Description \\
		\hline
		\hline
		$\epsilon_{d}$ -- wo. transl. & $1$ & $\epsilon_{d}$ -- w. transl. & $0.1$\\
		$\chi$ & $0.3$ & $\mu$ & $0.3$ \\
		$\mu_0$ & $0.3$ & Max. tree depth & $10$ \\
		Population size & $150$ & Number of iterations & $3000$ \\
		Best kept & $2$ & Tournament selection size & $30$ \\
		$p_{tournament}$ & $0.5$  & $\alpha$  & $10^{\circ}$\\
	\end{tabular}
	\label{tab:m5_gentab}
\end{table}

Looking at the quantitative results (Table~\ref{tab:m5_quan}), we can say that we pay the price of faithful reconstruction with computation time.
Both the segmentation and genetic optimization took more time with the additional translational primitive.
Although the depth of the tree decreased with the translational surface, the number of nodes increased $1.5$ times.
The stochastic behaviour of the genetic algorithm may explain this result.

On the other hand, the RMSE of the model improved with the translational surface.
It is much lower than the one without such primitive.
Although based on Fig.~\ref{fig:m5_render} this is expected, it is still an indicator of the successful work on exploring the design intent.

\begin{table}[h]
	\centering
	\caption{Quantitative results of reconstructing \emph{Model 5}}
	\begin{tabularx}{\textwidth}{ X | c | c | c}
		& Without translational & With translational & Difference\\
		\hline
		\hline
		RANSAC run time & $162.64 \ [ s ] $ & $59.72 \ [ s ] $ & $-45\%$\\
		\# Segmented primitives & $12$ & $6$ & $-50\%$ \\
		\# Segmented \& separating primitives & $36$ & $22$ & $38.8\%$ \\
		genetic optimization run time & $ 5.35 \ [ h ] $ & $ 9.53 \ [ h ] $ & $+78\%$\\
		Tree depth & $9$ & $8$ & $-12\%$\\
		Number of nodes and leaves & $20$ & $29$ & $+45\%$\\
		OBB diagonal & $16.779$ & $16.779$ & \\
		RMSE & $0.115$ & $0.0193$ & $-83\%$\\
	\end{tabularx}
	\label{tab:m5_quan}
\end{table}
