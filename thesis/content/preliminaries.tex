%----------------------------------------------------------------------------
\chapter{Preliminaries}
\label{sec:preliminaries}
%----------------------------------------------------------------------------

In this chapter two algorithms that form the basis of this work are presented, along with some implementation details.
The first is the CSG-tree reconstruction algorithm presented in~\cite{fayolle16} and the second is the efficient RANSAC algorithm~\cite{effRANSAC}.

Although we are aware of several RANSAC implementations (for instance in the CGAL~\cite{cgal} and PCL~\cite{pcl} libraries), we decided to implement the whole process ourselves.
The main reason is that we did not know in advance, how much modifications we have to make for the translational surfaces to work with the existing RANSAC implementations.
Also, with implementing these algorithms, we acquired more insights on the internals.

We chose the Julia language~\cite{julia} for implementation.
It is a high level language, with great performance and increasing interest from more and more areas outside of the technical computing community.
As the language itself and all the packages used for this thesis are open source and free, our implementation is also freely available\footnote{\url{https://gitlab.com/cserteGT3/masterthesis}}.

\section{An evolutionary approach to extract CSG-trees from point clouds}
\label{sec:fayolle_impl}
This method utilizes genetic programming to reconstruct the CSG-tree representation of an object.
The input is a 3D point cloud with surface normals, and the output is a construction tree with primitives in the leaves and geometric operations in the internal nodes.
Implicit representation is used for the primitives, therefore the tree can be interpreted as an implicit expression $f(p)$.
The zero level-set of the expression gives the surface approximation of the reconstructed object.

The main steps of the approach are segmentation and fitting of primitives (Sec.~\ref{sec:fayolle_fitting}), computation of separating primitives (Sec.~\ref{sec:fayolle_separating}), and construction tree evolution by genetic programming (Sec.~\ref{sec:fayolle_genetic}).
Description of the implicit surface and CSG-tree representations (Sec.~\ref{sec:impl_rep}) precedes the detailing of the reconstruction steps.

\subsection{Implicit surface and construction tree representations}
\label{sec:impl_rep}
An implicit surface is a surface representing the loci of a fixed value of some trivariate function $f: \mathbb{R}^3 \rightarrow \mathbb{R}$.
In other words, the surface divides the 3D space into two parts.
%The value of the implicit function in an arbitrary point is referred to as the implicit value.
We describe the primitives with such representation and use the set-operations (union, intersection, complement, subtraction) to build up complex models.
The set-operations can be defined with either min/max or R-functions, both are described in the paper.
We decided to use the min/max definitions.

In this thesis, we consider the inside of the primitives to be negative, or more generally, the surface normals point towards the growth of the value of the implicit function.
Min/max operations are defined in Table~\ref{tab:setops}, where $S$, $S_1$, $S_2$ are solid objects and $f_S$, $f_{S_1}$ and $f_{S_2}$ their corresponding implicit functions:

\begin{table}[h]
	\centering
	\caption{Definitions of the set-operations}
	\begin{tabular}{ c | l | c }
		Name & Definition & Notation \\
		\hline
		\hline
		union & $\min (f_{S_1},f_{S_2})$ & $\cup$ \\
		intersection & $\max (f_{S_1}, f_{S_2})$ & $\cap$ \\
		complement & $ - f_S$ & $\sim$ \\
		subtraction & $intersection(S_1, complement(S_2))$ & $-$ \\
	\end{tabular}
	\label{tab:setops}
\end{table}

Authors of the paper do not detail the implicit functions of the primitives, as it is easy to define them.
The equations for computing the implicit functions of the primitives used by our implementation are shown below.
Here \texttt{value}() and \texttt{normal}() define the value and gradient of the implicit function defining the primitive at an arbitrary point $p$.
Note that in this thesis we did not implement a torus primitive.

\subsubsection{Plane}
\label{sec:impl_plane}
A plane is described with one of its point $p_{plane}$ and its normal vector $n_{plane}$.
The value is the signed distance from the plane:
\begin{equation}
\mathtt{value}(p) = (p-p_{plane}) \cdot n_{plane}.
\end{equation}
The normal does not depend on the point:
\begin{equation}
\mathtt{normal}(p) = n_{plane}.
\end{equation}
Note that while we will refer to this surface as a plane, it is more correctly a halfspace, as it can be readily seen from its behaviour in Boolean operations.

\subsubsection{Sphere}
\label{sec:impl_sphere}
A sphere can be represented by its centre $c_{sphere}$ and its radius $r_{sphere}$.
The value is the signed distance from the surface of the sphere:
\begin{equation}
\mathtt{value}(p) = \| p-c_{sphere} \| - r_{sphere}.
\end{equation}
The surface normal is given by the direction of the vector from the centre to the point:
\begin{equation}
\mathtt{normal}(p) = \frac{p-c_{sphere}}{\| p-c_{sphere} \|}.
\end{equation}

\subsubsection{Cylinder}
\label{sec:impl_cylinder}
A cylinder is represented by its axis $a_{cylinder}$, a point on the axis $c_{cylinder}$ and its radius  $r_{cylinder}$.
The value is the signed distance from the surface of the cylinder:
\begin{equation}
\mathtt{value}(p) = \| p-c_{cylinder}-a_{cylinder} \cdot ((p-c_{cylinder}) \cdot a_{cylinder}) \| - r_{cylinder}.
\end{equation}
The surface normal is the direction vector pointing from the axis to the point:
\begin{equation}
\mathtt{normal}(p) = \frac{p-c_{cylinder}-a_{cylinder} \cdot ((p-c_{cylinder}) \cdot a_{cylinder})}{\| p-c_{cylinder}-a_{cylinder} \cdot ((p-c_{cylinder}) \cdot a_{cylinder}) \|}.
\end{equation}

\subsubsection{Cone}
\label{sec:impl_cone}
A cone can be described with its opening angle $\omega_{cone}$, its apex $ap_{cone}$ and its axis $ax_{cone}$ which considered to point from the apex towards the opening.
The following algorithm is used to calculate the distance from the cone's surface and normal of the surface at that point.

\begin{algorithm}[H]
	\caption{Calculate \texttt{value}($p$) and \texttt{normal}($p$) of the cone}
	\begin{algorithmic}[1]
		\REQUIRE Cone ($\omega_{cone}$, $ap_{cone}$, $ax_{cone}$) and a point $p$
		\STATE $to_p = ap_{cone}-p$
		\STATE $to_{pn} = \mathtt{normalize}(to_p)$
		\STATE $rot_{ax} = \mathtt{normalize}(ax_{cone} \times to_{pn})$
		\STATE $n_{comp} = \mathtt{normalize}( rot_ax \times ax_{cone})$
		\STATE $\mathbf{M}_r = \mathtt{rodriguesrad}(rot_ax, -\omega_{cone}/2)$
		\STATE $n = \mathtt{normalize}(\mathbf{M}_r \cdot n_{comp})$
		\STATE $d = -n \cdot -to_p$
		\RETURN $d, \ n$
	\end{algorithmic}
	\label{alg:cone}
\end{algorithm}

In Algorithm~\ref{alg:cone}, $\mathtt{rodriguesrad}(v, \alpha)$ constructs a rotation matrix -- based on Rodrigues' rotation formula -- which rotates around vector $v$ with an $\alpha$ angle.

\subsubsection{Construction tree}
\label{sec:construction_tree}
As mentioned earlier, the construction tree is a tree representation with implicit primitives in the leaves and the set-operations in the internal nodes.
The computation of both the implicit primitives and the set-operations have been discussed; this section is about visualising such trees.
The zero level-set of a tree $f$ gives the approximation of the surface.

Rendering implicit surfaces is a well-studied problem; two standard methods are ray-tracing and approximating meshing algorithms (Marching Cubes based algorithms).
These are difficult to completely implement and are out of the scope of this thesis.
Therefore we decided to use the ParaView visualization tool~\cite{paraview}.
We compute the values of the tree on a three-dimensional grid and write the values in a file that can be processed by ParaView.
Then the zero level-set is interpolated by the program.
The resolution and bounding box are chosen by the user.

As an example, a sphere cut half and bored with a cylinder is shown in Fig.~\ref{fig:hs}, with both its CSG-tree representation and a rendered view.

\begin{figure}[h]
	\centering
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\begin{forest}
			[$-$ [$\cap$ [Sphere1 ] [Plane2 ]] [Cylinder3 ]]
		\end{forest}
		%\vfill
		\caption{Construction tree of the hollow sphere model}
		\label{fig:hs_graph}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{hsw_pw_cropped}
		\caption{Render of the hollow sphere model in ParaView}
		\label{fig:hs_pw}
	\end{subfigure}
	\caption{Hollow sphere example}
	\label{fig:hs}
\end{figure}

%TODO move the tree? or what do i want here?
There are several noticeable artefacts on the rendered image \ref{fig:hs_pw}.
However, they are only due to the finite sampling of the three-dimensional grid.
These could be solved with more clever sampling, for instance with a finer resolution near to the surface.
However, applying these improvements would require a serious amount of work, and are not crucial with respect to the purpose of the thesis.

\subsubsection{Evaluating the construction tree}
\label{sec:eval_csg_tree}
The evaluation of the tree at an arbitrary point $p$ can be solved by a recursive strategy and also by flattening the tree.
By the recursive method, we start by evaluating the root node.
If it is a primitive, then its $\mathtt{value}(p)$ and $\mathtt{normal}(p)$ is computed.
If it is an inside node, we perform the corresponding set operation on the results of evaluating its children.
In our implementation we experienced poor performance using the recursive strategy, therefore we decided to flatten the tree.

In this case we walk through the tree with a post-order traversal and evaluate every node.
In this approach we always visit a child first, then its parent.
Therefore, when computing a set-operation, its operands are already computed.
In this case, the node evaluated last will be the root node.
The reasoning behind this is, that the structure of a tree does not change, therefore it must be walked through only once.

Exploiting Julia's metaprogramming capabilities, we can generate a function for any tree.
An example is shown in Alg.~\ref{alg:gencode} with the hollow sphere model.

\begin{algorithm}[H]
	\caption{Generated Julia code of the hollow sphere model}
	\begin{algorithmic}[1]
		\STATE \texttt{  :(function (var"\#\#409")}
		\STATE \texttt{var"\#\#404" = (CSGBuilding.evalf)(Cylinder3, var"\#\#409")}
		\STATE \texttt{var"\#\#403" = (CSGBuilding.evalf)(Plane2, var"\#\#409")}
		\STATE \texttt{var"\#\#402" = (CSGBuilding.evalf)(Sphere1, var"\#\#409")}
		\STATE \texttt{var"\#\#405" = (CSGBuilding.inters)(var"\#\#402", var"\#\#403")}
		\STATE \texttt{var"\#\#406" = (CSGBuilding.subtr)(var"\#\#405", var"\#\#404")}
		\STATE \texttt{return var"\#\#406"}
		\STATE \texttt{end)}
	\end{algorithmic}
	\label{alg:gencode}
\end{algorithm}

The above can be interpreted without knowing Julia's syntax.
The first and last lines indicate, that we define a function, with one input, the point $p$ where we would like to evaluate the tree.
On lines 2 to 4, we evaluate the primitives, denoted by their names.
Then on lines 5--6 we perform the two set-operations on the so far computed values, and finally return the desired values on line 7.


\subsection{Segmentation and fitting of primitives}
\label{sec:fayolle_fitting}
The first step of the algorithm is the segmentation of the input point cloud $S$ into clusters, and fitting primitives to each of these clusters. In this work plane, sphere, cylinder, cone and torus primitives were considered.
For this purpose, many algorithms are available; the authors present some of them.
The authors chose the RANSAC paradigm, which will be presented in detail in Section~\ref{sec:ransac}.

\subsection{Separating primitives}
\label{sec:fayolle_separating}
Detecting only primitives describing the surface may not be sufficient to fully recover an object.
Sometimes it is needed to introduce additional primitives, called separating primitives, that do not appear in the segmentation.
Let us illustrate this with an example from~\cite{fayolle16}.
The object shown in Fig.~\ref{fig:fayollebad} cannot be described by a construction tree containing only the segmented surfaces.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{fayolle16_bad}
	\caption{Example of using separating primitives. Left: segmented point-set. Middle: reconstructed object without separating primitives. Right: reconstructed object using two additional separating planar half-spaces.~\cite{fayolle16}}
	\label{fig:fayollebad}
\end{figure}

The left-most image shows the result of the segmentation; note that the fillet feature is successfully recognised as a cylinder.
However, as can be seen in the middle image, it is not retrieved properly.
If we introduce two more plane primitives that clip the cylinder at its boundary, the fillet feature can be reconstructed.
This can be seen on the right-most image.
To compute those separating primitives, Alg.~\ref{alg:sepprimitives} is introduced.

\begin{algorithm}[H]
	\caption{Compute separating primitives~\cite{fayolle16}}
	\begin{algorithmic}[1]
		\REQUIRE List of fitted primitives $C$ and their corresponding points-sets $S_i$
		\FOR{\texttt{each} primitive \texttt{in} $C$}
		\STATE $S_i \leftarrow$ corresponding point-set of $C_i$
		\STATE $b \leftarrow$ oriented bounding box of $S_i$
		\FOR{\texttt{each} face $f$ \texttt{in} $b$}
		\STATE $C \leftarrow C \cup f$
		\ENDFOR
		\ENDFOR
		\STATE remove duplicates from $C$
		\RETURN $C$
	\end{algorithmic}
	\label{alg:sepprimitives}
\end{algorithm}

We iterate through all the fitted primitives that were found in the fitting step.
If the primitive is not a plane, we retrieve those points that correspond to the primitive.
Then we compute the oriented bounding box of the point-set and add all of its planes to the list of the primitives.
Finally, we remove the duplicates if there is any.

Occasionally there are cases when using only planar separating primitives is not sufficient.
We do not deal with such problems; a detailed description is available in the paper.

Wu et al.~in~\cite{wu2018} also use additional separating primitives as described in Sec.~\ref{sec:wang2011}.
However, they treat the quarter of the cylinder as a special case and compute its maximum bounding box, instead of minimum (see Fig.~6 in~\cite{wu2018}).

In our implementation we treat all primitives the same way, and compute their minimum bounding boxes.

%TODO conversion as well

\subsection{Genetic programming}
\label{sec:fayolle_genetic}
To build the construction tree from the segmented and separating primitives and the set-operations, an evolutionary algorithm is used.
As Alg.~\ref{alg:genalg} describes, we generate a population (set of creatures) of construction trees.
We rank these creatures with a fitness function and keep the best ones.
Then we generate a new population including the bests by default.
The rest of the population is filled with offspring creatures which are generated by mutation and crossover operations -- analogously to the natural evolution.
We iterate these steps until a certain criteria is met, in our case it is the maximum number of iterations.

\begin{algorithm}[h]
	\caption{Tree extraction~\cite{fayolle16}}
	\label{alg:genalg}
	\begin{algorithmic}[1]
		\REQUIRE Finite point-set S and list of fitted primitives
		\STATE Let $s$ be the population size
		\STATE Let $\mu$ be the mutation rate
		\STATE Let $\chi$ be the crossover rate
		\STATE $p \leftarrow \mathtt{InitializeRandomPopulation}(s)$ \{Create a random initial population\}
		% start looping
		\WHILE{Stopping criterion is not satisfied}
		\STATE $p \leftarrow \mathtt{Rank}(p, S)$ \{Evaluate and sort the current population\}
		\STATE $p' \leftarrow [ p_1, \ldots, p_n ] $ \{Save the best $n$ creatures to the next generation\}
		\WHILE{$\mathtt{Length}(p') \leqslant s$}
		\STATE $c_1, c_2 \leftarrow \mathtt{SelectByTournament}(p)$
		\STATE $c_1', c_2' \leftarrow \mathtt{Crossover}(\chi, c_1, c_2)$
		\STATE $c_1' \leftarrow \mathtt{Mutate}(\mu, c_1')$
		\STATE $c_2' \leftarrow \mathtt{Mutate}(\mu, c_2')$
		\STATE $p' \leftarrow p' \cup [c_1', c_2']$ \{Add $c_1', c_2'$ to the next generation\}
		\ENDWHILE
		\STATE $p \leftarrow p'$
		\ENDWHILE
	\end{algorithmic}
\end{algorithm}

A random tree is created by drawing a sample from a unit uniform distribution and comparing it with the probability to create a sub-tree.
If the sample value is lower, and the maximum depth has not been reached for the tree, an operation is selected at random (with each operation having the same probability), and the procedure is recursively called to generate sub-trees.
Otherwise, a primitive is selected at random (with each primitive having the same probability) and the procedure terminates.

\subsubsection{Fitness function}
\label{sec:genetic_fitness}
To rank the creatures, the authors present a two-step method: first a raw score is computed, then those scores are rescaled, and creatures are sorted based on the rescaled scores.
The raw score (Eq.~\ref{eq:scoreeq}) is to be maximized; the rescaling turns this into a minimization.
We did not implement the rescaling step, we maximize the raw score.

\begin{equation}
\label{eq:scoreeq}
E(c,S):=\sum_{i=1}^{N}(exp(-d_i(c)^2)+exp(-\theta_i(c)^2))-\lambda \ \mathtt{size}(c),
\end{equation}
where:
\begin{itemize}
	\item $N$ is the size of the point cloud $S$,
	\item $d_i(c) = \frac{\mathtt{value}(\mathbf{x}_i)}{\epsilon_{d}}$, with \texttt{value()} corresponding to creature $c$ and $\epsilon_{d}$, a user defined parameter, and  $\mathbf{x}_i$ are the points from the input point cloud $S$,
	\item $\theta_i(c)=\frac{\arccos(\mathtt{normal}(\mathbf{x}_i) \cdot \mathbf{n}_i)}{\alpha}$, where \texttt{normal()} is the surface normal of the creature $c$, $\mathbf{n}_i$ is the normal vector at $\mathbf{x}_i$ and $\alpha$ a user defined parameter,
	\item $\mathtt{size}(c)$ the function that counts the number of nodes (internal and leaves) of creature $c$,
	\item $\lambda$ a user defined parameter.
\end{itemize}

This fitness function smoothly penalizes models that disagree with the point cloud (in terms of being near or on the points, and having the normal agreeing with the points' normal).
Large trees are also penalized, with the last term of the score function.
There are two reasons to do this, one is that as trees become larger, it takes longer to evaluate the corresponding functions.
The second reason is, that to use in further applications (analysis, rendering, etc.), it is beneficial to limit the number of sub-trees and primitives that do not contribute additional information.

Despite of the penalization term, unreasoned sub-trees may appear, which should be removed in a post-processing step.
A simple example for this is, when a primitive is operand of the subtraction operation twice in a row ($\sim \sim S_1 = S_1$).

For the parameter $\lambda$, the authors suggest to use $\lambda = \log(N)$, where $N$ is the size of the point cloud.

\subsubsection{Crossover operation}
\label{sec:genetic_crossover}
The two creatures, that has been selected by tournament, are passed to the \texttt{Crossover} function, which is described in Alg.~\ref{alg:crossover}.
For each tree, a cut point is selected randomly, then the two sub-trees are exchanged.
To control the size of the trees, if the depth of one of the resulting trees exceeds the user-defined maximum, the operation is cancelled.

\begin{algorithm}[h]
	\caption{\texttt{Crossover}~\cite{fayolle16}}
	\label{alg:crossover}
	\begin{algorithmic}[1]
		\REQUIRE Crossover rate $\chi$, two creatures $c_1, c_2$
		\STATE $r \leftarrow \mathtt{RandomNumberInUnitInterval}()$
		\IF{$r < \chi$}
		\STATE Select randomly cut points in $c_1$ and $c_2$
		\STATE Exchange the sub-trees at the two cut points above
		\RETURN $[ \tilde{c}_1, \tilde{c}_2 ]$ \{return the modified creatures\}
		\ELSE
		\RETURN $[ c_1, c_2]$
		\ENDIF
	\end{algorithmic}
\end{algorithm}

\subsubsection{Mutation operation}
\label{sec:genetic_mutation}
After the crossover operation, creatures are passed to the \texttt{Mutate} function, described in Alg.~\ref{alg:mutate}.
Altering the tree either means generating a random new tree (with probability $\mu_0$), or selecting a random node and replacing its sub-tree with a randomly generated new one.
As in the \texttt{Crossover} operation, if the depth of the resulting tree is larger than the defined threshold, the operation is cancelled.

\begin{algorithm}[H]
	\caption{\texttt{Mutate}~\cite{fayolle16}}
	\label{alg:mutate}
	\begin{algorithmic}[1]
		\REQUIRE Mutation rate $\mu$, a creature $c$
		\STATE $r \leftarrow \mathtt{RandomNumberInUnitInterval}()$
		\IF{$r < \mu$}
		% mutate tree
		\STATE $r \leftarrow \mathtt{RandomNumberInUnitInterval}()$
		\IF{$r < \mu_0$}
		\RETURN a new random tree
		\ELSE
		\STATE Select randomly a mutation point in $c$
		\STATE Generate a random sub-tree and attach it at the mutation point to $c$
		\RETURN $c$
		\ENDIF
		\ELSE
		\RETURN $c$
		\ENDIF
	\end{algorithmic}
\end{algorithm}

\newpage
\subsection{Summary of parameters}
\label{sec:sum_gen_pars}

In Table~\ref{tab:geneticpars} we summarized the parameters of the genetic optimization.

\begin{table}[h]
	\centering
	\caption{Parameters of the genetic optimization}
	\begin{tabular}{ c | l | c }
		Parameter & Description & Suggested value~\cite{fayolle16} \\
		\hline
		\hline
		$S$ & Input point cloud. & - \\
		$N$ & Size of the input point cloud. & - \\
		$s$ & Population size. & 150 \\
		$\mu$ & Mutation rate. & $0.3$ \\
		$\mu_0$ & Probability of generating a new tree in \texttt{Mutate}. & $0.7$ \\
		$\chi$ & Crossover rate. & $0.3$ \\
		$\epsilon_{d}$ & Distance threshold for the score function. & $0.01$ \\
		$\alpha$ & Angle threshold for the score function. & $10^\circ$ \\
		$\lambda$ & Weight of the score of a tree in the score function. & $\log(N)$\\
		$n$ & The best $n$ creatures are saved to the next generation. & $2$ \\
		& Number of iterations. & $3000$ \\
	\end{tabular}
	\label{tab:geneticpars}
\end{table}

\section{Efficient RANSAC}
\label{sec:ransac}
As mentioned earlier, the implementation (and also the summary) of the RANSAC algorithm is based on~\cite{effRANSAC}.
The input of the algorithm is a point cloud of size $N$ with points and associated surface normals.
The output is a set of primitive shapes with corresponding sets of points, and the rest of the points that do not belong to any primitives.

In every iteration, new shape candidates are created by fitting primitives to randomly sampled minimal sets.
Every shape primitive is generated for every minimal set and the valid ones are continuously collected in set $C$.
Then every candidate is scored and the one with the highest score is considered the best.
A candidate is only extracted if the probability ($p_t$) that no better candidates are in $C$ is high enough.
If the best candidate is chosen to be extracted, its points are removed from the point cloud, and every other candidate that has a removed point is also deleted from $C$.
The iteration continues until the probability that every at least $\tau$ sized shape is found is larger than a parameter threshold.
The score of a candidate is defined by the number of compatible points.
A point is compatible if it is in the $\epsilon$ band of the shape, and its normal does not deviate from the surface normal more than an $\alpha$ angle.
Also, only those points are considered that count towards the largest connecting component in the parameter space bitmap of the shape.

\begin{algorithm}[H]
	\caption{Extract shapes in the point cloud $\mathcal{P}$~\cite{effRANSAC}}
	\label{alg:ransac}
	\begin{algorithmic}[1]
		\STATE $\Psi \leftarrow 0$ \{extracted shapes\}
		\STATE $C \leftarrow 0$ \{shape candidates\}
		\REPEAT
		\STATE $C \leftarrow$ $C$ $\cup$ \texttt{newCandidates}() \{see Sec.~\ref{sec:fitting} and~\ref{sec:sampling}\}
		\STATE $m \leftarrow $ \texttt{bestCandidate}($C$) \{see Sec.~\ref{sec:score}\}
		\IF{$P(|m|,|C|)>p_t$}
		\STATE $\mathcal{P} \leftarrow \mathcal{P} \setminus \mathcal{P}_m$ \{remove points\}
		\STATE $\Psi \leftarrow \Psi \cup m$
		\STATE $C \leftarrow C \setminus C_m$ \{remove invalid candidates\}
		\ENDIF
		\UNTIL{$P(\tau, |C|) < p_t$}
		\RETURN $\Psi$
	\end{algorithmic}
\end{algorithm}

In the following sections, a detailed description of the algorithm is given from sampling, through primitive fitting, scoring and computation complexity.
Implementation details are also given based on our experiences.

\subsection{Primitive fitting}
\label{sec:fitting}
Considered shapes are planes, spheres, cylinders, cones and tori.
For these methods not only the points \{$p_1, p_2, p_3$\} are needed but the corresponding surface normals \{$n_1, n_2, n_3$\}, as well.
If normal information is not available, there are algorithms to approximate it, for example~\cite{hoppe92}.
With the presence of normals, the shapes can be approximated from only one or two samples.
However, an additional sample can be used to immediately verify a candidate, and thus reduce the number of many relatively low scored shapes.

\textbf{Plane}: In the case of planes, three points define a plane and their normals are used to validate the fit.
A candidate is only accepted if the deviations of the three normals and the fitted normal are less than a predefined angle $\alpha$.

\textbf{Sphere}: A sphere is fully defined by two points and their normals.
Of course, in the presence of noise, the two lines determined by the point-normal pairs do not meet at the centre of the sphere, therefore the midpoint of the shortest line segment between the two lines is used as a centre.
Then the mean of the distance from the centre to the two points is taken as the sphere radius.
The sphere is rejected if not all three points are in the $\epsilon$ band of the sphere surface, or the normals deviate by more than an $\alpha$ angle from the computed normals.

\textbf{Cylinder}: For cylinders, the cross product of two normals gives the axis of the cylinder.
A centre point can be computed by projecting the two lines (determined by the point-normal pairs) onto the plane defined by the axis and taking their intersection.
The radius of the circle is then set to the distance of the centre and one point on that plane.
As always, the compatibility of all three points is tested.

\textbf{Cone}: Although the cone is defined by two points and normals, all three are used for its recognition.
The apex ($c$) is found by intersecting the three planes defined by the point-normal pairs.
The axis ($a$) is given by the normal of the plane defined by the three points \{$c+\frac{p_1-c}{\|p_1-c\|}, \ldots, c+\frac{p_3-c}{\|p_3-c\|}$\}.
The opening angle is given as {$\omega=\frac{\sum_{i} \arccos((p_i-c)\cdot a)}{3}$}.
The cone is also verified by compatibility before becoming a candidate.

\textbf{Torus}: as the torus was not implemented in the scope of this thesis, the reader is guided to section 4.1 in~\cite{effRANSAC}.

The original paper does not elaborate more on the direction of the normals, but while implementing we took some notes that worth sharing.
First let us introduce the terms positive and negative shapes, as described in~\cite{weichung88}.
A positive shape means that the normals point from the centre or axis of the shape towards outside and of course negative shape means the opposite.
When the compatibility is determined we must take the positivity/negativity into account, which means that all the normals should similarly be positive or negative.
If they are different, the candidate is discarded.
This is mainly a noise filtering step to ensure the appropriate shapes, but one may use this information to accelerate the CSG-tree reconstruction with additional a priori information.

\subsection{Computational complexity}
\label{sec:complexity}
In section 4.2 of~\cite{effRANSAC}, the complexity of the algorithm is presented.
It is dominated by two major factors: the number of minimal sets that are drawn, and the cost of evaluating the score for every candidate shape.
Let $P$ be a point cloud consisting of $N$ points and therein a shape $\psi$ of size $n$.
Let $k$ denote the minimum number of points required to define the shape, also called a minimal set.
If we assume that every $k$ points will result in an appropriate candidate, the probability of detecting $\psi$ in a single pass:
\begin{equation}
\label{eq:Pn}
P(n) =  \binom{n}{k} \bigg/ \binom{N}{k} \approx   \bigg( \frac{n}{N}  \bigg)^k.
\end{equation}
The probability of finding the candidate in the first $s$ samples (drawing a new minimal set $s$ times) equals to the probability of the complementary of $s$ consecutive failures:
\begin{equation}
\label{eq:Pns}
P(n,s) =  1 - (1 - P(n))^s.
\end{equation}
Equations \ref{eq:Pn} and \ref{eq:Pns} will be used later to calculate the extraction and termination criteria.
After a short deduction, we can conclude that the asymptotic complexity of the RANSAC algorithm is
\begin{equation}
\label{eq:O}
O\left(\frac{1}{P(n)}C\right),
\end{equation}
where $C$ denotes the cost of evaluating the cost function.

To summarize the equations: the size of the minimal set should be as small as possible, and a good sampling strategy is crucial, as the runtime complexity is directly linked to the success rate of finding good sample sets.

\subsection{Sampling}
\label{sec:sampling}
As shapes are local phenomena, the smaller the distance between two points, the higher the probability that they belong to the same shape.
This should be exploited during sampling.
To establish spatial proximity, an octree data structure is used.\footnote{An octree is a tree data structure in which each internal node has exactly eight children.}
The first sample of a minimal set is chosen randomly from the entire point cloud.
Then an octree cell is chosen which includes the selected point and rest of the points ($k-1$ pcs.) are drawn randomly from this cell.

Selection of the proper octree level is an essential part of the sampling strategy.
Of course, we would like to choose the level so that the probability of a ``good cell" is as high as possible.
Therefore the cell is chosen from a level according to a non-uniform distribution that reflects the likelihood of the respective level to contain a good cell.
The probability $P_l$ of a level $l$ is initialized with $P_l=\frac{1}{d}$ where $d$ is the depth of the octree.
Then for every level, the score ($\sigma_l$) of previously scored candidates that originate from the corresponding level is summed and the distribution is updated:
\begin{equation}
\label{eq:upPl}
\hat{P}_l = x\frac{\sigma_l}{w P_l} + (1-x)\frac{1}{d},
\end{equation}
where $w = \sum_{i=1}^{d} \frac{\sigma_i}{P_i}$.
During the iteration, the distribution is continuously changing as more and more shapes are extracted from the point cloud.
Setting $x=0.9$ ensures that $10\%$ of the samples are always uniformly distributed among the levels.
This enables the sampling algorithm to handle the changing importance of levels as points are removed from the point cloud.
In one iteration of the RANSAC algorithm, not only one but $t$ number of minimal sets are sampled.
This is because sampling is computationally cheap compared to the rest of the iteration, and there are cases when no primitives fit the minimal set.

\subsection{Score}
\label{sec:score}
The $\sigma$ score measures the quality of a given shape candidate.
The score is equal to the number of compatible point-normal pairs.
A point is compatible if it satisfies all the following requirements:
\begin{itemize}
	\item It must be in the $\epsilon$ radius band of the surface of the candidate.
	\item The normal must not deviate from the surface normal at the point more than an $\alpha$ angle.
	\item The point must be part of the largest connected component in the parameter space bitmap of the shape.
\end{itemize}

As mentioned earlier when the normals are compared the positivity/negativity is also taken into account.

The connected component criterion is a noise filtering step, however, in our implementation it is not considered.
Based on our experiments, synthetic tests work without the bitmapping step, but of course, when this method is applied for real-world measurements, it is certainly needed.
Our experiences have also shown that the algorithm is easier to fine-tune if there are separate epsilon and alpha parameters for every primitive type (i.e., $\epsilon_{sphere}$, $\alpha_{sphere}$, $\epsilon_{plane}$, etc.).

\subsection{Score computing}
\label{sec:score_compute}
The second major performance factor of the RANSAC algorithm is evaluating the score of the candidates.
A naive implementation could be that we compute the compatibility of all the points, but it is not reasonable for large point clouds.
At this stage of the algorithm, we are not interested in all the points that correspond to a candidate, but we would like to determine which shape consists of the most points.
Therefore the computational cost can be reduced if the overall score is inferred from the score calculated for a smaller subset.

The point cloud is partitioned into $r$ number of disjoint random subsets: $\mathcal{P}=\{\mathcal{S}_1 \ ... \ \mathcal{S}_r\}$.
When the score is calculated, the candidate is scored only against the first subset (denoted by $\sigma_{\mathcal{S}_1}$).
From this value, an approximation is given for the complete point cloud ($\sigma_p$) using the well-known induction from inferential statistics:
\begin{equation}
\label{eq:estimate_score}
\hat{\sigma}_{\mathcal{P}}(\psi) = -1- \xi (-2-|\mathcal{S}_1|, -2-|\mathcal{P}|, -1-\sigma_{\mathcal{S}_1}(\psi)),
\end{equation}
where
\begin{equation}
\xi (N,x,n) = \frac{xn \pm \sqrt{\frac{xn(N-x)(N-n)}{N-1}}}{N}
\end{equation}
is the mean plus/minus the standard deviation of the hypergeometric distribution.
The result of the approximation is a confidence interval $[a,b]$ that gives a range of likely values of the exact score.
The one with the largest expected value $E=(a+b)/2$ is considered the best shape.
Of course, if the confidence interval of the best one intersects with other intervals we cannot be sure that the best is really the best.
Therefore further refinement -- evaluation of the score for more subsets -- is needed.
This part is not implemented in the scope of this thesis, corresponding equations and methodology can be found in Section 4.5.1 of~\cite{effRANSAC}.
This scoring method has some additional effects on the way bitmaps are generated, these aspects are also described in the paper.

\subsection{Termination and extraction conditions}
\label{sec:conditions}
Two important questions have not been addressed yet: how it is decided when to extract the best candidate ($\psi_m$) and when to terminate the iteration.
We aim to minimize the likelihood to extract a candidate that is not the best, therefore, we track the probability $P(\psi_m,s)$ where $s$ denotes the number of candidates that have already been drawn.
$P(\psi_m,s)$ is the likelihood that after drawing $s$ candidates $\psi_m$ is the best.
If it is higher than a threshold ($p_t$) we conclude that there is a low chance that we have overlooked a better candidate, and extract $\psi_m$.
The termination condition is similar, $P(\tau,s)$ is observed, where $\tau$ denotes a parameter, the minimum size of the shapes.

Implementation of these conditions seems straightforward, but based on the paper it is not clear what $s$ exactly denotes.
It can mean two things:
\begin{enumerate}
	\item The number of candidates that are in set $C$ at that given iteration.
	\item The number of minimal sets that have been drawn. At the $i$-th iteration, this equals to $i \cdot t$.
\end{enumerate}
Based on our experiments, the first results in bad performance, because it takes many iterations to collect enough candidates to increase $P(\psi_m,s)$ and extract the shapes.
When using the second one, $s$ increases fast, therefore candidates are quickly extracted, however, for the same reason, the algorithm might terminate too early.
In this case, one must pay attention to the minimal shape size ($\tau$), as larger values may cause early termination.

\newpage
\subsection{Extraction}
\label{sec:extraction}
When the best candidate is selected for extraction, refitting is executed before it is finally accepted.
In the paper it is done as follows: a least-square fit is calculated to all compatible points that are within $3\epsilon$ distance from the shape.
This is another noise filtering step that optimizes the geometric error of the shape.
Then the shape is extracted, the candidate is removed from set $C$, and its corresponding points are removed from the point cloud.
Also, those candidates that have points removed are marked as invalid, and also deleted.
In our implementation least-square fitting is not used, only the compatible points are searched in the whole point cloud.

\subsection{Summary of parameters}
\label{sec:paramsummary}

The following table summarizes the different parameters and annotations of the RANSAC algorithm.
Only those parameters are listed that are described in this thesis.

\begin{table}[h]
	\centering
	\caption{Parameters of the RANSAC paradigm}
	\begin{tabularx}{\textwidth}{ c | X | c }
		Parameter & Description & Suggested value~\cite{effRANSAC} \\
		\hline
		\hline
		$\mathcal{P}$ & Input point cloud. & - \\
		$N$ & Size of the input point cloud. & - \\
		$C$ & Collection of shape candidates. & - \\
		$p_t$ & Probability threshold. & $99\%$ \\
		$\tau$ & Minimum size of shape candidates. & - \\
		$\epsilon_{prim}$ & Compatibility parameter for distance. & - \\
		$\alpha_{prim}$ & Compatibility parameter for angle of the normals. & - \\
		$k$ & Size of the sampled minimal set. & 3 or 4 \\
		$s$ & Number of candidates at the given iteration (see \ref{sec:conditions}). & - \\
		$d$ & Octree depth. & - \\
		$\sigma_l$ & Sum of scores of shapes generated from octree level $l$. & - \\
		$P_l$ & Likelihood that level $l$ contains a good cell. & - \\
		$t$ & Number of sampled minimal sets in one iteration. & - \\
		$r$ & Number of subsets. & - \\
		$\hat{\sigma}_{\mathcal{P}}(\psi)$ & Approximate score of shape $\psi$. & - \\
	\end{tabularx}
	\label{tab:ransacpars}
\end{table}
