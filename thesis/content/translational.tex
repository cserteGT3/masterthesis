%----------------------------------------------------------------------------
\chapter{Translational surfaces}
\label{sec:translational_surfs}
%----------------------------------------------------------------------------
We define translational (or extruded) surfaces as the surface resulting from moving a closed planar curve along the plane normal (also called translational direction).
As opposed to the previously described primitives (plane, sphere, etc.), translational surfaces cannot be defined by a small (3-4) set of points.
The consequence is that we do not define a one-step fitting method, but rather three distinct methods that build into the stages of the RANSAC paradigm, namely fitting to minimal sets, scoring and extraction.
Reconstructing the lines and curves that define the contour requires a significant effort; we approximate the contour instead with polyline representation.

In brief: considering a set of three point-normal pairs, if there is a direction vector with each normal being perpendicular to it, we presume that those points belong to a translational surface.
The direction vector gives the normal of the plane in which the planar curve lies.
We project all or a subset of the points of the point cloud to this plane, filtering out those, whose normal is not lying in the plane.
We then have all the points that define the contour of the extruded surface.

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{transl_input}
		\caption{Point cloud ($16461$ points)}
		\label{fig:transl_inp_pc}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.82\textwidth]{Part5_contour}
		\caption{Contour of the extrusion}
		\label{fig:transl_inp_contour}
	\end{subfigure}
	\caption{\emph{Model 1}}
	\label{fig:transl_inp}
\end{figure}

In most cases, not only one closed curve generates the contour, but multiple ones, see the example in Figure~\ref{fig:transl_inp_contour}.
To deal with such surfaces, we decided that the contour of a translational surface can only consist of one closed curve.
Therefore if the projected points define multiple curves, they must be segmented into distinct ones.
At the end of the fitting step, we will have a translational direction and one or more set of points that define the contour of the translational surfaces.

At the scoring step, we count the number of points that define the contour and estimate a score based on that.
In this case we do not measure compatibility.

When a translational surface candidate is to be extracted, we fit a polyline representing the contour, and search all the compatible points.
Then we extract the shape the same way as the others.
Fitting the polyline is time-consuming enough not to execute it at the fitting or scoring step of the RANSAC paradigm.
To visualize the above mentioned steps, we follow the way of \emph{Model 1} throughout the process.
This is a synthetic example modelled for this purpose.
The outer contour consists of a sharp corner, a spline curve and rounded corners, as well.
Minimal noise is added to show why certain parts of the algorithm are needed.

\section{Fitting}
\label{sec:transl_fit}
Three steps are taken at the fitting step to get the points that define the contour of a translational surface:
\begin{enumerate}
	\item Search the translational direction.
	\item Project the points to the plane defined by that direction.
	\item Segment the projected points to closed contour(s).
\end{enumerate}

\subsection{Translational direction}
\label{sec:transl_fit_dir}
As in the case of other primitives, the starting point is three point-normal pairs (a minimal set).
We build on the observation that in every point of a translated surface, corresponding surface normals are perpendicular to the translational direction.
If the three normals have a common perpendicular line, they may belong to the same translational surface.
Therefore we compute the cross product of two of the normals and verify that the third is perpendicular to it.
For robust detection, we use those points that are farther apart.
We not only need the direction of the normal, but a coordinate frame is also required for consistent projection.
We define one of the normals as the x-axis.
The z-axis is the previously computed translational direction, and the right-hand rule gives the y-axis.

Figure~\ref{fig:transl_dir} shows an example with \emph{Model 1}.
The blue points are part of the selected minimal sets and based on their normal (green arrows), the translational direction is computed (red arrow).
The plane, in which the points will be projected in the next step is also shown (in yellow).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{transl_transldir}
	\caption{Determining the translational direction}
	\label{fig:transl_dir}
\end{figure}

\subsection{Projection}
\label{sec:transl_fit_proj}
Next, we project the point cloud to the plane defined by the previously computed coordinate frame.
Indeed, for large point clouds, this is a massive amount of points.
Therefore we exploit the random subsets of the RANSAC paradigm, and only points from one random subset are projected.
Naturally, this draws an upper line for the number of subsets as details may disappear as fewer and fewer points are in one subset.
%To filter out points that are not part of the supposed translational surface, we discard those, whose corresponding normal is not perpendicular to the translational direction.
To filter out points that are not part of the supposed translational surface, we discard those, whose corresponding normal is not perpendicular (within a threshold) to the translational direction.

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{transl_projall}
		\caption{Points from all subsets}
		\label{fig:transl_projall}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{transl_proj1}
		\caption{Points from one subset}
		\label{fig:transl_proj1}
	\end{subfigure}
	\caption{Projected and compatible points of \emph{Model 1}}
	\label{fig:transl_proj}
\end{figure}

Now we have a set of points on one plane that probably define a translational surface.
There is a particular case that we have to deal with.
For planes, it is also true that in every point the surface normals are perpendicular to a common direction, that is the normal of that plane.

We want to filter out these cases because we have a plane primitive with much lower computational burden.
Therefore we calculate the oriented bounding box of the projected points and discard the candidate if one side of the rectangle is much shorter than the other -- it is more like a line than an arbitrary shape.

On Fig.~\ref{fig:transl_proj} one can see the projected points.
On the left (Fig.~\ref{fig:transl_projall}) all the compatible points are projected that results in overall $12679$ points.
On the right (Fig.~\ref{fig:transl_proj1}) points from only one subset are projected.
In this example we use four subsets and this results in $3204$ points.
As the contour is well preserved with the fewer points there is no need to use the full point cloud with its higher computation burden.

\subsection{Segmentation}
\label{sec:transl_fit_segm}
Segmenting the resulting points is one of the hardest part of the fitting procedure.
We do not know the number of curves nor the noise that loads on our samples.
As a solution, we concluded the following: we build a graph that links points that are near and use the Union Find algorithm to search for disjoint cycles in that graph.

To reduce computation cost, we build a k-d tree from the projected points, then iterate through them to link every other point that is in an epsilon range.
A k-d tree is a space partitioning data structure where the points are recursively split into groups using hyper-planes.
After building the tree, an efficient range searching method can be used to find all points within the range of given point(s).

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{transl_circled}
		\caption{Points and their $\epsilon_{translational}$ radius circles}
		%The image depicts points, and the circles representing their epsilon radius ?surroundings?
		\label{fig:transl_segmentcirc}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{transl_segmented}
		\caption{Result of the segmentation}
		\label{fig:transl_segmented}
	\end{subfigure}
	\caption{Segmentation of projected points}
	\label{fig:transl_segment}
\end{figure}

The Union Find algorithm is a robust and efficient way of detecting cycles in a graph.
If the range parameter ($\epsilon_{translational}$) of the search is too small, many poor subgraphs will be recognized that do not recreate the contour.
Therefore we accept a subgraph only if the number of corresponding points is higher than the minimum shape size divided by the number of subsets ($\nicefrac{\tau}{number\_of\_subsets}$).
As $\tau$ is a parameter corresponding to the whole point cloud, we have to scale the condition with the number of subsets.
After the segmentation, we treat the accepted subgraphs as the contour of separate translational surfaces.
To create candidates, we also store the coordinate frame that represents the plane in which the contour is, and the index of the subset from where the projected points are derived.

In Fig.~\ref{fig:transl_segmentcirc} the points and their $\epsilon_{translational}$ radius circles are shown.
The points are mostly covered by the circles which means that a closed contour can be extracted with this parameter.
In Fig.~\ref{fig:transl_segmented} the result of the segmentation is shown.

\section{Scoring}
\label{sec:transl_score}
As described earlier, we estimate the score based on the number of points that define the contour.
The estimation (Equation~\ref{eq:estimate_score}) requires the size of the subset; therefore, it is stored along the primitive as described earlier.

\section{Polyline representation}
\label{sec:extr_polyline}

Before moving on to the extraction phase, we introduce our polyline representation.
The goal that we would like to achieve is similar to the implicit surfaces.
For any point in the space, we would like to be able to calculate the distance from the translational surface and the corresponding surface normal.
The distance inside the shape should be negative, while positive outside.
Based on the definition we gave for the translational surface, we need two things to determine: a coordinate frame that defines the plane in which the contour is lying, and a closed contour.
The former is easy to handle and store, the latter requires more attention.

We chose a polyline structure to represent the contour, because it is relatively easy to implement and compute the needed values.
We define a polyline as: a set of line segments represented by a list of points.
The $i$-th point represents the beginning and the $i+1$-th the end of the $i$-th line segment.
For a polyline with length $n$, the beginning of the $n$-th line segment is the $n$-th, and the end is the first point.
Thus we do not store the beginning and the endpoint of the polyline multiple times.

\subsection{Distance computing}
\label{sec:poly_distance}
We measure the distance in the plane of the contour; therefore, the first step of any distance computation is to project the point onto the plane.
There are a few functions that need to be defined before moving on to the topic of distance measurement.
The first two is self-explanatory.
The \texttt{MidPoint} of a line segment is the mean of the beginning and the endpoint of the segment.
The \texttt{NearestPoint} of point $p$ and polyline $L$ is the index of the point in $L$ that is the closest to $p$.

The normal vector of a line segment is defined for a segment (a pair of points $[a,b]$) as the unit length vector that is perpendicular to the direction of the line segment and, ``points to the left" (see Fig.~\ref{fig:segmennormal} and Alg.~\ref{alg:segmentnormal} for details).

\begin{algorithm}[h]
	\caption{\texttt{SegmentNormal}}
	\label{alg:segmentnormal}
	\begin{algorithmic}[1]
		\REQUIRE segment $L_i$ ($[a,b]$)
		\STATE $v^d \leftarrow \mathtt{normalize}(b-a)$
		\STATE $v_n \leftarrow [-v^d_2, v^d_1]$
		\RETURN $v_n$
	\end{algorithmic}
\end{algorithm}

Based on this, we can compute the normal to the $i$-th segment of a polyline (called $i$-th \texttt{SegmentNormal}) as merely the normal vector of the line segment $[L_i, L_{i+1}]$.
The last segment normal of a size $n$ polyline is the normal vector of the line segment $[L_n, L_1]$.
This method is consistent in the sense that all the normals point to the inside or to the outside of the contour.

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{segmentdistance_finish.pdf}
		\caption{Segment distances}
		\label{fig:segmentdistance}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\centering
		\includegraphics[width=0.6\textwidth]{segmentnormal.pdf}
		\caption{Segment normal}
		\label{fig:segmennormal}
	\end{subfigure}
	\caption{Distances to a segment and direction of its normal}
	\label{fig:segmentdn}
\end{figure}

We also use the distance of a point $p$ to a segment $[a,b]$, called \texttt{SegmentDistance}.
This is the distance of $p$ from its projection to the segment $[a,b]$.
If its projection falls outside of $[a,b]$, the distance of the closest endpoint is used.
See Alg.~\ref{alg:segmentdistance} and Fig.~\ref{fig:segmentdistance} for an explanation.
Similar to the $i$-th \texttt{SegmentNormal}, one can compute the $i$-th \texttt{SegmentDistance}.

\begin{algorithm}[h]
	\caption{\texttt{SegmentDistance}}
	\label{alg:segmentdistance}
	\begin{algorithmic}[1]
		\REQUIRE point $p$, segment $L_i$ ($[a,b]$)
		\STATE $v \leftarrow \mathtt{normalize}(b-a)$
		\STATE $a_{pv} \leftarrow v((p-a) \cdot v)$
		\STATE $p_v \leftarrow a+a_{pv}$
		\STATE $i \leftarrow |b_1-a_1| > |b_2-a_2|\ ?\ 1 : 2$ \{from $x$ and $y$ use those with greater distance\}
		\STATE $t \leftarrow \frac{p_i-a_i}{b_i-a_i}$
		\IF{$t < 0$}
		\RETURN $|p-a|$
		\ELSIF {$t > 1$}
		\RETURN $|p-b|$
		\ELSE
		\RETURN $|p-p_v|$
		\ENDIF
	\end{algorithmic}
\end{algorithm}

The expression on line~4 (\texttt{condition ? a : b}) of Alg.~\ref{alg:segmentdistance} evaluates as follows: return \texttt{a} if the condition is true, \texttt{b} otherwise.

The distance of point $p$ from the contour is simply the distance of the nearest point of the polyline.
Note that it is an absolute value; we will deal with its sign later.
We shall refer to this distance as \texttt{ContourDistance}.

\subsection{Normal computing}
\label{sec:poly_normal}
At this step of the calculation, we do not care yet if the surface normal points inwards or outwards.
As we cannot be sure that the thinning algorithm returns a polyline with clockwise or counter-clockwise direction, we need to deal with that anyway (detailed in Section~\ref{sec:extr_signs}).

As our contour representation is continuous but it makes sharp turns, it is not differentiable.
Therefore we do not expect the surface normal to be smooth along the curve.
However, we would like to avoid sudden changes, as the normal vector has an impact on the sign of the distance, and may result in undesirable artefacts.
The following method gives a satisfactory result for an arbitrary point $p$.
First, we find the nearest point (with index $w$) of the polyline $L$.
We assume that the $w-1$-th and $w$-th line segments are the closest segments and, compute the distance of $p$ to those segments ($d_{w-1}$, $d_w$), and the segment normal of those segments $[n_{w-1}, n_w]$.
Then the weighted average is used as the surface normal.
Alg.~\ref{alg:dn2contour} shows the equation; the closer one of the points is, the less weight the other will have.
We shall refer to this method as \texttt{ContourNormal}.

\begin{algorithm}[h]
	\caption{\texttt{ContourDistanceAndNormal}}
	\label{alg:dn2contour}
	\begin{algorithmic}[1]
		\REQUIRE polyline $L$, point $p$
		\STATE $d,w \leftarrow \mathtt{NearestPoint}(p, L)$ \{distance and index of the nearest point to $p$ \}
		\STATE $d_{w-1} \leftarrow \mathtt{SegmentDistance}(p, L_{w-1})$
		\STATE $d_{w} \leftarrow \mathtt{SegmentDistance}(p, L_{w})$
		\STATE $n_{w-1} \leftarrow \mathtt{SegmentNormal}(L_{w-1})$
		\STATE $n_{w} \leftarrow \mathtt{SegmentNormal}(L_{w})$
		\STATE $n \leftarrow \frac{d_{w} \cdot n_{w-1} + d_{w-1} \cdot n_{}}{d_{w-1}+d_w}$
		\RETURN $d, n, w$
	\end{algorithmic}
\end{algorithm}

Alg.~\ref{alg:dn2contour} for an arbitrary point $p$ returns the \texttt{ContourDistance}, the \texttt{ContourNormal} and the index of the point of the line segment that is closest to $p$.
This way we can efficiently query the needed values and do not have to calculate them separately.

\section{Extraction}
\label{sec:transl_extr}
As mentioned earlier, the translational surface gets its final form in this step. 
As in reverse engineering, the final goal is a compact representation of the input, so we need to find a representation for the contour of the translational surfaces.
Fitting lines, circles or higher-order polynomials exceeds the scope of this thesis; therefore, a polyline representation is used (detailed in Sec.~\ref{sec:extr_polyline}).

The input of this step is a fitted translational surface, namely a coordinate frame and a set of points $P^f$ that define the contour.
To handle noisy inputs, a thinning algorithm is deployed (Sec.~\ref{sec:extr_thinning}).
The output of the thinning is a polyline $L_t$ with unknown trace direction (clockwise or counter-clockwise).
As described in Sec.~\ref{sec:poly_normal}, computation of the normals is consistent, but we need to detect if they point inwards or outwards of the contour.
This step is detailed in Section~\ref{sec:extr_signs}.
With these steps completed, the translational surface is fully defined and finished.
One final thing to do is to search all the compatible points of the input point cloud, and finish the extraction, as with all other primitives.

\commentstart
The description of the polyline representation is given in Section~\ref{sec:extr_polyline}.
We need to handle noisy inputs; in Section~\ref{sec:extr_thinning}, a thinning algorithm is described that is capable of doing so.
The extraction has one more step sign of the normal vectors must be found (Section~\ref{sec:extr_signs}).
Finally, a summary of the extraction process is given in Section~\ref{sec:extr_summary}.
\commentend

\subsection{Thinning algorithm}
\label{sec:extr_thinning}
%The input of this step is a fitted translational surface, namely a coordinate frame and a set of points $P_f$ that define the contour.
%As described earlier the extraction has two major steps; thinning is the first of them.
Input of the thinning algorithm is an unorganized set of points $P^f$ that describes the contour.
The problems that we have to deal with are: $P^f$ is potentially noisy and the input is unorganized, but the polyline representation is based on an ordered list of points.
Thus $P^f$ must be filtered and sorted.
For this purpose, we used the following thinning algorithm, based on~\cite{lee2000}.
In brief: we build a graph of the point set and search its \emph{Euclidean Minimum Spanning Tree} (EMST).
Using this tree we can search for a starting point wherefrom we go through the tree and utilize a simple filtering algorithm.

Description of the EMST and also Algorithm~\ref{alg:collect1} is based on~\cite{lee2000}.
$P^f$ is a point set, defined as $\{ p_i=(x_i,y_i) \ | \ i=1, \ldots,N \}$.
Consider a graph $\mathcal{G} = \{P^f,\mathcal{E}\}$ having total connectivity among all point elements: $\mathcal{E} = \{ (\mathbf{P}_i,\mathbf{P}_j) \ | \ i,j=1 \ldots, N, i\neq j \}$.
The EMST of $\mathcal{G}$ is a tree (a graph having no cycle) connecting all points in $\mathcal{S}$ so that the sum of its edge lengths is minimal.
The EMST of $\mathcal{G}$ can be computed (for example) by the well-known Kruskal's algorithm.
Using the total connectivity graph is not a scalable solution, but we can exploit that the EMST is a subgraph of the \emph{Delaunay triangulation} (DT).
One way of computing the DT efficiently is described in~\cite{deBerg1997}.
%Delaunay triangulation is such a triangulation of a point set $P$, that no point in $P$ is inside the circumcircle of any triangle in DT.

Algorithm~\ref{alg:collect1} is used to collect a set of neighbouring points for $\mathbf{P}_*$ with respect to a distance $H$.
An empty set is initially assigned to $\mathcal{A}$, then $\mathbf{Collect1}(\mathbf{P}_*,\mathbf{P}_*,H,\mathcal{A})$ is called.
The size of $H$ must be determined to reflect the thickness of the point set, in other words it is related to the magnitude of the noise of the point cloud.

\begin{algorithm}[h]
	\caption{\texttt{Collect1}~\cite{lee2000}}
	\label{alg:collect1}
	\begin{algorithmic}[1]
		\REQUIRE $\mathbf{P}, \ \mathbf{P}_*, \ H, \ \mathcal{A}$
		\STATE $\mathcal{A} \leftarrow \mathcal{A} \cup \{ \mathbf{P} \}$
		\FOR{\texttt{each edge} $(\mathbf{P},\mathbf{P}_j)$ \texttt{in EMST}}
		\IF{$\mathbf{P}_j \notin \mathcal{A}$ \texttt{and} $\| \mathbf{P}_* - \mathbf{P}_j \| < H $}
		\STATE \textbf{Collect1}($\mathbf{P}_j, \ \mathbf{P}_*, \ H, \ \mathcal{A}$)
		\ENDIF
		\ENDFOR
	\end{algorithmic}
\end{algorithm}

\begin{algorithm}[h]
	\caption{\texttt{Thinning}, based on~\cite{lee2000}}
	\label{alg:thinning}
	\begin{algorithmic}[1]
		\REQUIRE $\mathbf{P}, \ H$
		\STATE $tri\_edges \leftarrow \mathtt{delaunay}(\mathbf{P})$ \{Delaunay triangulation\}
		\STATE $tree \leftarrow EMST(tri\_edges, \mathbf{P})$ \{calculate the EMST\}
		\STATE $ordered \leftarrow \mathtt{bfs}(\mathtt{bfs}(EMST)[end])$ \{order graph with two consecutive BFS\}
		\STATE $thinned \leftarrow 0$ \{initialize to empty set\}
		\WHILE{$ \mathtt{! isempty}(ordered)$}
		\STATE $adjacent \leftarrow 0$ \{initialize to empty set\}
		\STATE $adjacent \leftarrow \mathbf{Collect1}(\mathbf{P}, \ 1, \ H, \ adjacent)$ \{search neighbours\}
		\STATE $thinned \leftarrow thinned \cup \mathtt{mean}(adjacent)$ \{compute the mean of neighbouring points\}
		\STATE $ordered \leftarrow ordered \setminus adjacent$ \{remove points that have been visited or used\}
		\ENDWHILE
	\end{algorithmic}
\end{algorithm}

Starting from any point with a breadth-first search (BFS) on the EMST we get to one end of the EMST.
Then from that starting point with another breadth-first search we traverse through the EMST and utilize a filtering algorithm.
Algorithm~\ref{alg:thinning} describes this step.
We initialize an empty set of points as our thinned polyline.
Based on the EMST, we traverse through the graph.
For every point we visit, search the points that are closer than the $H$ thinning parameter.
We calculate the mean of those points, and add it to the thinned polyline, then remove them from the graph.
We continue this until the points of the EMST run out.

\begin{figure}[H]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{thin_in}
		\caption{Input: segmented points}
		\label{fig:thinning1}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{transl_delaunay}
		\caption{Delaunay triangulation}
		\label{fig:thinning2}
	\end{subfigure}
	\vfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{transl_spantree}
		\caption{Euclidean Minimum Spanning Tree}
		\label{fig:thinning3}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{transl_polyline}
		\caption{Polyline result}
		\label{fig:thinning4}
	\end{subfigure}
	\caption{Thinning the segmented contour of \emph{Model 1}}
	\label{fig:thinning}
\end{figure}

Fig.~\ref{fig:thinning} summarizes the thinning algorithm.
We start from a noisy, unorganized set of points (Fig.~\ref{fig:thinning1}).
From the Delaunay triangulation (Fig.~\ref{fig:thinning2}), the Euclidean Minimum Spanning Tree is computed (Fig.~\ref{fig:thinning3}).
Alg.~\ref{alg:thinning} with the input EMST results in the thinned polyline (Fig.~\ref{fig:thinning4}).

\subsection{Determining the sign of the surface normal and the distance}
\label{sec:extr_signs}
In this section, we tackle the problem of determining the sign of the normals and distances.
We work with the thinned polyline ($T$) and the point-normal pairs that were defined at the fitting step ($P^f$ and $N^f$).
We want the normal of the polyline to point outside of the shape, but it is not guaranteed by the thinning algorithm.
Therefore we iterate through all the point-normal pairs and inspect if the computed contour normals point to the same direction or not.
Algorithm~\ref{alg:normaldirs} describes our approach.

\begin{algorithm}[H]
	\caption{Compute the direction of the normals}
	\label{alg:normaldirs}
	\begin{algorithmic}[1]
		\REQUIRE polyline $T$, points and normals ($P^f$, $N^f$), $\epsilon_{translational}$, $\kappa_{normals}$
		\STATE $c_t \leftarrow \mathtt{centroid}(T)$
		\STATE $n_g \leftarrow 0$ \{number of points within $\epsilon_{translational}$\}
		\STATE $n_{inw} \leftarrow 0$ \{number of normals that must be flipped to point outwards\}
		\FOR{$i \ in \ 1 \dots n $} %\{for each point in $P^f$\}
		\STATE $d_i, n_i, T_i \leftarrow \mathtt{ContourDistanceAndNormal}(T, P^f_i)$
		\IF{$d_i > \epsilon_{translational}$}
		\CONTINUE
		\ENDIF
		\STATE $n_g \leftarrow n_g+1$
		\IF{$ N^f_i \cdot \overrightarrow{c_t T_i} < 0 $}
		\STATE $n_{inw} \leftarrow n_{inw} +1$ \{normal must be flipped\}
		\ENDIF
		\ENDFOR
		\STATE $r_{flip} \leftarrow \frac{n_{inw}}{n_g}$
		\IF{$r_{flip} > \kappa_{normals}$}
		\STATE $s_{flip} \leftarrow -1$
		\ELSIF{$r_{flip} < 1-\kappa_{normals}$}
		\STATE $s_{flip} \leftarrow 1$
		\ELSE
		\ABORT
		\ENDIF
		%endof first section
		\STATE $n_{meas} \leftarrow 0$ \{number of normals that must be flipped to match the measurements\}
		\FOR{$i \ in \ 1 \ldots n $} %\{for each point in $P^f$\}
		\STATE $d_i, n_i, T_i \leftarrow \mathtt{ContourDistanceAndNormal}(T, P^f_i)$
		\IF{$d_i > \epsilon_{translational}$}
		\CONTINUE
		\ENDIF
		\IF{$ s_{flip}n_i \cdot \overrightarrow{c_t T_i} < 0 $}
		\STATE $n_{meas} \leftarrow n_{meas} +1$ \{normal must be flipped\}
		\ENDIF
		\ENDFOR
		\STATE $r_{meas} \leftarrow \frac{n_{meas}}{n_g}$
		\IF{$r_{meas} > \kappa_{normals}$}
		\STATE $s_{meas} \leftarrow -1$
		\ELSIF{$r_{meas} < 1-\kappa_{normals}$}
		\STATE $s_{meas} \leftarrow 1$
		\ELSE
		\ABORT
		\ENDIF		
		\RETURN $s_{flip}$
	\end{algorithmic}
\end{algorithm}

First, we compute the $c_t$ centre of mass (also called \texttt{centroid}) of the thinned polyline (we take the mean of the points in $T$).
We iterate through the point-normal pairs and calculate the \texttt{ContourDistance} $d_i$, \texttt{ContourNormal} $n_i$ and the nearest point $T_i$.
We discard those points that are further away than an $\epsilon_{translational}$ threshold to filter out any leftover noise.

The next step is to compute the vector that points from the centre $c_t$ to the nearest point: $\overrightarrow{c_t T_i}$.
This vector represents the direction outside the contour.
To determine if the contour normal directs outside we inspect the sign of the dot product of this vector ($\overrightarrow{c_t T_i}$) and the contour normal $n_i$.
If it is negative, we have to flip the contour normal to point outside.
Throughout the iteration, we keep track of the number of points that are in the $\epsilon_{translational}$ band and also for how many points must the contour be flipped.
We define a threshold $\kappa_{normals}$ that defines how many of the normals must agree on flipping or not-flipping the normals scaled with the points in the epsilon band.

If the ratio of the to-be-flipped normals meets this threshold, we conclude that the normals must be flipped.
If the ratio for the not-to-be-flipped normals meets the threshold we conclude that the normals must not be flipped.
For both cases, a flip-sign variable is set to either $-1$ or $1$.
In the case the ratios do not meet the threshold, we discard the candidate and do not extract it.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{transl_normals}
	\caption{Contour and surface normals of \emph{Model 1}}
	\label{fig:transl_normal}
\end{figure}

Now we have a method to compute the surface normal of the polyline (\texttt{PolylineNormal}) for an arbitrary point (equals to the \texttt{ContourNormal} times the flip-sign).
An example is given for the contour and corresponding surface normals in Fig.~\ref{fig:transl_normal}.

We want to point out that the described method only works for convex polylines, and may not for concave.
There are other algorithms that can handle these cases, for example computing the signed area of the polygon (defined by the polyline).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{dist2_finish.pdf}
	\caption{Polyline distance}
	\label{fig:signed_dist}
\end{figure}

Next step is to define a method to calculate a distance that is negative in the inside and positive on the outside of the contour.
The absolute value is provided by the \texttt{ContourDistance} method.
For an arbitrary point $p$ first, we compute the \texttt{ContourDistance} ($d_i)$, the \texttt{PolylineNormal} ($n^p_i$), and also the nearest point of the polyline $T_i$.
%Then we consider the sign of the dot product of the polyline normal ($n^p_i$) and the vector directing from the nearest point of the contour ($T_i$) to the point ($p$).
For an arbitrary point $p$, we decide the sign of the distance by comparing the polyline normal ($n^p_i$) with the vector which points from the nearest point to the point ($\overrightarrow{p T_i}$).
If the dot product of $n^p_i$ and $\overrightarrow{p T_i}$ is negative, the point is inside of the contour, therefore its sign is $-1$.
Algorithm~\ref{alg:polyline_dist} and Fig.~\ref{fig:signed_dist} presents the method.

\begin{algorithm}[h]
	\caption{\texttt{PolylineDistance}}
	\label{alg:polyline_dist}
	\begin{algorithmic}[1]
		\REQUIRE polyline $T$, point $p$
		\STATE $d_i, n_i, T_i \leftarrow \mathtt{ContourDistanceAndNormal}(T, P^f_i)$
		\STATE $n^p_i \leftarrow s_{flip} n_i$ \{polyline normal at $p$\}
		\STATE $s_{d} \leftarrow  n^p_i \cdot (p-T_i) < 0 \ ? \ -1 : 1$ \{distance sign\}
		\STATE $d_{signed} \leftarrow s_d d_i$
		\RETURN $d_{signed}$
	\end{algorithmic}
\end{algorithm}

As in our implementation of the RANSAC paradigm, we distinguish between positive and negative shapes; therefore we compare the polyline normals at every $P^f_i$ with the measured normals $N^f_i$.
Similarly to the previous approach at each iteration, we observe the dot product of the polyline normal (that always points outwards) at $P^f_i$ and the measured normal $N^f_i$.
If for most points it is negative, then the translational surface is an inward shape.
This is calculated together with the flip-sign ($d_{flip}$) in Alg.~\ref{alg:normaldirs}.

To summarize the refitting steps, the following defines a translational surface: a coordinate frame that represents a plane, a closed contour in polyline form in that plane, a flip-sign that helps to compute outside directing surface normals, a bit that represents the positivity of the shape and also the centre of mass of the polyline is stored as it is used quite often.

The final step of the extraction is the compatibility measure, we search for all the compatible points in the point cloud.
Therefore the points and their normals must be projected to the plane of the translational surface.
We consider only those points, whose normal is perpendicular to the translational direction.
Then the usual $\epsilon_{translational}$ and $\alpha_{translational}$ compatibility measures are used, but for the normals, we take the positivity of the shape into account (as with all other primitives).

\section{Implicit compatibility}
\label{sec:transl_impl_comp}
As described in Sec.~\ref{sec:impl_rep}, to work together with the other implicit primitives, two methods need to be defined: \texttt{value}() and \texttt{normal}().
We kept this in mind when constructing the representation of the translational surface, therefore it can be used as is.
The \texttt{value}() function maps to computing the \texttt{PolylineDistance}(), and \texttt{normal}() is equivalent to the \texttt{PolylineNormal}().
Of course, three-dimensional points (and normals, if we want to compare) must be projected to the plane defined by the coordinate frame of the translational surface before using these functions.

Similarly to the simple primitives, one can evaluate a translational surface on a three-dimensional grid to visualize it in ParaView.
This is shown in Fig.~\ref{fig:m1_pw}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{m1_pw}
	\caption{Rendering of the reconstructed translational surface}
	\label{fig:m1_pw}
\end{figure}

Since implicit surfaces are halfspaces, the surface is cropped at the boundaries of the three-dimensional grid.
As can be seen, the surface of the model is groovy, because the contour consists of small line segments.
We could eliminate those artefacts with another representation, for example by fitting lines and circles to larger connected components.
%TODO
%ridges, groovy, rugged
