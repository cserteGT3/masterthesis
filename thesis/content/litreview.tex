%----------------------------------------------------------------------------
\chapter{Literature review}
\label{sec:litrev}
%----------------------------------------------------------------------------
Nowadays, reverse engineering (RE) has a broader interpretation than just the reconstruction of shapes.
Understanding of design intents and mechanisms is one of the focus of today's research on reverse engineering.
To summarize the process of reconstruction of geometric models, focusing on engineered objects, we refer to~\cite{VARADY1997} and~\cite{buonamici17}.
As shown in Fig.\ref{fig:varady_steps}, reconstruction can be separated into four steps: \emph{data capture}, \emph{preprocessing}, \emph{segmentation and surface fitting} and \emph{CAD model creation}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{varady_steps.pdf}
	\caption{Basic phases of reverse engineering~\cite{VARADY1997}}
	\label{fig:varady_steps}
\end{figure}

There are many different instruments and methods to acquire three-dimensional shape data.
Two main types can be differentiated: contact and non-contact methods.
In the former, the surface of the object is touched by using mechanical probes, while in the latter light, sound or magnetic fields are used to trace the surface.
Contact devices, such as coordinate measurement machines or robotic arms, are in general slower but may achieve higher precision measurement (depending on the exact circumstances).
Non-contact methods, however, are faster and rather economic, but they are usually less accurate and could be potentially affected by the surface properties of the object.
There are several different principles utilized by such instruments: triangulation, measurement of distance by sensing time-of-flight of light beams, interferometry (recognition of interference patterns in the order of the wavelength of light).
Structured lighting involves projecting patterns of light on the object, and capturing the reflection by digital cameras.
With appropriate image processing algorithms, relatively high accuracy can be achieved.
With the great interest in \emph{Artifical Intelligence} (AI) and \emph{Machine Learning} (ML), image analysis based methods are also spreading.
Irradiation-based techniques such as computed tomography or X-ray scanning must also be mentioned; however, these solutions are relatively expensive.

The next step is the preprocessing of the acquired data.
Of course, applicable methods heavily depend on the measurement approach.
Preprocessing has its own literature, let us mention here just some of the problems that one may have to deal with.
In the case of multiple recorder devices or multiple views, the alignment of the segments must be solved.
For point clouds, the \emph{Iterative Closest Point} (ICP) algorithm is a suitable applicant, see~\cite{ROB-035} for a review on its different variants.
One may have to deal with removing the fixture of the measured object.
Handling of missing or noisy measurements may be a problem that must be solved.

Segmentation and fitting is one (if not the most) important and challenging part of the reconstruction process.
It consists of three steps:
\begin{labeling}{\textbf{classification}}
\item [\textbf{segmentation}] to logically divide a point set into subsets, one for each surface, so that each subset contains points corresponding only to its surface;
\item [\textbf{classification}] to decide to what type of surface each subset of points belongs to; and
\item[\textbf{fitting}] to find the parameters of the given type which is the best fit to those points in the given subset.
\end{labeling}
These steps are tightly connected, so often it is not possible to distinguish one from the other.
Some methods will be introduced together with the CAD model creation.


The final step of reverse engineering is the creation of the CAD model.
It is hard to give a detailed, multipurpose method, as this step (naturally) depends on the whole reconstruction framework, as well as the kind of the desired result (parametric or non-parametric).
A few of the possible operations that could be done at this step: stitching existing surfaces together (with blending surfaces), generating fillets and chamfers and applying geometric constraints (symmetry, concentricity, parallelism, etc.)

\section{Reconstruction strategies}
\label{sec:literv_strats}
In this section, we introduce a few methods that are used to reconstruct engineered objects.
The summary is based on~\cite{buonamici17}.

\subsection{Feature-based reconstruction strategies}
\label{sec:strats_featurebased}

\subsubsection{Independent fitting of surfaces}
\label{sec:independent_fitting}
Independent fitting of surfaces represents the early strategy to reconstruct an object from 3D data.
In this approach, the segmented groups are handled separately, and a straightforward fitting of a mathematical surface is performed.
In other terms, the distance between the surface and the set of triangles/points is minimized using an objective function selected to fit the purpose.
Reverse engineering methods based on independent fitting are among the less computationally expensive ones, and are the most suitable choice if the desired fitting goal is only related to minimizing the distance to the measured data.
However, as fitting is separated from the segmentation step, the results heavily depend on the quality of the segmentation.
Also, these methods do not incorporate any factors that point towards the exploration of designer intent, such as geometric constraints or relations between surfaces and features.
For the above mentioned reasons, independent fitting of surfaces is sometimes used as a base for more complex algorithms.

\subsubsection{Constrained fitting}
\label{sec:constrained_fitting}
One of the most researched fields of CAD reconstruction is constrained fitting~\cite{benkHo2001algorithms}.
These methods introduce geometrical constraints between the analytical surfaces and features, enforcing them during the fitting step.
A few examples of constraints are parallelism of axes, orthogonality of planes or even symmetries and regularities (i.e., feature patterns).
Although the enforcement of known or inferred constraints may introduce slight differences from the analytical surfaces, the reconstructed model tends to respect the design intent.
There are two approaches to determine the ``correct" constraints: user-guided and automatic strategies.
User-guided approaches exploit the experience and knowledge of the reverse engineer.
The engineer can potentially recognize the functionality and structure of the object to be reconstructed.
This strategy offers the substantial advantage of relying on human expertise since designer insights can be exploited, and meaningless mistakes (which may be introduced by automated processes) can be avoided.
The second strategy relies on the automatic identification of constraints and regularities.
In most cases, these constraints are searched on a model obtained by a first-attempt fitting (without introducing any constraints).
Among the constraints that have been recognized, a coherent and significant subset must be found.
The former means that the selected constraints can coexist at the same time, while the latter characterizes the need for such constraints that are advantageous for future modelling operations.

On a general level, constrained fitting based methods introduce effective solutions to reduce the gap between the reconstructed model and the original designer intent.
Nevertheless, the implementation complexity of geometrical constraints is a relevant issue that needs to be addressed.

\subsubsection{Reconstruction based on 2D mesh sections}
\label{sec:2dmesh_sections}
An alternative strategy undertaken by several researchers is reconstructing surfaces from 2D cross-sections of the original mesh~\cite{antonis2012}.
This concept is, in a way, similar to traditional 3D modelling, where volumes and surfaces are built from 2D parametric sketches using, for example, extrusions or sweeps.
This class of methods attempts to extract 2D sketches from cross-sections of the mesh or generally retrieve geometric information of the object on multiple planar sections.
These methods gain an advantage from the experience that generation/fitting of 2D profiles upon the original mesh cross-sections proves to be cheaper from a computational point of view.
However, the efficiency depends on the identification of the most significant 2D profiles to be used.

\subsubsection{Knowledge-based methods}
\label{sec:knowledge_based}
Although the field of reverse engineering greatly benefits from the increasing performance of computers, in most cases human interaction is still key to a successful reconstruction.
For instance, the selection of different thresholds is still issued to the user based on his or her expertise and experiences.
Starting from the consideration that ``computers are good at data analysis and fitting operation; and humans are good at recognizing and classifying patterns"~\cite{FISHER2004501}, a new class of RE techniques appeared that explicitly rely on human capabilities to provide or retrieve high-level knowledge or information.
These are collected in the class of knowledge-based methods.
A few of them is introduced in~\cite{buonamici17}.
In general, those knowledge-based methods that rely on low or even no user interaction have been successfully tested only in lower complexity tasks, with a smaller number of basic geometric shapes.
As the number of detectable features grows, a severe complexity burden is expected as all the possible interactions between the features overgrow.
Nonetheless, knowledge-based methods that do rely on user cooperation in key tasks (such as classification of features or constraint definitions) have been successfully applied on more complex cases.

\subsection{Freeform surfaces}
\label{sec:strats_freeform}
Use of freeform surfaces is an extensively investigated field of RE.
A broad range of methods is available to reconstruct objects based on B-Spline or NURBS representation.
Two main advantages of such methods are the required little-to-none user assistance and the capability of reproducing any 3D shape.
Although freeform surface methods generally generate an aesthetically pleasing result with potentially little deviation error, they do not provide information beyond the mere 3D geometry.
This limits the use of such methods on their own in the reconstruction process of engineered objects.
Nevertheless, as many engineered parts do have freeform surface pieces, these methods may be combined with feature-based reconstruction techniques, for example by recognizing not only ``traditional" features (i.e. planes, quadrics, swept surfaces), but freeform surfaces as well.
Some of these hybrid strategies are described briefly in~\cite{buonamici17}.

\section{Reconstruction of engineered objects}
\label{sec:reconstr_engineered}
The previously given overview introduced techniques from a broader perspective.
Let us now focus on the reconstruction of engineered objects.
First, a few complete reconstruction techniques will be described, then various methods to construct CSG-trees.
%TODO enable this if needed
%As most techniques, as well as our choice, relies on detecting primitives, a short overview of of those methods will be described, mainly focusing on the RANSAC paradigm.
Finally, we present two approaches that address the problem of reconstructing translational and rotational surfaces.

\subsection{A framework for 3D model reconstruction in reverse engineering}
\label{sec:wang2011}
Wang et al.~\cite{wang2011} introduce a framework to reconstruct geometric models from surface meshes.
Their method is capable of producing the boundary representation of a complex 3D object.
They can automatically extract geometric features and reconstruct CAD models from low-quality meshes.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{wang2011-3}
	\caption{A systematic flowchart for model reconstruction from a surface mesh.~\cite{wang2011}}
	\label{fig:wang2011}
\end{figure}

Their approach utilizes four main steps (Fig.~\ref{fig:wang2011}): pre-processing the input mesh to filter out noise, partitioning into distinct geometric feature patches, reconstructing primitive features exploiting solid and surface feature based strategies, and utilizing modelling operations such as solid Boolean operations and surface trimming.

To pre-process the input mesh, they utilize an efficient, feature preserving mesh denoising approach, based on
anisotropic neighbourhood searching and surface fitting.

In the next step, they segment the mesh into patches, each of which corresponds to a single feature surface.
A region growing based plane segmentation technique is used to achieve an initial segmentation.
For each segment, the standard deviation of the face normals is computed to decide whether the segment is a plane.
If not, further segmentation is needed.
Remaining patches are submitted to a quadratic surface segmentation.
The used method is based on region growing and Gauss mapping the normals of the faces.
As a result, the quadric surface can be recognized as a cylinder, sphere or cone.
If neither, the patch is identified as a free-form surface, and a bicubic B-spline surface fitting method is applied.
With these steps finished, the final segmentation refinement is achieved.

The authors build up the 3D model from solid features and surface features.
The considered solid primitive features are: extrusion, revolution, sweep and loft features.
The authors convert the problem of feature reconstruction into extracting feature parameters.
To summarize the methods deployed after finding the line that drives the given feature (extrusion direction, revolution axis, sweep and loft path), a constraint-based fitting method is used to find the contour of the feature.
In the case of parts that are created by trimming solid geometry with surfaces, the quadratic or freeform surface features must be reconstructed as well.
For quadratic surfaces, a constrained optimization method is used, while for freeform surfaces, an error-based adaptive fitting algorithm.

After recovering the solid features and surface features, a modelling operation history tree is constructed.
The history tree records the primitives and their relations (Boolean operations, surface trimming and stitching) to build up the final reconstructed model.

Their results show a robust method that is capable of reconstructing complex engineered objects.
As presented in the paper (see Fig.~9 in~\cite{wang2011}), they can recognize the contour of extruded and lofted features.
However, their method of constructing the history tree is not descriptive, and their algorithm requires a triangulated mesh as an input.

\subsection{Automatic extraction of surface structures in digital shape reconstruction}
\label{sec:varady2007}
In~\cite{varady2007} the authors present an automatic process to create a CAD-like surface structure over a polygonal mesh.
Their goal is to separate highly curved transition regions and primary regions, thereby creating a structure that reflects the original design intent.
Their process consists of five phases: hierarchical Morse complex segmentation, feature skeleton construction, computing region boundaries, surface structure creation and surface fitting.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{varady2007-2}
	\caption{\textbf{(a)}~Separator sets and feature skeleton. \textbf{(b)}~Features, vertex blends and the final surface structure~\cite{varady2007}}
	\label{fig:varad2007_method}
\end{figure}

The first step is the segmentation of the input triangulated mesh with hierarchical Morse complex segmentation.
Piecewise linear functions are estimated from the curvature indicator values computed at each vertex that highlight the highly curved parts of the shape (dark blue lines on Fig.~\ref{fig:varad2007_method}~\textbf{a}).

Thickening these curves results in \emph{separator sets} (red triangles in Fig.~\ref{fig:varad2007_method}~\textbf{a}) that segment the mesh to monotonic regions.
Thus the result is a set of relatively flat regions, separated by triangles of highly curved transitions.

In the next step, a \emph{feature skeleton} is constructed.
It is a network of smooth edges running in the middle of the separator sets.
As shown in Fig.~\ref{fig:varad2007_method}~\textbf{a}, a separator set may indicate
\begin{enumerate*}[label=(\emph{\roman*})]
\item a smooth connecting feature between two regions (fillet for example),
\item a sharp feature, or
\item a subdividing curve that cuts larger regions into smaller ones.
\end{enumerate*}
Feature edges are only temporary, while sharp edges and subdividing curves will be present in the final model.

In phase three and four, the final region boundaries and surface structure are created.
For a more in-depth discussion, the reader is referred to Section 4 and 5 of~\cite{varady2007}.
One thing that we would like to note is that one must handle vertex blends which replace certain vertices of the feature skeleton.

The above steps result in relatively large primary regions that are devoid of highly curved features.
The final step is to approximate these regions.
For this purpose, many concepts are available, the authors suggest the use of trimmed surfaces to obtain conventional CAD models.

\begin{figure}[h]
	\centering
	\begin{subfigure}[t]{0.24\textwidth}
		\centering
		\includegraphics[width=\textwidth]{varady2007-9_01}
		\caption{Separator sets}
		\label{fig:varady_example_1}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.24\textwidth}
		\centering
		\includegraphics[width=\textwidth]{varady2007-9_02}
		\caption{Feature skeleton}
		\label{fig:varady_example_2}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.24\textwidth}
		\centering
		\includegraphics[width=\textwidth]{varady2007-9_03}
		\caption{Thickened feature skeleton, aligned with curvatures.}
		\label{fig:varady_example_3}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.24\textwidth}
		\centering
		\includegraphics[width=\textwidth]{varady2007-9_04}
		\caption{Primary regions}
		\label{fig:varady_example_4}
	\end{subfigure}
	\caption{Example from~\cite{varady2007}}
	\label{fig:varady_example}
\end{figure}

The proposed process ends with a well-aligned, feature-based structure (see example in Fig.~\ref{fig:varady_example}); however, more steps are needed to extract a finished CAD model.
Model history, engineering constraints (orthogonality, symmetry, etc.) or often used features (extrude, revolve) are not incorporated into this reconstruction method.
Nonetheless, this work can give a strong basis for further processing with its excellent segmentation capabilities.

\section{Reconstruction of CSG-trees}
\label{sec_csgrec}
In this section, three methods will be introduced that address the problem of CSG-tree building from point clouds.
Common to all three methods that the first major step is the search of primitives in the point cloud.
A broad review of primitive fitting is described in~\cite{kaiser18}.

\subsection{An evolutionary approach to the extraction of object construction trees from 3D point clouds}
\label{sec:lit_fayolle}
Fayolle and Pasko~\cite{fayolle16} suggest an evolutionary approach to build the CSG-representation of an object from a measured point cloud.
First, the input point cloud is segmented into primitive surfaces such as plane, cylinder, cone, sphere and torus.
For this purpose, the efficient RANSAC algorithm~\cite{effRANSAC} is utilized that will be introduced and described in detail later (see Sec.~\ref{sec:ransac}).

In the second step, separating primitives are identified and added to the list of fitted primitives.
With a list of primitives, genetic programming is used to evolve a construction tree with the fitted primitives in the leaves and geometric operations in the internal nodes.
The objective function penalizes the larger trees while promotes the ones with smaller error with respect to the distance from the point cloud to the evolved construction tree.
A detailed description of this method, as well as some implementation details, are given in Sec.~\ref{sec:fayolle_impl}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{POs}
	\caption{Efficient CSG-tree building~\cite{friedrich18}}
	\label{fig:fayolle_po}
\end{figure}

The objective function penalizes bloat trees, though computing the score function for such trees may still be computationally expensive.
To address this problem, in a recent work~\cite{friedrich18}, the authors introduce a possible method to speed up the computation.
The key idea is to partition the search space into independent groups of spatially overlapping primitives.
This exploits the fact that primitives that are not overlapping cannot be operands of a CSG-operation.
CSG-trees are built separately for the partitions, then merged in a final step.
Steps of the method are shown in Fig.~\ref{fig:fayolle_po}.
Their experiments show significant speed-up compared to the baseline evolutionary approach.

To summarize, in~\cite{fayolle16} an evolutionary approach is presented to build a construction tree of a scanned object.
In most cases, the algorithm results in a tree that satisfies the set criteria 
\begin{enumerate*}[label=(\emph{\roman*})]
	\item precise approximation of the input point cloud with
	\item a possibly small tree size.
\end{enumerate*}
The utilized genetic algorithm has the advantage that it is relatively easy to implement.
Drawbacks are that it is not ensured that the result is the optimal representation and the iteration may take a long time to finish.

\subsection{InverseCSG: Automatic Conversion of 3D Models to CSG Trees}
\label{sec:lit_inversecsg}
Du et al.~introduce a completely different approach in their work~\cite{du18}.
Based on the observation that CSG is a formal grammar, the reconstruction process is reformulated as a program synthesis problem.
In their work, they describe the grammar of the CSG-language, the reconstruction process, and publish a new library of several test CAD models.

In the first step, primitives are detected in the input mesh with an algorithm built on top of the efficient RANSAC approach.
To reconstruct solid objects, one must deal with separating primitives that are not visible, thus cannot be detected in the first step.
To find these primitives a method introduced by Shapiro and Vossler~\cite{sv93},~\cite{sv91a} is used, similarly to~\cite{fayolle16} (described in Sec.~\ref{sec:fayolle_impl}).
Result of these two steps is a list of primitives with their parameters defined, thus the mixed search problem is reduced into a discrete one.
As all the points of all primitives mean an infinite constraint set, the next phase is a sampling stage, which transforms the infinite constraint set into a finite one.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.95\textwidth]{duet}
	\caption{Steps of the InverseCSG algorithm~\cite{du18}}
	\label{fig:duet}
\end{figure}

This is achieved by searching all the canonical intersection terms (introduced in~\cite{sv93} and~\cite{sv91a}).
Based on the fact that each canonical intersection term is either fully included or excluded in the CSG-tree, so the input mesh can be accurately reconstructed if and only if each canonical intersection term is fully inside or outside the mesh.
As a result, there is no need to check every point of the mesh, it is sufficient to check only one representative point from each canonical intersection term.

Performing the above steps, the reconstruction problem is converted into a compact semantic search over a discrete language that can be efficiently solved with program synthesis techniques, feeding in the sampled points as constraints.
The result is a CSG-program (a CSG-tree), that approximates the input mesh geometry as close as possible.

Finally the result CSG-program is post-processed.
Potential redundancies are removed, and where possible, the model is reparameterized for easier use.
The latter covers, for example, a case where the parameters of two symmetric features are changed to a shared one for easier editing.

To summarize, in~\cite{du18} a program synthesis based approach is presented to build the CSG-tree of a scanned object.
According to the authors, this approach outperforms the existing algorithms in both precision (regarding the volumetric error) and computation time, though the implementation of the whole process requires significant effort.

\subsection{Constructing 3D CSG Models from 3D Raw Point Clouds}
\label{sec:lit_wu18}

In~\cite{wu2018} the authors propose an efficient method to construct CSG models and trees directly over raw point clouds.
As a first step, a robust fitting technique is applied to extract the surface patches.
From the fitted planar patches cuboid hypotheses are created, then filtered.
Quadratic primitives are directly used.
As in previous approaches, additional primitives are added if necessary.
For building the CSG-tree, a bottom-up solution is proposed, consisting of two phases, CSG-subtree construction and CSG tree-construction, obeying the general order for CSG tree recovery from the subtrees to the entity.
An example is shown in Fig.~\ref{fig:wu18}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{wu2018-9}
	\caption{CSG construction of a fairly complex mechanical part~\cite{wu2018}}
	\label{fig:wu18}
\end{figure}

To retrieve primitive patches, the GlobFit~\cite{globfit} algorithm is used where the patches are constrained by some global relations, including orientation, placement and equality alignments.
Primitive patches can be planar, cylindrical, conical, spherical or toroidal patches.
The fitted planar patches are grouped so that they form a cuboid hypothesis.
The number of initial cuboid hypotheses could be huge, which can certainly impact the performance of the extraction method.
Therefore the authors design three criteria to filter out redundant or unreasonable hypotheses.
A cuboid built from incorrectly oriented patches is discarded, and also those, whose planar patches do not explain well the hypothesis.
Finally, redundant cuboids are also rejected.
Quadratic patches represent their shape well, however -- similarly to~\cite{fayolle16} and~\cite{du18} -- additional, separating primitives must be added.
This additional primitive is the minimum bounding box of a corresponding quadratic primitive.
Again, redundant primitives are removed from the full list of primitives.

In the first step of CSG-tree construction, the minimum bounding box of the point cloud is voxelized.
Then for every voxel, its internal or external property is computed.
Based on the voxelization, primitives are labelled if they must be a part of a subtree or can be part of the final tree directly.
For every primitive that must be part of a subtree, its neighbour set is computed, then subtrees are constructed for every such set, considering only the intersection and difference Boolean operations.
In this way, the tree construction can be formed as a binary optimization since the sequence of the actions does not affect the subtree.
To solve the optimization, a BDD-based heuristic method is deployed.
Combining the primitives and the subtrees gives the list of candidates to build the CSG-tree from.
Solving a combinatorial optimization problem for these candidates, the final CSG-tree can be constructed.

Authors of~\cite{wu2018} demonstrate a method that can construct optimal CSG-trees that are geometrically consistent with the input data.
Their algorithm performs well and handles complex models.
The time required for the reconstruction is also satisfying for the presented examples; it is mainly within two minutes.
Limitation of the approach is the voxelization step, as finer subdivision requires more computation.
Also setting up and solving the optimization problems may require a more in-depth knowledge of those fields.

\section{Reconstruction of translational and rotational surfaces}
\label{sec:sec_translrot}
As the major contribution of this thesis is the description of the use of translational surfaces in CSG-representations, we describe two relevant techniques.
Both address the problem of reconstructing translational and rotational surfaces from point clouds.

\subsection{Extracting Geometric Features from Clouds of Points Using Sweeping}
\label{sec:yoon2008}

In~\cite{yoon2008} the authors propose a method to reconstruct swept surfaces from point clouds.
First, they introduce different types of sweeping.
Then the reconstruction algorithm is described, which consists of four steps: pre-processing, slice plane selection, projection of points to the slice planes and reconstruction of curves.
Finally, an example is given, phases of reconstructing a broken screw, see Fig.~\ref{fig:yoon_example}.

Authors categorize sweeping into three groups: translational, rotational and general sweeping.
Translational sweeping refers to moving a generator curve along a trajectory or spine curve.
A rotational sweeping surface is constructed by rotating a profile curve about an axis of rotation.
In both cases, the profile curve is constant.
However, in general sweeping, the generator curve may change its shape as it sweeps.

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{yoon2008_01}
		\caption{A broken screw}
		\label{fig:yoon_ex1}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.35\textwidth}
		\centering
		\includegraphics[width=\textwidth]{yoon2008_02}
		\caption{The oriented bounding box and the object frame}
		\label{fig:yoon_ex2}
	\end{subfigure}
	\vfill
	\begin{subfigure}[h]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{yoon2008_03}
		\caption{Obtained result.}
		\label{fig:yoon_ex3}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{yoon2008_04}
		\caption{Reconstruction of the missing part from Fig.~\ref{fig:yoon_ex1}}
		\label{fig:yoon_ex4}
	\end{subfigure}
	\caption{Broken screw example from~\cite{yoon2008}}
	\label{fig:yoon_example}
\end{figure}

The input of their algorithm is a point cloud with surface normals that corresponds to only one surface.
As pre-processing, they compute the oriented bounding box of the point cloud.
After selecting the trajectory plane, the bounding box is recursively subdivided to select the slice planes which contain the profile curve.
After projecting the points that are near to a plane, a thinning algorithm is used to organize the projected points.
To define the contour, NURBS interpolation is used on the thinned points.
The trajectory curve is also interpolated by a NURBS curve.
%Figure 13 gives an example of the reconstruction process.

The authors in~\cite{yoon2008} describe a method to reconstruct sweeping surfaces.
Their algorithm can reconstruct translational, rotational and general sweepings and handles missing data well.
We should point out, though, that the point cloud of a complex object must be segmented before using the described method.

\subsection{Best Fit Translational and Rotational Surfaces for Reverse Engineering Shapes}
\label{sec:benko2000}
In~\cite{benko2000} the authors propose a method to approximate a point set with either a translational or a rotational surface.
In the first part of the paper, fitting of such surfaces is described.
After determining the translational direction or the rotational axis, a \emph{guiding polygon} is constructed to assist the profile fitting.
Then, in the second part, a possible application is introduced, the use of their algorithm with region growing techniques to achieve segmentation and fitting.

First, the translational direction or the rotational axis must be determined from the input, a triangulated mesh.
The normal vectors of a translational surface must be perpendicular to the translational direction.
Based on this, we can formulate the search of the translational direction to a well known three-dimensional eigenvalue problem.

The normal lines of a rotational surface must intersect a common axis, the rotational axis.
However, finding this axis is not as trivial as finding the translational direction.
Pottmann and Randrup gave a solution for this problem in~\cite{randrup1998} using the so-called Plücker coordinates.

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{benko01}
		\caption{Fast fit went wrong}
		\label{fig:benko01}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{benko02}
		\caption{Safe fit stops in time}
		\label{fig:benko02}
	\end{subfigure}
	\vfill
	\begin{subfigure}[h]{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{benko03}
		\caption{Profile not manifold}
		\label{fig:benko03}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{benko04}
		\caption{Manifold profile}
		\label{fig:benko04}
	\end{subfigure}
	\caption{Example for fast and safe region growing~\cite{benko2000}}
	\label{fig:benko2000}
\end{figure}

Fitting the profile of the translational or rotational surfaces considering only points, is difficult.
Constructing a guiding polygon helps in the process, on the one hand by giving a ``natural" parametrization for parametric curves, and on the other hand, it helps to tackle the problem of thick point sets.

Finally, utilizing region growing, a translational or rotational surface can be fitted and segmented at the same time.
The authors present two methods, a safe and a fast one, both require a user-defined seed as a way to start.
As shown in Fig.~\ref{fig:benko2000} the fast version does not take into account the possible branching of the surface.
The safe version can handle branching profiles, however it is significantly slower.

In summary, the authors of~\cite{benko2000} propose a method to fit translational and rotational surfaces.
Their guiding polygon based approach gives a solution to thick, unorganized point sets.
However, their approach heavily builds on the triangulation of the input point set.