using StaticArrays
using AbstractTrees
using BSON

using Revise
using RANSAC
using CSGBuilding
using CSGBuilding: _name

cd(@__DIR__)

fname = "icsg_158csgstateoftheart-v1_genetic.bson"

bd = BSON.load(fname)
bcached = bd[:bestcached]

struct NiceTree2{A<:AbstractArray}
    data::Union{String,Function}
    children::A
end

AbstractTrees.children(tree::NiceTree2) = tree.children
AbstractTrees.printnode(io::IO, tree::NiceTree2) = print(io, tree.data)

function CSGNode2NiceTree(node::CSGNode)
    if isempty(node.children)
        return NiceTree2(_name(node.data), [])
    else
        op = node.data
        cs = node.children
        if op == CSGBuilding.complement
            return NiceTree2(op, [CSGNode2NiceTree(cs[1])])
        else
            return NiceTree2(op, [CSGNode2NiceTree(cs[1]), CSGNode2NiceTree(cs[2])])
        end
    end
end

struct NiceTree{A<:AbstractArray}
    data::String
    children::A
end

AbstractTrees.children(tree::NiceTree) = tree.children
AbstractTrees.printnode(io::IO, tree::NiceTree) = print(io, tree.data)

function CachedNode2NiceTree(node::CachedCSGNode)
    if isempty(node.children)
        return NiceTree(_name(node.data), [])
    else
        op = node.data
        cs = node.children
        if op === :complement
            return NiceTree(string(op), [CachedNode2NiceTree(cs[1])])
        else
            return NiceTree(string(op), [CachedNode2NiceTree(cs[1]), CachedNode2NiceTree(cs[2])])
        end
    end
end

ntr = toNiceTree(bcached)
toJSON("t.json", ntr)

toJSON("t2.json", (((((ntr|>children)[1]|>children)[1]|>children)[1]|>children)[1]|>children)[1])
