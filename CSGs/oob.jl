using StaticArrays
using AbstractTrees
using LinearAlgebra
using Random
using FileIO
using Makie
using GeometryTypes: HyperRectangle

using Revise
using CSGBuilding
using RANSACVisualizer
const CSGB = CSGBuilding
cd(@__DIR__)

poins2 = [[0,0], [0,1], [1,0], [1,1]]
c = CSGB.centroid(poins2)

poins3 = [[0,0,0], [0,0,1], [0,1,0], [0,1,1]]
c = CSGB.centroid(poins3)

poins33 = [SVector(i, j, k) for i in 0:1 for j in 0:1 for k in 0:1]
c = centroid(poins33)
CSGB.normedcovmat(poins33)

pints = [rand(3) for i in 1:25]
ci = centroid(pints)
nm = CSGB.normedcovmat(pints)

evs = [eigvecs(nm)[:,i] for i in 1:3]


nm2 = CSGB.covmat(pints)
evs2 = [eigvecs(nm2)[:,i] for i in 1:3]

t(v) = dot(cross(v[1],v[2]), v[3])

t1 = load("t1.obj");
t1v = t1.vertices;
CSGB.findOBB(t1v)

t2 = load("t2.obj");
t2v = t2.vertices;
obb2, ocf2 = CSGB.findOBB(t2v)
#obb2 = CSGB.findOBB(t2v)
sc2 = scatter(t2v)
scatter!(sc2, obb2, color=:orange, markersize=0.4)
scatter!(sc2, [obb2[1]], color=:green)


_1(x)=x[1]
_2(x)=x[2]
_3(x)=x[3]
_1a(x)=[x[1]]
_2a(x)=[x[2]]
_3a(x)=[x[3]]
#=
np = obb2[1];
xa = ocf2[1];
ya = ocf2[2];
za = ocf2[3];
sc2 = scatter(t2v);
scatter!(sc2, [np], color=:purple, markersize=0.4);
arrows!(_1a(np), _2a(np), _3a(np), _1a(xa), _2a(xa), _3a(xa), arrowcolor=:red)
arrows!(_1a(np), _2a(np), _3a(np), _1a(ya), _2a(ya), _3a(ya), arrowcolor=:green)
arrows!(_1a(np), _2a(np), _3a(np), _1a(za), _2a(za), _3a(za), arrowcolor=:blue)
=#

sidel = [1,2,3];
nb = -sidel/2;
bi = 0:1
cs_ = [SVector{3}(nb+sidel .*[i,j,k]) for i in bi for j in bi for k in bi];
scatter(cs_)


t2 = load("t2.obj");
t2v = t2.vertices;
#obb2, ocf2 = CSGB.findOBB(t2v)
obb2, _ = CSGB.findOBB(t2v)
sc2 = Scene()
scatter!(sc2, t2v)
scatter!(sc2, obb2, color=:green, markersize=0.3)
for i in eachindex(obb2)
    text!(sc2,"$i", position=obb2[i], textsize=0.5)
end


obb2, obplanes = CSGB.findOBB(t2v)
sc2 = Scene()
scatter!(sc2, t2v)
scatter!(sc2, obb2, color=:green, markersize=0.3)
for i in [3,6]
    plotimplshape!(sc2, obplanes[i], scale=(5.,5.))
    p = obplanes[i].point
    n = obplanes[i].normal
    arrows!(sc2, _1a(p), _2a(p), _3a(p), _1a(n), _2a(n), _3a(n))
end
