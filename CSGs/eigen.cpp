// source:
// https://codextechnicanum.blogspot.com/2015/04/find-minimum-oriented-bounding-box-of.html

// Compute principal directions
Eigen::Vector4f pcaCentroid;
pcl::compute3DCentroid(*cloudSegmented, pcaCentroid);
Eigen::Matrix3f covariance;
computeCovarianceMatrixNormalized(*cloudSegmented, pcaCentroid, covariance);
Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
Eigen::Matrix3f eigenVectorsPCA = eigen_solver.eigenvectors();
eigenVectorsPCA.col(2) = eigenVectorsPCA.col(0).cross(eigenVectorsPCA.col(1));  /// This line is necessary for proper orientation in some cases. The numbers come out the same without it, but
                                                                                ///    the signs are different and the box doesn't get correctly oriented in some cases.
/* // Note that getting the eigenvectors can also be obtained via the PCL PCA interface with something like:
pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPCAprojection (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PCA<pcl::PointXYZ> pca;
pca.setInputCloud(cloudSegmented);
pca.project(*cloudSegmented, *cloudPCAprojection);
std::cerr << std::endl << "EigenVectors: " << pca.getEigenVectors() << std::endl;
std::cerr << std::endl << "EigenValues: " << pca.getEigenValues() << std::endl;
// In this case, pca.getEigenVectors() gives similar eigenVectors to eigenVectorsPCA.
*/

// Transform the original cloud to the origin where the principal components correspond to the axes.
Eigen::Matrix4f projectionTransform(Eigen::Matrix4f::Identity());
projectionTransform.block<3,3>(0,0) = eigenVectorsPCA.transpose();
projectionTransform.block<3,1>(0,3) = -1.f * (projectionTransform.block<3,3>(0,0) * pcaCentroid.head<3>());
pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPointsProjected (new pcl::PointCloud<pcl::PointXYZ>);
pcl::transformPointCloud(*cloudSegmented, *cloudPointsProjected, projectionTransform);
// Get the minimum and maximum points of the transformed cloud.
pcl::PointXYZ minPoint, maxPoint;
pcl::getMinMax3D(*cloudPointsProjected, minPoint, maxPoint);
const Eigen::Vector3f meanDiagonal = 0.5f*(maxPoint.getVector3fMap() + minPoint.getVector3fMap());

// Final transform
const Eigen::Quaternionf bboxQuaternion(eigenVectorsPCA); //Quaternions are a way to do rotations https://www.youtube.com/watch?v=mHVwd8gYLnI
const Eigen::Vector3f bboxTransform = eigenVectorsPCA * meanDiagonal + pcaCentroid.head<3>();

// This viewer has 4 windows, but is only showing images in one of them as written here.
pcl::visualization::PCLVisualizer *visu;
visu = new pcl::visualization::PCLVisualizer (argc, argv, "PlyViewer");
int mesh_vp_1, mesh_vp_2, mesh_vp_3, mesh_vp_4;
visu->createViewPort (0.0, 0.5, 0.5, 1.0,  mesh_vp_1);
visu->createViewPort (0.5, 0.5, 1.0, 1.0,  mesh_vp_2);
visu->createViewPort (0.0, 0, 0.5, 0.5,  mesh_vp_3);
visu->createViewPort (0.5, 0, 1.0, 0.5, mesh_vp_4);
visu->addPointCloud(cloudSegmented, ColorHandlerXYZ(cloudSegmented, 30, 144, 255), "bboxedCloud", mesh_vp_3);
visu->addCube(bboxTransform, bboxQuaternion, maxPoint.x - minPoint.x, maxPoint.y - minPoint.y, maxPoint.z - minPoint.z, "bbox", mesh_vp_3);

// source:
// http://docs.pointclouds.org/1.7.0/centroid_8hpp_source.html#l00252

//////////////////////////////////////////////////////////////////////////////////////////////
  251 template <typename PointT, typename Scalar> inline unsigned int
  252 pcl::computeCovarianceMatrixNormalized (const pcl::PointCloud<PointT> &cloud,
  253                                         const Eigen::Matrix<Scalar, 4, 1> &centroid,
  254                                         Eigen::Matrix<Scalar, 3, 3> &covariance_matrix)
  255 {
  256   unsigned point_count = pcl::computeCovarianceMatrix (cloud, centroid, covariance_matrix);
  257   if (point_count != 0)
  258     covariance_matrix /= static_cast<Scalar> (point_count);
  259   return (point_count);
  260 }

//////////////////////////////////////////////////////////////////////////////////////////////
 263 template <typename PointT, typename Scalar> inline unsigned int
 264 pcl::computeCovarianceMatrix (const pcl::PointCloud<PointT> &cloud,
 265                               const std::vector<int> &indices,
 266                               const Eigen::Matrix<Scalar, 4, 1> &centroid,
 267                               Eigen::Matrix<Scalar, 3, 3> &covariance_matrix)
 268 {
 269   if (indices.empty ())
 270     return (0);
 271
 272   // Initialize to 0
 273   covariance_matrix.setZero ();
 274
 275   size_t point_count;
 276   // If the data is dense, we don't need to check for NaN
 277   if (cloud.is_dense)
 278   {
 279     point_count = indices.size ();
 280     // For each point in the cloud
 281     for (size_t i = 0; i < point_count; ++i)
 282     {
 283       Eigen::Matrix<Scalar, 4, 1> pt;
 284       pt[0] = cloud[indices[i]].x - centroid[0];
 285       pt[1] = cloud[indices[i]].y - centroid[1];
 286       pt[2] = cloud[indices[i]].z - centroid[2];
 287
 288       covariance_matrix (1, 1) += pt.y () * pt.y ();
 289       covariance_matrix (1, 2) += pt.y () * pt.z ();
 290
 291       covariance_matrix (2, 2) += pt.z () * pt.z ();
 292
 293       pt *= pt.x ();
 294       covariance_matrix (0, 0) += pt.x ();
 295       covariance_matrix (0, 1) += pt.y ();
 296       covariance_matrix (0, 2) += pt.z ();
 297     }
 298   }
 299   // NaN or Inf values could exist => check for them
 300   else
 301   {
 302     point_count = 0;
 303     // For each point in the cloud
 304     for (size_t i = 0; i < indices.size (); ++i)
 305     {
 306       // Check if the point is invalid
 307       if (!isFinite (cloud[indices[i]]))
 308         continue;
 309
 310       Eigen::Matrix<Scalar, 4, 1> pt;
 311       pt[0] = cloud[indices[i]].x - centroid[0];
 312       pt[1] = cloud[indices[i]].y - centroid[1];
 313       pt[2] = cloud[indices[i]].z - centroid[2];
 314
 315       covariance_matrix (1, 1) += pt.y () * pt.y ();
 316       covariance_matrix (1, 2) += pt.y () * pt.z ();
 317
 318       covariance_matrix (2, 2) += pt.z () * pt.z ();
 319
 320       pt *= pt.x ();
 321       covariance_matrix (0, 0) += pt.x ();
 322       covariance_matrix (0, 1) += pt.y ();
 323       covariance_matrix (0, 2) += pt.z ();
 324       ++point_count;
 325     }
 326   }
 327   covariance_matrix (1, 0) = covariance_matrix (0, 1);
 328   covariance_matrix (2, 0) = covariance_matrix (0, 2);
 329   covariance_matrix (2, 1) = covariance_matrix (1, 2);
 330   return (static_cast<unsigned int> (point_count));
 331 }
 332
