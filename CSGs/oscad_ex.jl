# Convert .scad files to other file formats with OpenSCAD
using Logging

function scad2file(filedir, extractdir, extension=".stl")
    i = 1
    for (root, dirs, files) in walkdir(filedir)
        for file in files
            f = joinpath(root, file) # path to files
            if occursin(".scad", f)
                # if it's a .scad file
                @info "Current file: $f"
                dirn = splitdir(dirname(f))[end]
                newf = joinpath(extractdir, replace(dirn*basename(f), ".scad" => extension))
                if isfile(newf)
                    newf = replace(newf, dirn*extension => "_$i_"*extension)
                    i += 1
                end
                run(`openscad -o $newf $f`)
            end
        end
    end
end
