using LinearAlgebra
using Logging
using Random
using StaticArrays
using D3Trees
using BSON
using FileIO
#using Makie


#using Revise
using CSGBuilding
using RANSAC
#using RANSACVisualizer
#const CSGB = CSGBuilding
const deflog = global_logger()

#=
Tasks:
1. Put the models folder into this folder ("onemodelonefile/")
2. Create a copy of this file and rename to the name of the model
# locally - line-by-line
3. Run RANSAC locally to test the parameters
4. Convert into CSG representation to test the parameters
# in cloud
5. If works, set the proper CSG parameters
6. Comment out the lines that are not needed for cloud
7. Commit and run in the cloud.
=#

cd(@__DIR__)

f = @__FILE__
sbf = splitext(basename(f))
fbase = sbf[1]
fn = fbase*".obj"
if ! isfile(fn)
    @error "$fn is not a file."
    @show f
    @show sbf
    @show fbase
    @show fn
end

fmesh = load(fn);
# showgeometry(fmesh)

global_logger(nosource_IterInflog())
pcr = PointCloud(fmesh.vertices, fmesh.normals, 2);

p = RANSACParameters{Float64}(ϵ_plane=0.2,
                                α_plane=deg2rad(5),
                                ϵ_sphere=0.1,
                                α_sphere=deg2rad(1),
                                ϵ_cylinder=0.3,
                                α_cylinder=deg2rad(2.5),
                                ϵ_cone=0.15,
                                α_cone=deg2rad(1),
                                ϵ_transl=0.3,
                                α_transl=deg2rad(5),
                                minsubsetN=15,
                                itermax=1000,
                                τ=200,
                                prob_det=0.9);

cand, extr = ransac(pcr, p, true, reset_rand=true);

#=
# Comment these out after line-by-line testing!
showtype(extr)
showtype(cand)
showbytype(pcr, extr, markersize=0.3)
showshapes(pcr, extr, markersize=0.5)
scatter(pcr.vertices[getrest(pcr)])
=#

## CSGB
convp = (α_conv=cosd(5), ϵ_conv=7);
impls, remt = ransacresult2implicit(pcr, extr, convp);

#=
# Comment these out after line-by-line testing!
sc = Scene()
scatter!(sc, pcr.vertices)
for o in impls
    plotimplshape!(sb, o, scale=(10.,10.))
end
=#
pcsg = CSGGeneticBuildParameters{Float64}(maxdepth=10,
                                        itermax=10)

popu, best = cachedfuncgeneticbuildtree(impls, pcr.vertices, pcr.normals, pcsg)
minV, maxV = findAABB(pcr.vertices)
minc = minimum(minV)
maxc = maximum(maxV)
edgel = (mincorner=minc-1, maxcorner=maxc+1, edgelength=110);
@info "Writing paraview format."
writeparaviewformat(best, fbase*"_reconstr", edgel)

@info "Writing BSON."
bson(fbase*".bson", Dict(:bestnode=>best))
@info "Writing html."
tofile(D3Tree(best, init_expand=15), fbase*".html")
