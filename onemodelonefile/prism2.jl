using LinearAlgebra
using Logging
using Random
using StaticArrays
using AbstractTrees
using D3Trees
using Makie

using Revise
using CSGBuilding
using RANSAC
using RANSACVisualizer
const CSGB = CSGBuilding
const deflog = global_logger()

cd(@__DIR__)

mpr = load("prism2.obj");

showgeometry(mpr)

global_logger(nosource_IterInflog())
pcr = PointCloud(mpr.vertices, mpr.normals, 2);

p = RANSACParameters{Float64}(ϵ_plane=0.4,
                                α_plane=deg2rad(5),
                                ϵ_sphere=0.1,
                                α_sphere=deg2rad(1),
                                ϵ_cylinder=0.3,
                                α_cylinder=deg2rad(2.5),
                                ϵ_cone=0.15,
                                α_cone=deg2rad(1),
                                ϵ_transl=0.3,
                                α_transl=deg2rad(5),
                                minsubsetN=15,
                                τ=50,
                                itermax=10_000,
                                prob_det=0.9);

cand, extr = ransac(pcr, p, true, reset_rand=true);
showtype(extr)
showtype(cand)
showbytype(pcr, extr, markersize=0.5)
showshapes(pcr, extr, markersize=0.5)
scatter(pcr.vertices[getrest(pcr)])

## CSGB
convp = (α_conv=cosd(5), ϵ_conv=7);
impls, remt = ransacresult2implicit(pcr, extr, convp);
sc = Scene()
scatter!(sc, pcr.vertices, markersize=0.9)
for o in impls
    plotimplshape!(sc, o, scale=(20.,20.))
end
