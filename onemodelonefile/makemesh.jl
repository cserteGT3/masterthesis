# This file is only for testing the meshes and the parameters
using LinearAlgebra
using Logging
using Random
using StaticArrays
using D3Trees
using BSON
using FileIO
using Makie
using UnionFind
using NearestNeighbors

using Revise
using CSGBuilding
using RANSAC
using RANSACVisualizer
#const CSGB = CSGBuilding
const deflog = global_logger()
global_logger(nosource_IterInflog())
global_logger(nosource_IterLow1log())

cd(@__DIR__)

fn = "icsg_lockcsg2.obj"
fn = "furcsa32k.obj"
fn = "157_2.obj"
fn = "Part4red.obj"
fn = "icsg_144csg.obj"
fn = "icsg_csg144_new.obj"
fn = "icsg_157csg.obj"
fn = "icsg_158csg.obj"
fn = "szt_bilincs.obj"
fn = "szt_Group3_2-Finger_85_Adaptive_Gripper.obj"
fmesh = load(fn);
sc = showgeometry(fmesh)
cam3d!(sc)
println("mesh size: ", size(fmesh.vertices,1))

pcr = PointCloud(fmesh.vertices, fmesh.normals, 4);

p = RANSACParameters{Float64}(ϵ_plane=0.2,
                                α_plane=deg2rad(4),
                                ϵ_sphere=0.3,
                                α_sphere=deg2rad(1),
                                ϵ_cylinder=0.15,
                                α_cylinder=deg2rad(3),
                                ϵ_cone=0.2,
                                α_cone=deg2rad(1),
                                ϵ_transl=0.3,
                                α_transl=deg2rad(3),
                                α_perpend = cosd(89.5),
                                minsubsetN=15,
                                itermax=60_000,
                                τ=500,
                                shape_types=[:sphere, :plane, :cylinder, :cone],
                                thinning_par=0.3/4,
                                force_transl=false,
                                thin_method=:deldir,
                                min_normal_num=0.8,
                                prob_det=0.9);

cand, extr, rtime = ransac(pcr, p, true, reset_rand=true);

showtype(extr)
println("Left: $(count(pcr.isenabled)) of $(pcr.size)")

showbytype(pcr, extr, markersize=0.1)
showshapes(pcr, extr, markersize=0.1)
showshapes(pcr, extr[[5,7]], markersize=0.1)

scatter(pcr.vertices[pcr.isenabled])

scatter(pcr.vertices[extr[1].inpoints])


b = findhighestscore(cand)
bc = cand[b.index];
scatter(pcr.vertices[cand[b.index].inpoints])
scatter(pcr.vertices[cand[1].inpoints])

proj_p = RANSAC.project2sketchplane(pcr.vertices[cand[b.index].inpoints], cand[b.index].candidate.shape.coordframe)
scatter(proj_p, scale_plot=false)

## contours
bs = extr[1].candidate.shape
scatter(bs.contour, scale_plot=false, markersize=0.1)
lines(bs.contour, scale_plot=false, markersize=0.1)

## rerun
extrbc = deepcopy(extr);
pcrbc = deepcopy(pcr);
p2 = RANSACParameters{Float64}(ϵ_plane=0.3,
                                α_plane=deg2rad(2),
                                ϵ_sphere=0.3,
                                α_sphere=deg2rad(1),
                                ϵ_cylinder=0.25,
                                α_cylinder=deg2rad(5),
                                ϵ_cone=0.2,
                                α_cone=deg2rad(1),
                                ϵ_transl=0.2,
                                α_transl=deg2rad(3),
                                α_perpend = cosd(89.5),
                                minsubsetN=15,
                                itermax=10_000,
                                τ=100,
                                shape_types=[:sphere, :plane, :cylinder, :cone],
                                thinning_par=0.2,
                                force_transl=false,
                                thin_method=:slow,
                                min_normal_num=0.8,
                                prob_det=0.9);

final_extr, rtime2, extr2 = rerunleftover!(pcrbc, 4, p2, extrbc, reset_rand=true)
println("Left: $(count(pcrbc.isenabled)) of $(pcrbc.size)")

showbytype(pcrbc, extr2, markersize=0.1)
showbytype(pcrbc, final_extr, markersize=0.1)
showshapes(pcrbc, extr2, markersize=0.1)
showshapes(pcrbc, final_extr, markersize=0.1)
scatter(pcrbc.vertices[pcrbc.isenabled])

showbytype(pcrbc, [extr2[6]], markersize=0.1)


## filter planes
println("dummy line")
proj_p = RANSAC.project2sketchplane(pcr.vertices[cand[b.index].inpoints], cand[b.index].candidate.shape.coordframe)
sc = scatter(proj_p, scale_plot=false)
drawcircles!(sc, proj_p, 4, scale_plot=false, strokecolor=:blue)
miv, mav = findAABB(proj_p)
c = centroid(proj_p)
scatter!([c], color=:orange)
s = mav-miv
s .* 0.01
s[1] < s[2]*0.01

## UnionFind
function t(pois, thr)
    btree = BallTree(pois)
    uf = UnionFinder(size(pois,1))

    for i in eachindex(pois)
        inr = inrange(btree, pois[i], thr)
        for j in eachindex(inr)
            # the point itself is always in range
            if i == inr[j]
                continue
            end
            union!(uf, i, inr[j])
        end
    end
    return uf
end

function drawcircles!(sc, points, r; kwargs...)
    ps = [Makie.Point2f0(p) for p in points]
    poly!(sc, [Circle(p, Float32(r/2)) for p in ps], color = (:blue, 0.0), transparency = true, strokecolor=:blue, strokewidth=1; kwargs...)
    sc
end

sc = Scene()
scatter!(sc, proj_p, scale_plot=false)
drawcircles!(sc, proj_p, 0.5; scale_plot=false)

ucf = RANSAC.segmentpatches(proj_p, 0.25);
groups(ucf)

function tik(cf, p)
    sc = Scene()
    color = [:red, :green, :blue, :orange, :black]
    for i in 1:5
        ps = p[findall(x->x==i, ucf.ids)]
        scatter!(sc, ps, color=color[i], scale_plot=false)
        #scatter!(sc, ps)
    end
    sc
end

sc = tik(ucf, proj_p)

tpk = [[1,0], [2,0], [3,0], [4.5,0], [5,0], [6,0]]
tps = [SVector{2,Float64}(p) for p in tpk]
leps = 1
ucs = t(tps, 1.0)
cfs = CompressedFinder(ucs)
groups(cfs)

sc = Scene()
scatter!(sc, tps, scale_plot=false)
drawcircles!(sc, tps, 2*leps; scale_plot=false)


scatter(pcr.vertices[cand[end].inpoints])
scatter(pcr.vertices[extr[1].inpoints])

showtype(extr)
showtype(cand)
showbytype(pcr, extr, markersize=0.3)
showshapes(pcr, extr, markersize=0.3)
scatter(pcr.vertices[getrest(pcr)])

convp = (α_conv=cosd(5), ϵ_conv=7);
impls, _ = ransacresult2implicit(pcr, extr, convp);


using BSON
using JSON

f = "icsg_158csgstateoftheart-v1_genetic.bson"

db = BSON.load(f)

nt = toNiceTree(db[:bestcached])
tree2tex(nt, "tes2.tex", true)


toJSON("t.json", nt)
json2tex("t.json", "t.tex")

## dual contour

btr = db[:besttree];
bb = ([-5.,-5,-5], [5,5,5.]);
res = (110,110,110);

isosurf = isosurface(btr, 0., bb, res)
writeOBJ(isosurf[1], isosurf[2], "first.obj")




vr = fmesh.vertices
nr = fmesh.normals
fr = fmesh.faces
