using LinearAlgebra
using Logging
using Random
using StaticArrays
using D3Trees
using BSON
using FileIO
#using Makie


#using Revise
using CSGBuilding
using RANSAC
#using RANSACVisualizer
#const CSGB = CSGBuilding
const deflog = global_logger()

#=
Tasks:
1. Put the models folder into this folder ("onemodelonefile/")
2. Create a copy of this file and rename to the name of the model
# locally - line-by-line
3. Run RANSAC locally to test the parameters
4. Convert into CSG representation to test the parameters
# in cloud
5. If works, set the proper CSG parameters
6. Comment out the lines that are not needed for cloud
7. Commit and run in the cloud.
=#

cd(@__DIR__)

#####################
#TODO
# additional name to the written files
ADD_NAME = "wo_v4_eps001"
# on which PC does it run? and any other infos
RUN_INFO = Dict(:PC=>"EMIcompute", :threads=>"1", :addname=>ADD_NAME)
#####################

f = @__FILE__
sbf = splitext(basename(f))
fbase = replace(sbf[1], "-"=>"")
fadd = fbase*ADD_NAME
fn = fbase*".obj"
if ! isfile(fn)
    @error "$fn is not a file."
    @show f
    @show sbf
    @show fbase
    @show fn
end

fmesh = load(fn);
println(fadd, " mesh size: ", size(fmesh.vertices,1))

#####################
#TODO
# showgeometry(fmesh)
#####################

function appendkv(d, keyS, valAny)
    ks = collect(keys(d))
    push!(ks, keyS)
    vs = collect(values(d))
    push!(vs, string(valAny))
    return Dict([(ks[i],vs[i]) for i in eachindex(ks)])
end

RUN_INFO = appendkv(RUN_INFO, :meshsize, size(fmesh.vertices,1))

global_logger(nosource_IterInflog())
pcr = PointCloud(fmesh.vertices, fmesh.normals, 4);

p = RANSACParameters{Float64}(ϵ_plane=0.3,
                                α_plane=deg2rad(2),
                                ϵ_sphere=0.3,
                                α_sphere=deg2rad(1),
                                ϵ_cylinder=0.5,
                                α_cylinder=deg2rad(5),
                                ϵ_cone=0.2,
                                α_cone=deg2rad(1),
                                minconeopang=deg2rad(15),
                                max_contour_it=25,
                                ϵ_transl=0.15,
                                α_transl=deg2rad(3),
                                α_perpend = cosd(89.5),
                                checksidepar=0.2,
                                minsubsetN=15,
                                itermax=60_000,
                                τ=400,
                                shape_types=[:sphere, :plane, :cylinder, :cone],
                                #shape_types=[:sphere, :plane, :cylinder, :cone, :translational_surface],
                                #shape_types=[:translational_surface],
                                thinning_par=0.15/2,
                                #max_end_d=0.5,
                                jumpback=true,
                                min_normal_num=0.9,
                                prob_det=0.9);

cand, extr, rtime = ransac(pcr, p, true, reset_rand=true);
p2 = nothing

#####################
# if rerun

#TODO
p2 = RANSACParameters{Float64}(ϵ_plane=0.3,
                                α_plane=deg2rad(2),
                                ϵ_sphere=0.3,
                                α_sphere=deg2rad(1),
                                ϵ_cylinder=0.5,
                                α_cylinder=deg2rad(5),
                                ϵ_cone=0.2,
                                α_cone=deg2rad(1),
                                minconeopang=deg2rad(15),
                                max_contour_it=25,
                                ϵ_transl=0.15,
                                α_transl=deg2rad(3),
                                α_perpend = cosd(89.5),
                                checksidepar=0.2,
                                minsubsetN=15,
                                itermax=120_000,
                                τ=40,
                                #shape_types=[:sphere, :plane, :cylinder, :cone],
                                shape_types=[:plane],
                                #shape_types=[:sphere, :plane, :cylinder, :cone, :translational_surface],
                                #shape_types=[:translational_surface],
                                thinning_par=0.15/2,
                                #max_end_d=0.5,
                                jumpback=true,
                                min_normal_num=0.9,
                                prob_det=0.9);

final_extr, rtime2, extr2 = rerunleftover!(pcr, 4, p2, extr, reset_rand=true)
RUN_INFO = appendkv(RUN_INFO, :ransactimes2, rtime2)
RUN_INFO = appendkv(RUN_INFO, :totalransacts, rtime+rtime2)

# rerun end
#####################


RUN_INFO = appendkv(RUN_INFO, :ransactimes, rtime)
bson(fadd*"_ransac.bson", Dict(:extracted=>extr, :pointcloud=>pcr, :params=>[p, p2], :runinfo=>RUN_INFO))

#####################
#=
# Comment these out after line-by-line testing!
#TODO
showtype(extr)
showtype(cand)
showbytype(pcr, extr, markersize=0.3)
showshapes(pcr, extr, markersize=0.5)
scatter(pcr.vertices[getrest(pcr)])
=#
#####################

## CSGB
convp = (α_conv=cosd(2), ϵ_conv=p.ϵ_plane);
impls, _ = ransacresult2implicit(pcr, extr, convp);

#####################
#=
# Comment these out after line-by-line testing!
#TODO
sc = Scene()
scatter!(sc, pcr.vertices)
for o in impls
    plotimplshape!(sb, o, scale=(10.,10.))
end
=#
#####################

#####################
# free up some memory
cand = nothing
vs = pcr.vertices
ns = pcr.normals
pcr = nothing
GC.gc()
#####################

pcsg = CSGGeneticBuildParameters{Float64}(maxdepth=10,
                                        ϵ_d=0.001,
                                        itermax=4000)

popu, best, gtime, scora = cachedfuncgeneticbuildtree(impls, vs, ns, pcsg)
RUN_INFO = appendkv(RUN_INFO, :genetictimes, gtime)
RUN_INFO = appendkv(RUN_INFO, :scores, scora)

minV, maxV = findAABB(vs)
minc = minimum(minV)
maxc = maximum(maxV)
edgel = (mincorner=minc-5, maxcorner=maxc+5, edgelength=110);
RUN_INFO = appendkv(RUN_INFO, :edgel, edgel)
@info "Writing paraview format."
writeparaviewformat(best, fadd*"_reconstr", edgel)

@info "Writing BSON."
bson(fadd*"_genetic.bson", Dict(:besttree=>best, :params=>pcsg, :bestcached=>popu[1], :surfaces=>impls, :runinfo=>RUN_INFO))
@info "Writing html."
tofile(D3Tree(popu[1], init_expand=15), fadd*".html")
