using LinearAlgebra
using Logging
using Random
using StaticArrays
using D3Trees
using BSON
using FileIO
#using Makie


#using Revise
using CSGBuilding
using RANSAC
#using RANSACVisualizer
#const CSGB = CSGBuilding
const deflog = global_logger()

#=
Tasks:
1. Put the models folder into this folder ("onemodelonefile/")
2. Create a copy of this file and rename to the name of the model
# locally - line-by-line
3. Run RANSAC locally to test the parameters
4. Convert into CSG representation to test the parameters
# in cloud
5. If works, set the proper CSG parameters
6. Comment out the lines that are not needed for cloud
7. Commit and run in the cloud.
=#

cd(@__DIR__)

#####################
#TODO
# additional name to the written files
ADD_NAME = "translational"
# on which PC does it run? and any other infos
RUN_INFO = Dict(:PC=>"EMIcompute", :threads=>"1", :addname=>ADD_NAME)
#####################

f = @__FILE__
sbf = splitext(basename(f))
fbase = replace(sbf[1], "-"=>"")
fadd = fbase*ADD_NAME
fn = fbase*".obj"
if ! isfile(fn)
    @error "$fn is not a file."
    @show f
    @show sbf
    @show fbase
    @show fn
end

fmesh = load(fn);
println(fadd, " mesh size: ", size(fmesh.vertices,1))

#####################
#TODO
# showgeometry(fmesh)
#####################

function appendkv(d, keyS, valAny)
    ks = collect(keys(d))
    push!(ks, keyS)
    vs = collect(values(d))
    push!(vs, string(valAny))
    return Dict([(ks[i],vs[i]) for i in eachindex(ks)])
end

RUN_INFO = appendkv(RUN_INFO, :meshsize, size(fmesh.vertices,1))

global_logger(nosource_IterInflog())
pcr = PointCloud(fmesh.vertices, fmesh.normals, 4);

p = RANSACParameters{Float64}(ϵ_plane=0.5,
                                α_plane=deg2rad(5),
                                ϵ_sphere=0.2,
                                α_sphere=deg2rad(1),
                                ϵ_cylinder=0.8,
                                α_cylinder=deg2rad(5),
                                ϵ_cone=0.2,
                                α_cone=deg2rad(1),
                                ϵ_transl=3.5,
                                α_transl=deg2rad(5),
                                α_perpend = cosd(89.5),
                                minsubsetN=15,
                                itermax=50_000,
                                τ=500,
                                shape_types=[:sphere, :plane, :cylinder, :cone, :translational_surface],
                                #shape_types=[:translational_surface],
                                thinning_par=3.5/3,
                                force_transl=false,
                                thin_method=:slow,
                                min_normal_num=0.8,
                                prob_det=0.9);

cand, extr, rtime = ransac(pcr, p, true, reset_rand=true);
RUN_INFO = appendkv(RUN_INFO, :ransactimes, rtime)
bson(fadd*"_ransac.bson", Dict(:extracted=>extr, :pointcloud=>pcr, :params=>p, :runinfo=>RUN_INFO))

#####################
#=
# Comment these out after line-by-line testing!
#TODO
showtype(extr)
showtype(cand)
showbytype(pcr, extr, markersize=0.3)
showshapes(pcr, extr, markersize=0.5)
scatter(pcr.vertices[getrest(pcr)])
=#
#####################

## CSGB
convp = (α_conv=cosd(5), ϵ_conv=1);
impls, _ = ransacresult2implicit(pcr, extr, convp);

#####################
#=
# Comment these out after line-by-line testing!
#TODO
sc = Scene()
scatter!(sc, pcr.vertices)
for o in impls
    plotimplshape!(sb, o, scale=(10.,10.))
end
=#
#####################

#####################
# free up some memory
cand = nothing
vs = pcr.vertices
ns = pcr.normals
pcr = nothing
GC.gc()
#####################

pcsg = CSGGeneticBuildParameters{Float64}(maxdepth=10,
                                        itermax=5000)

popu, best, gtime = cachedfuncgeneticbuildtree(impls, vs, ns, pcsg)
RUN_INFO = appendkv(RUN_INFO, :genetictimes, gtime)

minV, maxV = findAABB(vs)
minc = minimum(minV)
maxc = maximum(maxV)
edgel = (mincorner=minc-5, maxcorner=maxc+5, edgelength=110);
RUN_INFO = appendkv(RUN_INFO, :edgel, edgel)
@info "Writing paraview format."
writeparaviewformat(best, fadd*"_reconstr", edgel)

@info "Writing BSON."
bson(fadd*"_genetic.bson", Dict(:besttree=>best, :params=>pcsg, :bestcached=>popu[1], :surfaces=>impls, :runinfo=>RUN_INFO))
@info "Writing html."
tofile(D3Tree(popu[1], init_expand=15), fadd*".html")
