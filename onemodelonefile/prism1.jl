using LinearAlgebra
using Logging
using Random
using StaticArrays
using AbstractTrees
using D3Trees
using FileIO
using Makie

using Revise
using CSGBuilding
using RANSAC
using RANSACVisualizer
const CSGB = CSGBuilding
const deflog = global_logger()

cd(@__DIR__)

mpr1 = load("prism1.obj");

showgeometry(mpr1)

global_logger(nosource_IterInflog())
pcr = PointCloud(mpr1.vertices, mpr1.normals, 2);

p = RANSACParameters{Float64}(ϵ_plane=0.4,
                                α_plane=deg2rad(5),
                                ϵ_sphere=0.1,
                                α_sphere=deg2rad(1),
                                ϵ_cylinder=0.3,
                                α_cylinder=deg2rad(2.5),
                                ϵ_cone=0.15,
                                α_cone=deg2rad(1),
                                ϵ_transl=0.3,
                                α_transl=deg2rad(5),
                                minsubsetN=15,
                                itermax=10_000,
                                τ=100,
                                prob_det=0.9);

cand, extr = ransac(pcr, p, true, reset_rand=true);
showtype(extr)
showtype(cand)
showbytype(pcr, extr, markersize=0.3)
showbytype(pcr, cand, markersize=0.3)
showshapes(pcr, extr, markersize=0.3)
showshapes!(sc, pcr, [cand[6]]; markersize=1)
scatter(pcr.vertices[getrest(pcr)])


## CSGB
impls = ransacresult2implicit(pcr, extr);
sc = Scene()
scatter!(sc, pcr.vertices)
for o in impls
    plotimplshape!(sc, o, scale=(100.,100.))
end
