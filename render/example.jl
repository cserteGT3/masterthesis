using LinearAlgebra
using ImageView
#using Images
using ColorVectorSpace
using Colors
using SignedDistanceFields

using Revise
using RANSACVisualizer
using CSGBuilding
const CSGB = CSGBuilding

z = to_code(
            SUnion(
            (
            #Trans(Sphere(5*sqrt(2),4),space(0.0,-10.0,0.0)),
            #Trans(Ball(5*sqrt(2),5,1),space(0.0,5.0,8.66)),
            #Trans(Ball(5*sqrt(2),6,0.5),space(0.0,5.0,-8.66)),
            Plane(space(1.0,0.0,0.0),2),
            #1 RepQ(Trans(Ball(0.5,1,3.5),space(2.5,2.5,2.5)),5.0),
            Trans(Ball(5,3,2.5),space(0.0,0.0,0.0))
            )))

scene = func(z,:space)
p = render(scene, (x=720, y=1280), DefIsoCam, DefaultShaderArray)

scene2 = func(to_code(Trans(Ball(5,3,2.5),space(0.0,0.0,0.0))), :space)
p2 = render(scene2, (x=720, y=1280), DefIsoCam, DefaultShaderArray)

scene3 = func(to_code(Trans(Sphere(5*sqrt(2),4),space(0.0,0.0,0.0))), :space)

scene4 = func(to_code(
            SUnion((Trans(Sphere(5*sqrt(2),4),space(0.0,0.0,0.0)),Plane(space(1.0,0.0,0.0),2)))), :space)

p = render(scene2, (x=1080, y=1920), :pincam)

fname = joinpath(@__DIR__, "rendered", "first.png")
save(fname, p)

## testing the shaders
shad_z = to_code(
            SUnion(
            (
            Trans(Sphere(1,1),space(0.0,0.0,0.0)),
            Trans(Sphere(1,2),space(0.0,0.0,2.0)),
            Trans(Sphere(1,3),space(0.0,0.0,4.0)),
            Trans(Sphere(1,4),space(0.0,0.0,6.0)),
            Trans(Sphere(1,5),space(0.0,0.0,8.0)),
            Trans(Sphere(1,6),space(0.0,0.0,10.0)))
            )
            )

sceneshad = func(shad_z,:space)
p = render(sceneshad, (x=480, y=640), DefIsoCam, DefaultShaderArray)

## test for CSGNode
sp = ImplicitSphere([0.0,0,0], 4.5)
pl = ImplicitPlane([0.0,0,0], [0,1,0])
cyl = ImplicitCylinder([0,0,1], [0,-2.5,0], 1.5)

spn = CSGNode(sp, [])
pln = CSGNode(pl, [])
cyln = CSGNode(cyl, [])
tr1 = CSGNode(CSGB.intersection, [spn, pln])
tr = CSGNode(CSGB.subtraction, [tr1, cyln])

p = render(tr, (x=1080, y=1080), c2, DefaultShaderArray)
## compare CSGNode and Scene
tscene1 = func(to_code(Sphere(1,2)), :space)
tscene2 = func(to_code(Trans(Sphere(1,2),space(0.0,0.0,0.0))), :space)
pt1 = render(tscene1, (x=480, y=640), DefIsoCam, DefaultShaderArray)
pt2 = render(tscene2, (x=480, y=640), DefIsoCam, DefaultShaderArray)

## camera playing

arg1 = space(-25.0,-25.0,-25.0);
arg2 = normalize(space(1.0,1.0,1.0));
arg3p = space(0.0,1.0,0.0);
arg4p = space(0.0,0.0,1.0);
fl = 90.;
c1(x, y) = PinholeCamera(arg1, arg2, x/600*35*normalize(arg3p), y/600*35*normalize(arg4p),x,y,fl);
c1(x, y) = PinholeCamera(arg1, arg2, x/600*10*normalize(arg3p), y/600*10*normalize(arg4p),x,y,fl);
c2(x, y) = IsometricCamera(arg1, arg2, x/600*10*normalize(arg3p), y/600*10*normalize(arg4p),x,y);
p = render(tr, (x=1080, y=1080), c2, DefaultShaderArray)

p = render(CSGNode(ImplicitSphere([0.0,0,0], 5), []), (x=480, y=640), c1, DefaultShaderArray)
p = render(CSGNode(ImplicitSphere([0.0,0,0], 5), []), (x=1080, y=1920), c1, DefaultShaderArray)
