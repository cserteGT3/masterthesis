using StaticArrays
using LinearAlgebra
using Random
using Makie

using Revise
using RANSAC
using RANSAC: segmentnormal, midpoint
using RANSACVisualizer

function tsegments(R, nofs)
    @assert nofs>2
    pia = collect(range(0, stop=2π, length=nofs+1))
    deleteat!(pia,lastindex(pia))
    return [SVector{2,Float64}(R*cos(i), R*sin(i)) for i in pia]
end

function plotsegmentwithnormals(segs)
    tns = [segmentnormal(segs, i) for i in eachindex(segs)]
    tms = [midpoint(segs, i) for i in eachindex(segs)]

    sct = lines(segs, scale_plot=false)
    scatter!(sct, tms, color=:red, scale_plot=false)
    x_ = (x->x[1]).(tms)
    y_ = (x->x[2]).(tms)
    u_ = (x->x[1]).(tns)
    v_ = (x->x[2]).(tns)
    arrows!(sct, x_, y_, u_, v_, scale_plot=false)
    sct
end

tps = tsegments(2, 20)
plotsegmentwithnormals(tps)

sct = scatter(tps)
sct = scatter(tps, scale_plot=false)
scatter!(sct, tms, color=:red)
