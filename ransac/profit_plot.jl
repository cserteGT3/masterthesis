using LinearAlgebra
using Random
using StaticArrays
using Makie
using GeometryTypes

using Revise
using RANSAC
using RANSAC: delaunay, to_edges!, spanning_tree, thinning, fit_in_chunks

function wframe(ps, trs; kwargs...)
	vrts = [Point3(i[1], i[2], 0.) for i in ps]
	fces = [Face{3,Int}(i) for i in trs]
	tm = HomogenousMesh(vrts, fces, [], [], [], [], [])
	Makie.wireframe(tm; kwargs...)
end

function makeps(noise=1)
	p = []
    for α in range(pi/2, stop=pi, length=400)
        push!(p, [cos(α), sin(α)] * 20 + [rand(), rand()] * noise)
    end
	for t in range(0, stop=1, length=100)
        push!(p, [0, 20] + [20, 0] * t + [rand(), rand()] * noise)
    end
    for α in range(pi, stop=3pi/2, length=100)
        push!(p, [30, 20] + [cos(α), sin(α)] * 10 + [rand(), rand()] * noise)
    end
    for t in range(0, stop=1, length=100)
        push!(p, [30, 10] + [0, -20] * t + [rand(), rand()] * noise)
    end
	return [SVector{2,Float64}(i) for i in p]
end

function plotspantree(points, tree)
	s = Scene()
	for (i, obj) in enumerate(tree)
		p2 = points[obj]
		lines!(s, (x->x[1]).(p2), (x->x[2]).(p2))
	end
	s
end

function plotfitted(points, chunks, fitresult, fit_chunks)
	sce = Scene()
	for (i, object) in enumerate(fitresult)
		# chunks
		f,t = fit_chunks[i]
		inds = vcat(chunks[f:t]...)
		coli = isodd(i) ? :blue : :red
		scatter!(sce, (x->x[1]).(points[inds]), (x->x[2]).(points[inds]), color=coli, markersize=0.2)
		# fitted lines/circles
		samples = []
		if object.type === :arc
            samples = [object.p + [cos(x), sin(x)] * object.r
                       for x in range(object.min, stop=object.max, length=100)]
        elseif object.type === :segment
            samples = [object.a, object.b]
        end
		col2 = isodd(i) ? :orange : :green
		lines!(sce, (x->x[1]).(samples), (x->x[2]).(samples), linewidth=2.5, color=col2)
	end
	sce
end

nois = 1
pn = makeps(nois)
shuffle!(pn)

sc1_ = scatter(pn, markersize=0.4, color=:blue)
sc1 = title(sc1_, "Input")

#plot1 = scatter((x->x[1]).(pn), (x->x[2]).(pn), xlim=(-25,35), ylim=(-25,30),
#				markersize=2, markerstrokealpha=0.2, label="input")
tris = delaunay(pn);
all_edges = to_edges!(tris);
weights = [norm(pn[e[1]] - pn[e[2]]) for e in all_edges];
tree = spanning_tree(all_edges, weights);
thinned, chunks = thinning(pn, tree, 2.0);
th = [SVector{2,Float64}(b) for b in thinned]

sc2_ = wframe(pn, tris)
sc2 = title(sc2_, "Triangulation")

sc3_ = plotspantree(pn, tree)
sc3 = title(sc3_, "Minimum spanning tree")

sc4_ = lines(th, color=:red, linewidth=1.5)
sc4 = title(sc4_, "Polyline")

res, resc = fit_in_chunks(pn, th, chunks, 1.0)

sc5_ = plotfitted(pn, chunks, res, resc)
sc5 = title(sc5_, "First fit")

hbox(vbox(sc3, sc4, sc5), vbox(sc1, sc2))

norms = [RANSAC.segmentnormal(th, i) for i in eachindex(th)]
mip = [RANSAC.midpoint(th, i) for i in eachindex(th)]

#scatter!(sc4_, mip, markersize=0.3)

x_ = [mip[i][1] for i in eachindex(mip)]
y_ = [mip[i][2] for i in eachindex(mip)]
u_ = [norms[i][1] for i in eachindex(mip)]
v_ = [norms[i][2] for i in eachindex(mip)]

sc66_ = lines(th, color=:red, linewidth=1.5)
sc6_ = arrows!(deepcopy(sc4_), x_, y_, u_, v_)
sc6 = title(sc6_, "Polyline with normals")

hbox(vbox(sc4, sc6, sc5), vbox(sc1, sc2, sc3))

hmx = collect(-25:0.5:34)
hmy = collect(-15:0.5:25)
memm = [RANSAC.dist2segment([i, j], th)[1] for i in hmx, j in hmy]

sc7_ = heatmap(hmx, hmy, memm)
lines!(sc7_, th, color=:black)
sc7 = title(sc7_, "Distance from contour")
