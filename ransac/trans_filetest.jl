# every include
using LinearAlgebra
using StaticArrays
using Random
using Colors
using Makie
using FileIO
using Logging
const deflog = global_logger()

using Revise
using RANSAC
using RANSACVisualizer
using CSGBuilding

cd(@__DIR__)

m1 = load("tomi1.obj");
m2 = load("tomi2.obj");
m3 = load("tomi3.obj");

mr = load("tomi_remesh.obj");

showgeometry(m1)
showgeometry(m2)
showgeometry(m3)
showgeometry(mr)

## Remesh


global_logger(nosource_IterInflog())
Random.seed!(1234);
rp = randperm(size(mr.vertices,1));
pcr = PointCloud(mr.vertices[rp], mr.normals[rp], 2);

p = RANSACParameters{Float64}(ϵ_plane=0.2,
                                α_plane=deg2rad(5),
                                ϵ_sphere=0.1,
                                α_sphere=deg2rad(1),
                                ϵ_cylinder=0.3,
                                α_cylinder=deg2rad(2.5),
                                ϵ_cone=0.15,
                                α_cone=deg2rad(1),
                                ϵ_transl=0.3,
                                α_transl=deg2rad(5),
                                minsubsetN=15,
                                itermax=100,
                                τ = 200,
                                prob_det=0.9);

cand, extr = ransac(pcr, p, true, reset_rand=true);
showtype(extr)
showtype(cand)
showbytype(pcr, extr, markersize=0.3)
showbytype(pcr, cand, markersize=0.5)
showshapes(pcr, extr, markersize=0.3)
showshapes!(sc, pcr, [cand[6]]; markersize=1)
scatter(pcr.vertices[getrest(pcr)])

global_logger(nosource_Errorlog())
## distances
sc = Vector{Scene}(undef, 6)
for i in [1,2,3,4,6]
    sc2 = Scene()
    translheatmap!(sc2, extr[2].candidate.shape, 100, i; scale_plot=false)
    #sc[i] = sc2
    sc[i] = title(sc2, "$i-th")
end
sc[5] = title(scatter([1]), "5-th")
scv = hbox(vbox(sc[4], sc[5], sc[6]), vbox(sc[1], sc[2], sc[3]))
global_logger(deflog)

translheatmap(extr[2].candidate.shape, 100, 1; scale_plot=false)
translheatmap(extr[2].candidate.shape, 100, 2; scale_plot=false)
translheatmap(extr[2].candidate.shape, 500, 3; scale_plot=false)
translheatmap(extr[2].candidate.shape, 500, 4; scale_plot=false)
translheatmap(extr[2].candidate.shape, 500, 6; scale_plot=false)

## CSGB
convp = (α_conv=cosd(5), ϵ_conv=7);
impls, remt = ransacresult2implicit(pcr, extr, convp);
sc = Scene()
scatter!(sc, pcr.vertices)
for o in impls
    plotimplshape!(sc, o, scale=(10.,10.))
end

##by hand
extr1 = impls[1]
extr2 = impls[end]
n1 = CSGNode(extr1, []);
n2 = CSGNode(extr2, []);
edgel = (mincorner=-20, maxcorner=20, edgelength=110);
handy = n2
writeparaviewformat(handy, "handy_extr", edgel)

plotcontour(extr2, true, true)


## by type
sc = showbytype(pcr2, extr; markersize=0.5)
scatter!(sc, pcr2.vertices[getrest(pcr2)], markersize=0.5)
cam3d_cad!(sc)

sc2 = showgeometry(d_vs, d_ns);
sc3 = hbox(sc, sc2)

## automatic
pcsg = CSGGeneticBuildParameters{Float64}(maxdepth=10,
                                        itermax=100)
popu, best = cachedfuncgeneticbuildtree(impls, pcr.vertices, pcr.normals, pcsg)

edgel = (mincorner=-20, maxcorner=20, edgelength=110);
writeparaviewformat(tload[:a], "best_tomi_remesh", edgel)


## different
sc = showshapes(pcr2, extr; markersize=0.3)
scatter!(sc, pcr2.vertices[getrest(pcr2)], markersize=0.3)
cam3d_cad!(sc)

sc2 = showgeometry(d_vs, d_ns);
sc3 = hbox(sc, sc2)


using ColorSchemes

colsss = ColorSchemes.gnuplot
get(colsss, 0.5)



tl = [0, 1, 2]
lll = [interp(tl[i], tl[i+1],4) for i in 1:2]

vcat(lll...)

function interp(a, b, n)
    @assert n>2
    r = collect(range(0, stop=1, length=n))
    deleteat!(r, 1)
    deleteat!(r, lastindex(r))
    return [(1-i)*a+i*b for i in r]
end
