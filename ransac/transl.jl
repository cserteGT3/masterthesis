using StaticArrays
using LinearAlgebra
using FileIO
using Random
using Makie

using Revise
using RANSAC
using RANSAC: delaunay, to_edges!, spanning_tree, thinning, fit_in_chunks
using RANSAC: segmentnormal, midpoint
using RANSACVisualizer


cd(@__DIR__)

m1 = load("tomi1.obj");
m2 = load("tomi2.obj");
m3 = load("tomi3.obj");

mr = load("tomi_remesh.obj");

showgeometry(m1)
showgeometry(m2)
showgeometry(m3)
showgeometry(mr)

Random.seed!(1234)
rp = randperm(size(mr.vertices,1));
vs1 = [SVector{3, Float64}(i) for i in mr.vertices];
ns1 = [SVector{3, Float64}(i) for i in mr.normals];

pc1 = PointCloud(vs1[rp], ns1[rp], 1)
rind = [155, 4500, 10000];
p = RANSACParameters{Float64}(min_normal_num=0.85);
fr = RANSAC.fittranslationalsurface(pc1, vs1[rind], ns1[rind], p)

vbox(lines.(x->x.contour, fr)...)
cc1 = compandcenter(fr[1])
scatter!(cc1, [fr[1].center], color=:orange)

vbox(plotcontour.(fr, trues(2), trues(2))...)


sc1 = scatter(ps)

dtuple = RANSAC.minmaxdistance(ps)

thi, chu = RANSAC.thinning_slow(ps, p.ϵ_transl/2)
sc2 = lines([SVector{2,Float64}(t_) for t_ in thi])
vbox(sc1, sc2)


Random.seed!(1234)
rp = randperm(size(m2.vertices,1));
vs1 = [SVector{3, Float64}(i) for i in m2.vertices];
ns1 = [SVector{3, Float64}(i) for i in m2.normals];

pc1 = PointCloud(vs1[rp], ns1[rp], 1)
rind = [516, 4312, 6789];
p = RANSACParameters{Float64}(max_contour_it=10, ϵ_transl=0.7);
fittr = RANSAC.fittranslationalsurface(pc1, vs1[rind], ns1[rind], p)

scatter(fittr[2])

vbox(lines.(x->x.contour, fittr[1])...)

## normals
Random.seed!(1234)
rp = randperm(size(mr.vertices,1));
vs1 = [SVector{3, Float64}(i) for i in mr.vertices];
ns1 = [SVector{3, Float64}(i) for i in mr.normals];

pc1 = PointCloud(vs1[rp], ns1[rp], 1)
rind = [155, 4500, 10000];
p = RANSACParameters{Float64}(min_normal_num=0.85);
fr = RANSAC.fittranslationalsurface(pc1, vs1[rind], ns1[rind], p)
compandcenter(fr[1])

plotcontour(fr[1], true, false)

sc_ = scatter(fr[2].p, scale_plot=false, color=:green)
scatter!(sc_, [fr[2].c], color=:orange, scale_plot=false)
_1(x) = x[1]
_2(x) = x[2]

arrows!(sc_, _1.(fr[2].p), _2.(fr[2].p), _1.(fr[2].n), _2.(fr[2].n), arrowcolor=:blue, scale_plot=false)


psi = fr[2].p
nsi = fr[2].n
csi = fr[2].c
ssi = fr[2].closed

k = RANSAC.normaldirs(ssi, psi, nsi, csi, p)

calcs = [RANSAC.dist2segment(p, ssi) for p in psi]

sc_ = scatter(fr[2].p, scale_plot=false, color=:green)
lines!(sc_, ssi, scale_plot=false,color=:orange)

lines(ssi, scale_plot=false,color=:orange)
scatter!([psi[1]], scale_plot=false)
ith = calcs[1][2]
lines!([ssi[ith], ssi[ith+1]], scale_plot=false, color=:red)

RANSAC.dist2segment(psi[1], ssi)

for i in eachindex(ssi)
    println(RANSAC.distance2onesegment(psi[1], ssi, i))
end
