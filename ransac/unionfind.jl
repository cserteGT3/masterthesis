using NearestNeighbors
using StaticArrays
using LinearAlgebra
using FileIO
using Random
using Makie
using ImageView
using UnionFind
using BenchmarkTools

using Revise
using RANSAC

cd(@__DIR__)

mr = load("tomi_remesh.obj");

Random.seed!(1234)
rp = randperm(size(mr.vertices,1));
vs1 = [SVector{3, Float64}(i) for i in mr.vertices];
ns1 = [SVector{3, Float64}(i) for i in mr.normals];

pc1 = PointCloud(vs1[rp], ns1[rp], 1)
rind = [155, 4500, 10000];
p = RANSACParameters{Float64}();
ps, bm = RANSAC.fittranslationalsurface(pc1, vs1[rind], ns1[rind], p)

scatter(ps)
imshow(bm)


function t(pois, thr)
    btree = BallTree(pois)
    uf = UnionFinder(size(pois,1))

    for i in eachindex(pois)
        inr = inrange(btree, pois[i], thr ,true)
        for j in eachindex(inr)
            # the point itself is always in range
            i == inr[j] && continue
            union!(uf, i, inr[j])
        end
    end
    return uf
end

function t2(pois, thr)
    btree = KDTree(pois)
    uf = UnionFinder(size(pois,1))

    for i in eachindex(pois)
        inr = inrange(btree, pois[i], thr ,true)
        for j in eachindex(inr)
            # the point itself is always in range
            i == inr[j] && continue
            union!(uf, i, inr[j])
        end
    end
    return uf
end

ufn = t(pcr.vertices[ps.contourindexes], 0.51)
cfn = CompressedFinder(ufn)
groups(cfn)

g1 = pcr.vertices[ps.contourindexes[findall(x->x==1, cfn.ids)]]
g2 = pcr.vertices[ps.contourindexes[findall(x->x==2, cfn.ids)]]

sc = scatter(g1, color=:blue)
scatter!(sc, g2, color=:red)

scatter(ps)

## triangle
function makecurve()
    noise = 1
    p = []
    for α in range(pi/2, stop=pi, length=100)
        push!(p, [cos(α), sin(α)] * 20 + [rand(), rand()] * noise)
    end
    for t in range(0, stop=1, length=100)
        push!(p, [0, 20] + [20, 0] * t + [rand(), rand()] * noise)
    end
    for α in range(pi, stop=3pi/2, length=100)
        push!(p, [30, 20] + [cos(α), sin(α)] * 10 + [rand(), rand()] * noise)
    end
    for t in range(0, stop=1, length=100)
        push!(p, [30, 10] + [0, -20] * t + [rand(), rand()] * noise)
    end
    shuffle!(p)
    return SVector{2,Float64}.(p)
end

pt = makecurve()

dpt = [pti+[2, 13] for pti in pt]

scd = scatter(pt, color=:green, markersize=0.5)
scatter!(scd, dpt, color=:orange, markersize=0.5)

apt = shuffle!(vcat(pt, dpt))
ufn2 = t(apt, 1.1)
cfn2 = CompressedFinder(ufn2)
groups(cfn2)

g1p = apt[findall(x->x==1, cfn2.ids)]
g2p = apt[findall(x->x==2, cfn2.ids)]

scp = scatter(g1p, color=:blue, markersize=0.5)
scatter!(scp, g2p, color=:red, markersize=0.5)

vbox(scd, scp)


## kd tree vs balltree
pss = [SVector{3,Float64}(10 .* rand(3)) for _ in 1:100_000];
epsi = 0.5
cff = CompressedFinder(t(pss, epsi))
groups(cff)

cff2 = CompressedFinder(t2(pss, epsi))
groups(cff)
