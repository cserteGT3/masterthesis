module HomVs

export HomV, HomMat
export materialize, multi

struct HomV
    v
    HomV(x) = size(x) == (3,) ? new(x) : error("Must be 3 long.")
end

struct HomMat
    rot
    transl
end

multi(m, v) = m.rot*v.v+m.transl


materialize(m::HomV) = vcat(m.v, 1)

function materialize(m::HomMat)
    vcat(hcat(m.rot, m.transl), [0 0 0 1])
end



function fullika(m1, v1)

end


end
