using LinearAlgebra
using Logging
using Random
using StaticArrays
using FileIO
#using Makie

using Revise
using RANSAC

using RANSACVisualizer
const deflog = global_logger()
cd(@__DIR__)

f = @__FILE__
sbf = splitext(basename(f))
fbase = replace(sbf[1], "-"=>"")
fn = fbase*".obj"
if ! isfile(fn)
    @error "$fn is not a file."
    @show f
    @show sbf
    @show fbase
    @show fn
end

fmesh = load(fn);
println("mesh size: ", size(fmesh.vertices,1))


global_logger(nosource_IterInflog())
#TODO set number of subsets!!!
pcr = PointCloud(fmesh.vertices, fmesh.normals, 4);

p = RANSACParameters{Float64}(ϵ_plane=0.2,
                                α_plane=deg2rad(3),
                                ϵ_sphere=0.1,
                                α_sphere=deg2rad(3),
                                ϵ_cylinder=0.3,
                                α_cylinder=deg2rad(3),
                                ϵ_cone=0.2,
                                α_cone=deg2rad(1),
                                ϵ_transl=0.3,
                                α_transl=deg2rad(3),
                                α_perpend = cosd(89.5),
                                minsubsetN=15,
                                itermax=8000,
                                τ=500,
                                shape_types=[:plane, :sphere, :cylinder, :cone],
                                prob_det=0.9);

cand, extr, rtime = ransac(pcr, p, true, reset_rand=true);

showtype(extr)
showtype(cand)
showbytype(pcr, extr, markersize=0.3)
showshapes(pcr, extr, markersize=0.5)
scatter(pcr.vertices[getrest(pcr)])

## Profiling
using Profile
using PProf
