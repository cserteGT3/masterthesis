using RANSAC, RANSACBenchmark
#using RANSAC, RANSACBenchmark, RANSACVisualizer

bc2 = benchmarkcloud2();
pc = RANSACCloud(bc2.vertices, bc2.normals, 4);
p = ransacparameters(iteration=(τ=100, itermax=10, prob_det=0.99,));
@time extr, _ = ransac(pc, p, true; reset_rand=true);
extr

using ProfileVega

p = ransacparameters(p, iteration=(itermax=20_000,));

@profview ransac(pc, p, true; reset_rand=true)

using StaticArrays, RegionTrees

minV, maxV = findAABB(pc.vertices)
octree=Cell(SVector{3}(minV), SVector{3}(maxV), OctreeNode(pc, collect(1:pc.size), 1))
r = OctreeRefinery(8)
adaptivesampling!(octree, r)

#=
problems:
iterations.jl - 

114 - EZ BAJ
# get all the parents
cs = getcellandparents(current_leaf)

124+runtime dispatch - EZ NAGYON FELESLEGES
# frome the above, those that are enabled
bool_cell_ind = @view pc.isenabled[cell_ind]
enabled_inds = cell_ind[bool_cell_ind]

171
forcefitshapes!(pc, f_v, f_n, params, candidates, curr_level)

180 - EZ A LEGROSSZABB
sc = scorecandidate(pc, c, which_, params)

181, 182
best_shape = largestshape(scoredshapes)
best = findhighestscore(scoredshapes)
=#

#= ugly benchmark:
using RANSAC, RANSACBenchmark
bc2 = benchmarkcloud2();
pc = PointCloud(bc2.vertices, bc2.normals, 4);
p = ransacparameters(iteration=(τ=100, itermax=10, prob_det=0.99,));
_, extr, _ = ransac(pc, p, true; reset_rand=true);
p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));
_, extr, tt = ransac(pc, p, true; reset_rand=true);
println("$tt seconds")
extr
versioninfo()

## after #12 got merged

julia> println("after #12")
after #12

julia> using RANSAC, RANSACBenchmark

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=10, prob_det=0.99,));

julia> _, extr, _ = ransac(pc, p, true; reset_rand=true);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> _, extr, tt = ransac(pc, p, true; reset_rand=true);

julia> println("$tt seconds")
2.73 seconds

julia> extr
4-element Array{ShapeCandidate,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps
 
# home pc:
 
 julia> using RANSAC, RANSACBenchmark
[ Info: Precompiling RANSAC [d6bc97e0-a563-11e9-1861-c573f65f3bc3]
[ Info: Precompiling RANSACBenchmark [37ac5fee-0dc1-428d-87ed-e8867d5d5384]

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=10, prob_det=0.99,));

julia> _, extr, _ = ransac(pc, p, true; reset_rand=true);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> _, extr, tt = ransac(pc, p, true; reset_rand=true);

julia> println("$tt seconds")
2.05 seconds

julia> extr
4-element Array{ShapeCandidate,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i5-3570 CPU @ 3.40GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)

julia>   
 
=#

## a bit nicer benchmark

#=
using RANSAC, RANSACBenchmark, BenchmarkTools
bc2 = benchmarkcloud2();
pc = RANSACCloud(bc2.vertices, bc2.normals, 4);
p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));
extr, _ = ransac(pc, p, true; reset_rand=true);
extr
@benchmark ransac($pc, $p, true; reset_rand=true) seconds=20
versioninfo()

# home pc after #12

julia> using RANSAC, RANSACBenchmark, BenchmarkTools

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> _, extr, _ = ransac(pc, p, true; reset_rand=true);

julia> extr
4-element Array{ShapeCandidate,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=60
BenchmarkTools.Trial:
  memory estimate:  703.70 MiB
  allocs estimate:  6975636
  --------------
  minimum time:     1.301 s (4.71% GC)
  median time:      1.354 s (4.69% GC)
  mean time:        1.369 s (6.38% GC)
  maximum time:     1.534 s (16.15% GC)
  --------------
  samples:          44
  evals/sample:     1

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i5-3570 CPU @ 3.40GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)

# home pc after sphere remove commit

julia> using RANSAC, RANSACBenchmark, BenchmarkTools
[ Info: Precompiling RANSAC [d6bc97e0-a563-11e9-1861-c573f65f3bc3]
[ Info: Precompiling RANSACBenchmark [37ac5fee-0dc1-428d-87ed-e8867d5d5384]

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> _, extr, _ = ransac(pc, p, true; reset_rand=true);

julia> extr
4-element Array{ShapeCandidate,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=60
BenchmarkTools.Trial:
  memory estimate:  699.63 MiB
  allocs estimate:  6931837
  --------------
  minimum time:     1.263 s (5.01% GC)
  median time:      1.288 s (5.06% GC)
  mean time:        1.319 s (6.65% GC)
  maximum time:     1.546 s (16.09% GC)
  --------------
  samples:          46
  evals/sample:     1

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i5-3570 CPU @ 3.40GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)

  # after not collecting octree cells

  julia> using RANSAC, RANSACBenchmark, BenchmarkTools
[ Info: Precompiling RANSAC [d6bc97e0-a563-11e9-1861-c573f65f3bc3]
[ Info: Precompiling RANSACBenchmark [37ac5fee-0dc1-428d-87ed-e8867d5d5384]

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> _, extr, _ = ransac(pc, p, true; reset_rand=true);

julia> extr
4-element Array{ShapeCandidate,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=60
BenchmarkTools.Trial:
  memory estimate:  696.82 MiB
  allocs estimate:  6870593
  --------------
  minimum time:     1.194 s (4.98% GC)
  median time:      1.226 s (5.24% GC)
  mean time:        1.250 s (7.02% GC)
  maximum time:     1.470 s (17.62% GC)
  --------------
  samples:          48
  evals/sample:     1

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i5-3570 CPU @ 3.40GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)

# home PC before score magic

julia> using RANSAC, RANSACBenchmark, BenchmarkTools

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> extr, _ = ransac(pc, p, true; reset_rand=true);

julia> extr
4-element Array{ShapeCandidate,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=60
BenchmarkTools.Trial:
  memory estimate:  678.64 MiB
  allocs estimate:  6644264
  --------------
  minimum time:     1.117 s (5.12% GC)
  median time:      1.163 s (5.23% GC)
  mean time:        1.186 s (6.82% GC)
  maximum time:     1.374 s (17.90% GC)
  --------------
  samples:          51
  evals/sample:     1

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i5-3570 CPU @ 3.40GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)

# home PC after #17

julia> using RANSAC, RANSACBenchmark, BenchmarkTools
[ Info: Precompiling RANSAC [d6bc97e0-a563-11e9-1861-c573f65f3bc3]
[ Info: Precompiling RANSACBenchmark [37ac5fee-0dc1-428d-87ed-e8867d5d5384]

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> extr, _ = ransac(pc, p, true; reset_rand=true);

julia> extr
4-element Array{ShapeCandidate,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=60
BenchmarkTools.Trial:
  memory estimate:  680.32 MiB
  allocs estimate:  6664208
  --------------
  minimum time:     1.180 s (4.59% GC)
  median time:      1.255 s (5.26% GC)
  mean time:        1.295 s (6.67% GC)
  maximum time:     1.720 s (19.36% GC)
  --------------
  samples:          47
  evals/sample:     1

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i5-3570 CPU @ 3.40GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)
 
# work laptop on #12

julia> using RANSAC, RANSACBenchmark, BenchmarkTools

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> _, extr, _ = ransac(pc, p, true; reset_rand=true);

julia> extr
4-element Array{ShapeCandidate,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=60
BenchmarkTools.Trial:
  memory estimate:  703.70 MiB
  allocs estimate:  6975636
  --------------
  minimum time:     1.713 s (4.51% GC)
  median time:      1.902 s (4.36% GC)
  mean time:        1.953 s (6.10% GC)
  maximum time:     2.476 s (14.94% GC)
  --------------
  samples:          31
  evals/sample:     1

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i7-3632QM CPU @ 2.20GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)

julia>

# work laptop on #22+fix commit

julia> using RANSAC, RANSACBenchmark, BenchmarkTools
[ Info: Precompiling RANSAC [d6bc97e0-a563-11e9-1861-c573f65f3bc3]
[ Info: Precompiling RANSACBenchmark [37ac5fee-0dc1-428d-87ed-e8867d5d5384]

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> extr, _ = ransac(pc, p, true; reset_rand=true);

julia> extr
4-element Array{ExtractedShape,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=60
BenchmarkTools.Trial:
  memory estimate:  218.39 MiB
  allocs estimate:  1611185
  --------------
  minimum time:     1.141 s (1.87% GC)
  median time:      1.180 s (2.08% GC)
  mean time:        1.238 s (4.10% GC)
  maximum time:     1.717 s (14.33% GC)
  --------------
  samples:          49
  evals/sample:     1

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i7-3632QM CPU @ 2.20GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)

## work laptop, before sampling is moved to function

julia> println("before function")
before function

julia> using RANSAC, RANSACBenchmark, BenchmarkTools
[ Info: Precompiling RANSAC [d6bc97e0-a563-11e9-1861-c573f65f3bc3]
[ Info: Precompiling RANSACBenchmark [37ac5fee-0dc1-428d-87ed-e8867d5d5384]

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> extr, _ = ransac(pc, p, true; reset_rand=true);

julia> extr
4-element Array{ExtractedShape,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=60
BenchmarkTools.Trial:
  memory estimate:  189.18 MiB
  allocs estimate:  1130106
  --------------
  minimum time:     838.522 ms (1.93% GC)
  median time:      869.304 ms (2.26% GC)
  mean time:        897.526 ms (4.96% GC)
  maximum time:     1.087 s (20.02% GC)
  --------------
  samples:          67
  evals/sample:     1

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i7-3632QM CPU @ 2.20GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)
  
# work laptop on master

julia> println("master- 7609")
master- 7609

julia> using RANSAC, RANSACBenchmark, BenchmarkTools
[ Info: Precompiling RANSAC [d6bc97e0-a563-11e9-1861-c573f65f3bc3]
[ Info: Precompiling RANSACBenchmark [37ac5fee-0dc1-428d-87ed-e8867d5d5384]

julia> bc2 = benchmarkcloud2();

julia> pc = PointCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> extr, _ = ransac(pc, p, true; reset_rand=true);

julia> extr
4-element Array{ExtractedShape,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=60
BenchmarkTools.Trial:
  memory estimate:  189.18 MiB
  allocs estimate:  1130106
  --------------
  minimum time:     850.231 ms (2.40% GC)
  median time:      882.723 ms (2.31% GC)
  mean time:        909.333 ms (5.49% GC)
  maximum time:     1.035 s (12.83% GC)
  --------------
  samples:          67
  evals/sample:     1

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i7-3632QM CPU @ 2.20GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)

# on work laptop after octree merged into point cloud

julia> using RANSAC, RANSACBenchmark, BenchmarkTools

julia> bc2 = benchmarkcloud2();

julia> pc = RANSACCloud(bc2.vertices, bc2.normals, 4);

julia> p = ransacparameters(iteration=(τ=100, itermax=20_000, prob_det=0.99,));

julia> extr, _ = ransac(pc, p, true; reset_rand=true);

julia> extr
4-element Array{ExtractedShape,1}:
 Cand: (sphere, R: 5.0), 2567 ps
 Cand: (cone, ω: 0.7853981633974495), 868 ps
 Cand: (cylinder, R: 1.7899999999999996), 629 ps
 Cand: (plane), 312 ps

julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=60
BenchmarkTools.Trial:
  memory estimate:  191.23 MiB
  allocs estimate:  1240501
  --------------
  minimum time:     846.271 ms (1.74% GC)
  median time:      882.345 ms (2.14% GC)
  mean time:        912.912 ms (4.22% GC)
  maximum time:     1.186 s (1.71% GC)
  --------------
  samples:          66
  evals/sample:     1

julia> versioninfo()
Julia Version 1.4.0
Commit b8e9a9ecc6 (2020-03-21 16:36 UTC)
Platform Info:
  OS: Windows (x86_64-w64-mingw32)
  CPU: Intel(R) Core(TM) i7-3632QM CPU @ 2.20GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-8.0.1 (ORCJIT, ivybridge)

  # on master after 0.5.1 & fix worklaptop
  julia> @benchmark ransac($pc, $p, true; reset_rand=true) seconds=20
BenchmarkTools.Trial:
  memory estimate:  198.89 MiB
  allocs estimate:  1277254
  --------------
  minimum time:     827.354 ms (2.27% GC)
  median time:      840.109 ms (2.19% GC)
  mean time:        868.631 ms (4.82% GC)
  maximum time:     1.087 s (20.42% GC)
  --------------
  samples:          24
  evals/sample:     1

  # on PR#27 before merge on worklaptop

=#
