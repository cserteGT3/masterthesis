using Revise
using RANSAC
using FileIO

bp = @__FILE__;
bdir = dirname(bp);
fn = joinpath(bp |> dirname|> dirname |> dirname |> dirname, "DATA", "Schnabel2009", "fandisk_input.obj");

m = load(fn)

pc = PointCloud(m.vertices, m.normals, 8)
aa,bb = findAABB(m.vertices);
EPS = (maximum(bb-aa))*0.01
ALF = deg2rad(10)

p = RANSACParameters{Float64}()
p = setalphas(p, ALF)
p = setepsilons(p, EPS)
p = RANSACParameters(p, τ=50, minconeopang=deg2rad(5), itermax=100_000)

cand, extr, rtime = ransac(pc, p, true, reset_rand=true);

using BSON
bson(joinpath(bdir, "_ransac.bson"), Dict(:extracted=>extr, :pointcloud=>pc, :params=>[p]))

using StaticArrays
bs = BSON.load(joinpath(bdir, "_ransac.bson"))
pc = bs[:pointcloud]
extr = bs[:extracted]

using RANSACVisualizer
using Makie

s = showshapes(pc, extr; show_axis = false, resolution=(1920,1080))
s.center=false
#save(joinpath(bdir, "showshapes_.png"), s)


s2 = showgeometry(m, arrow=0.3; show_axis = false, resolution=(1920,1080))
s2.center=false
#save(joinpath(bdir, "showgeom_.png"), s2)

s3 = showbytype(pc, extr; show_axis = false, resolution=(1920,1080))
s3.center=false
#save(joinpath(bdir, "bytype_.png"), s3)


# 2 subsets: 242.98s
# 8 subsets: 252.01s
# 16 subsets: 232.94s
# 50 subsets: 336.41s
