module GlobFitParser

using GeometryTypes
using FileIO
using LinearAlgebra: norm, normalize!

export parseglobfit, saveglobfit2

export DATASETS_PATH

const DATASETS_PATH = raw"C:\datasets"

"""
    parseglobfit(fname; donormalize=false, MeshType=GLNormalMesh)

Parse a globfit file into mesh.
"""
function parseglobfit(fname; donormalize=false, MeshType=GLNormalMesh)
    isfile(fname) || error("$fname not exists.")

    lines = readlines(fname)
    msize = parse(Int, lines[2])
    occursin("the $msize Points", lines[3]) || error("Should be $msize points, but line 3 doesn't match.")

    VertexType = vertextype(MeshType)
    NormalType = normaltype(MeshType)

    vts = Array{VertexType}(undef, msize)
    nts = Array{NormalType}(undef, msize)
    fcs = facetype(MeshType)[]

    shifted = @view lines[5:4+msize]

    for i in eachindex(shifted)
        splitted = split(shifted[i], " ")
        vts[i] = VertexType(parse.(eltype(VertexType), splitted[1:3]))
        nts[i] = NormalType(parse.(eltype(NormalType), splitted[1:3]))
    end

    if donormalize
        for i in eachindex(nts)
            nts[i] = normalize(nts[i])
        end
    else
        for a in nts
            if ! isapprox(norm(a), 1)
                @warn "At least one normal is not normalized. You may normalize all."
                break
            end
        end
    end

    GLNormalMesh(vertices=vts, normals=nts, faces=[])
end

"""
    saveglobfit2(fname, ext; kwargs...)

`fname` must be a `.globfit` file.
"""
function saveglobfit2(fname, ext; kwargs...)
    m = parseglobfit(fname; kwargs...)
    nfile = replace(fname, ".globfit"=>ext)
    save(nfile, m)
end

end
