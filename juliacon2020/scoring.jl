using RANSAC, StaticArrays, LinearAlgebra, Random

Random.seed!(9)
ra = [SVector{3}(rand(3)) for i in 1:100]
rn = [SVector{3}(0,0,1.) for i in 1:100]
fp = FittedPlane(SVector(0.5,0.5,0.5), SVector{3}(0,0,1.))
p = ransacparameters(; plane=(ϵ=0.2,))
