using LinearAlgebra
using StaticArrays
using Random
using Makie
using FileIO
using Logging
using GeometryTypes
using BSON
using D3Trees

const deflog = global_logger()



using Revise
using RANSAC
using RANSACVisualizer
using CSGBuilding

cd(@__DIR__)

m = load("icsg_157csg.obj")
showgeometry(m)

dbr = BSON.load("icsg_157csgbasic_ex_ransac.bson")
pc = dbr[:pointcloud]
extr = dbr[:extracted]

dbg = BSON.load("icsg_157csgbasic_ex_genetic.bson")
best = dbg[:bestcached]
inchrome(D3Tree(best, init_expand=15))

showshapes(pc, extr)
showbytype(pc, extr)
