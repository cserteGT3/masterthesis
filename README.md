# MasterThesis

[![pipeline](https://gitlab.com/cserteGT3/masterthesis/badges/master/pipeline.svg)](https://gitlab.com/cserteGT3/masterthesis)

This repository contains my master thesis.
I graduated in 2021, this code and the packages have not been updated since then.
The packages I developed together with my advisor Péter Salvi:
- [RANSAC.jl](https://github.com/cserteGT3/RANSAC.jl)
- [CSGBuilding.jl](https://github.com/cserteGT3/CSGBuilding.jl)
- [RANSACVisualizer.jl](https://github.com/cserteGT3/RANSACVisualizer.jl)

Latest build of the thesis itself is available as a [pdf](https://gitlab.com/csertegt3/masterthesisbuild/-/jobs/artifacts/master/raw/cstamas_thesis.pdf?job=pdf).
